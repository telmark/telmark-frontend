import dynamic from 'next/dynamic';

import Head from '@/entities/seo/Head';

const Empty404Page = dynamic(() => import('../src/pages/Empty404/Empty404Page'), {
  ssr: true,
});

export default function Empty404() {
  return (
    <>
      <Head title="Страница не найдена" descriptionText="Страница не найдена" />
      <Empty404Page />
    </>
  );
}
