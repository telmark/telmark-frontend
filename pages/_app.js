import dynamic from 'next/dynamic';
import localFont from 'next/font/local';
import { Suspense } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import 'yet-another-react-lightbox/plugins/captions.css';
import 'yet-another-react-lightbox/plugins/thumbnails.css';
import 'yet-another-react-lightbox/styles.css';

import { GlobalContextProvider, Layout } from '@/application';
import { RuntimeError } from '@/entities/common/runtime-error';
import '@/shared/styles/globals.scss';

const Gilroy = localFont({
  src: [
    {
      path: '../src/shared/fonts/Gilroy-Regular.woff2',
      style: 'normal',
      display: 'swap',
      weight: '400',
    },
    {
      path: '../src/shared/fonts/Gilroy-Medium.woff2',
      style: 'normal',
      display: 'swap',
      weight: '500',
    },
    {
      path: '../src/shared/fonts/Gilroy-Bold.woff2',
      display: 'swap',
      weight: '700',
      style: 'normal',
    },
  ],
});

const Metrika = dynamic(() => import('../src/entities/seo/Metrika'), {
  ssr: false,
});
const Roistat = dynamic(() => import('../src/entities/seo/Roistat'), {
  ssr: false,
});

export default function MyApp({ Component, pageProps }) {
  const props = { ...pageProps };
  return (
    <>
      <ErrorBoundary FallbackComponent={RuntimeError}>
        <GlobalContextProvider {...props}>
          <Layout className={Gilroy.className} {...props}>
            <Component {...props} />
          </Layout>
        </GlobalContextProvider>
      </ErrorBoundary>
      <Suspense>
        <Metrika />
        <Roistat />
      </Suspense>
    </>
  );
}
