import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const CompanyPage = dynamic(() => import('../src/pages/Company/CompanyPage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function About(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head
        title="О компании Телмарк"
        descriptionText="Информация о нашей компании. Делаем отгрузки кабельной продукции по всей России. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="О нас" />
      <CompanyPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
