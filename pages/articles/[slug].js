import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchSelectedArticle } from '@/pages/Articles/model';
import { addHeaders } from '@/shared/utils';

const SelectedArticlePage = dynamic(() => import('../../src/pages/Articles/SelectedArticlePage'), {
  ssr: true,
});
const Bredcrumps = dynamic(() => import('../../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function SelectedArticle(props) {
  const { canonicalUrl, articleInfo } = props;

  return (
    <>
      <Head
        title={articleInfo.meta?.title}
        descriptionText={articleInfo.meta?.description}
        canonicalUrl={canonicalUrl}
      />
      <Bredcrumps
        title={articleInfo.h1}
        additionalRoutes={[
          {
            href: '/articles',
            label: 'Статьи',
          },
        ]}
      />
      <SelectedArticlePage articleInfo={articleInfo} />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const { query } = context;
  const canonicalUrl = 'https://telmark.ru' + context.resolvedUrl.split('?')[0];
  addHeaders(context);
  let res = {};
  try {
    res = await fetchSelectedArticle(query.slug);

    if (!res?.meta) {
      return {
        notFound: true,
      };
    }
  } catch (e) {
    console.error('fetchSelectedArticle==', e);
    return {
      notFound: true,
    };
  }
  return { props: { canonicalUrl, articleInfo: res } };
};
