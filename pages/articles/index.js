import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchArticleList } from '@/pages/Articles/model';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const ArticlesPage = dynamic(() => import('../../src/pages/Articles/ArticlesPage'), {
  ssr: false,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Articles(props) {
  const { canonicalUrl, articleList = [] } = props;
  return (
    <>
      <Head
        title="Статьи от компании Телмарк"
        descriptionText="Полезные статьи. Телмарк — это низкие цены, отгрузки кабеля по всей России, сертифицированый товар."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="Статьи" />
      <ArticlesPage articleList={articleList} />
    </>
  );
}

export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  let res = {};
  try {
    res = await fetchArticleList();
  } catch (e) {
    console.error('fetchSelectedArticle==', e);
    return {
      notFound: true,
    };
  }
  return { props: { canonicalUrl, articleList: res } };
};
