import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchSelectedCategory, filterCatalogList } from '@/features/catalog';
import { fetchSelectedMark } from '@/features/marks/model';
import { fetchSelectedProduct } from '@/features/product/model';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const SelectedProductPage = dynamic(() => import('../../src/pages/Cable/SelectedProductPage'), {
  ssr: true,
});
const SelectedCablePage = dynamic(() => import('../../src/pages/Cable/SelectedCablePage'), {
  ssr: true,
});

const Bredcrumps = dynamic(() => import('../../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function SelectedCategory(props) {
  const { canonicalUrl, catalogInfo, selectedPageInfo, isSelectedProductPage } = props;

  return (
    <>
      <Head
        title={selectedPageInfo.meta?.title}
        descriptionText={selectedPageInfo.meta?.description}
        canonicalUrl={canonicalUrl}
      />
      <Bredcrumps
        title={selectedPageInfo.title || 'Категория...'}
        additionalRoutes={selectedPageInfo.additionalRoutes}
      />
      {isSelectedProductPage ? (
        <SelectedProductPage productInfo={selectedPageInfo} />
      ) : (
        <SelectedCablePage selectedPageInfo={selectedPageInfo} catalogInfo={catalogInfo} />
      )}
    </>
  );
}

export const getServerSideProps = async (context) => {
  const { query, resolvedUrl } = context;
  const isSelectedProductPage = !resolvedUrl.includes('[') && resolvedUrl.split('/').length > 3;
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  let catalogInfo = null;
  let selectedPageInfo = null;

  if (isSelectedProductPage) {
    /* СТРАНИЦА ВЫБРАННОГО ТОВАРА */
    const slugSelectedProduct = query.slug;
    const productInfo = await fetchSelectedProduct(slugSelectedProduct);
    //console.log('resCategoryInfo===', productInfo);
    if (!productInfo?.category) {
      console.error('productInfo==', productInfo);
      return {
        notFound: true,
      };
    }
    selectedPageInfo = {
      ...productInfo,
      additionalRoutes: [
        {
          href: '/cable',
          label: 'Каталог',
        },
        {
          href: '/cable/' + productInfo?.category?.slug || '/',
          label: productInfo?.category?.plural_name || 'mope',
        },
        {
          href: '/cable/' + productInfo?.mark.slug || '/',
          label: productInfo?.mark.name || 'mope',
        },
      ],
      title: productInfo?.raw_name,
      isMark: false,
    };
  } else {
    const resCategoryInfo = await fetchSelectedCategory(query.slug);
    //console.log('resCategoryInfo=fetchSelectedCategory==', resCategoryInfo);
    if (resCategoryInfo?.length === undefined) {
      console.error('fetchSelectedCategory===', resCategoryInfo);
      return {
        notFound: true,
      };
    }

    if (resCategoryInfo.length) {
      /* СТРАНИЦА ВЫБРАННОЙ КАТЕГОРИИ*/
      const resF = await filterCatalogList({
        paramsFilter: '&categorySlug=' + query.slug,
        page: query.page || 1,
        core: query.core || 0,
        section: query.section || 0,
        per_page: 10,
      });

      if (!resF?.data || !resCategoryInfo[0]) {
        console.error('fetchSelectedProductListByCategory===', resF);
        return {
          notFound: true,
        };
      }
      catalogInfo = { ...resF };
      selectedPageInfo = {
        ...selectedPageInfo,
        ...resCategoryInfo[0],
        additionalRoutes: [
          {
            href: '/cable',
            label: 'Каталог',
          },
        ],
        title: resCategoryInfo[0]?.categories?.plural_name,
      };
    } else {
      /* СТРАНИЦА ВЫБРАННОЙ МАРКИ */
      const resMarkInfo = await fetchSelectedMark({
        markSlug: query.slug,
      });
      //console.log('resMarkInfo===', resMarkInfo);
      if (!resMarkInfo?.data[0]) {
        console.error('fetchSelectedMark===', resMarkInfo);
        return {
          notFound: true,
        };
      }

      const resCatalogList = await filterCatalogList({
        paramsFilter: '&markSlug=' + query.slug,
        page: query.page || 1,
        core: query.core || 0,
        section: query.section || 0,
        per_page: 10,
      });
      //console.log('resCatalogList===', resCatalogList);
      if (!resCatalogList?.data) {
        console.error('filterCatalogList==', resCatalogList);
        return {
          notFound: true,
        };
      }
      const markInfo = resMarkInfo?.data[0];
      const markPopularInfo = resMarkInfo?.data[1];

      selectedPageInfo = {
        ...markInfo,
        ...markPopularInfo,
        additionalRoutes: [
          {
            href: '/cable',
            label: 'Каталог',
          },
          {
            href: '/cable/' + markInfo.categories[0].slug || '/',
            label: markInfo.categories[0].plural_name || 'nope',
          },
        ],
        title: markInfo.marks.name,
        isMark: true,
      };
      catalogInfo = { ...resCatalogList };
    }
  }
  return {
    props: { canonicalUrl, selectedPageInfo, catalogInfo, isSelectedProductPage },
  };
};
