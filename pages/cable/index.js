import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import makeRequestXHR from '@/shared/api/http';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const CablePage = dynamic(() => import('../../src/pages/Cable/CablePage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function SelectedCategory(props) {
  const { canonicalUrl, catalogList = {} } = props;
  return (
    <>
      <Head
        title={'Каталог кабельной продукции - Телмарк'}
        descriptionText="Кабельно-проводниковая продукция. Низкие цены за метр. Делаем отгрузки по всей России. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />
      <Bredcrumps title="Каталог" />
      <CablePage catalogList={catalogList} />
    </>
  );
}

export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const res = await makeRequestXHR('get', {
    url: '/categories/group',
    params: {
      per_page: 30,
    },
  });

  return { props: { canonicalUrl, catalogList: res.data } };
};
