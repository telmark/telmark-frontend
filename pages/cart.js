import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const CartPage = dynamic(() => import('../src/pages/Cart/CartPage'), {
  ssr: false,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Cart(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head title="Заказ - Телмарк" descriptionText="Заказ - Телмарк" canonicalUrl={canonicalUrl} />

      <Bredcrumps title="Корзина" />
      <CartPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
