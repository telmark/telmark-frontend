import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const CertificatesPage = dynamic(() => import('../src/pages/Certificates/CertificatesPage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Certificates(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head
        title="Сертификаты"
        descriptionText="Сертификаты и паспорта на кабель. Делаем отгрузки кабельной продукции по всей России. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="Сертификаты" />
      <CertificatesPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
