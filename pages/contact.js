import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchCurrentCity } from '@/features/select-city/model/api';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const ContactPage = dynamic(() => import('../src/pages/Contact/ContactPage'), {
  ssr: false,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Contact(props) {
  const { canonicalUrl, currentCityObj } = props;
  const title = `Контакты в городе ${
    currentCityObj.currentCity ? currentCityObj.currentCity?.name_prepositional : 'Санкт-Петербурге'
  }`;

  return (
    <>
      <Head
        title={title}
        descriptionText="Вы можете связаться с компанией Телмарк любым удобным для вас способом."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="Контакты" />
      <ContactPage title={title} isMainCity={currentCityObj?.currentCity?.is_main} />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const currentCityObj = await fetchCurrentCity();
  return {
    props: { canonicalUrl, currentCityObj },
  };
};
