import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchCurrentCity } from '@/features/select-city/model/api';
import makeRequestXHR from '@/shared/api/http';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const HomePage = dynamic(() => import('../src/pages/Home/HomePage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Index(props) {
  const { canonicalUrl, res, currentCityObj } = props;
  const desc = `Кабельно-проводниковая продукция в ${
    currentCityObj.currentCity ? currentCityObj.currentCity.name_prepositional : 'России'
  }`;
  return (
    <>
      <Head
        title="Купить кабель и провод по оптовым ценам в наличии 368 195 позиций"
        descriptionText={desc}
        canonicalUrl={canonicalUrl}
      />
      <HomePage catalogCategory={res} />
    </>
  );
}

export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const res = await makeRequestXHR('get', {
    url: '/categories/popular-by-types',
    params: {
      per_page: 30,
    },
  });
  const currentCityObj = await fetchCurrentCity();
  return { props: { canonicalUrl, res: res.data, currentCityObj } };
};
