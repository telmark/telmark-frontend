import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

import almaz from '../src/shared/images/objects/almaz.jpg';
import cran from '../src/shared/images/objects/cran.jpg';
import cron from '../src/shared/images/objects/cron.jpg';
import gaz from '../src/shared/images/objects/gazprom.jpg';
import gaz2 from '../src/shared/images/objects/gazprom.jpg';
import piter from '../src/shared/images/objects/piter.jpg';
import ural from '../src/shared/images/objects/ural.jpg';
import vergaz from '../src/shared/images/objects/vergaz.jpg';

const ObjectsPage = dynamic(() => import('../src/pages/Objects/ObjectsPage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Objects(props) {
  const { canonicalUrl, arr } = props;
  return (
    <>
      <Head
        title="Объекты - Телмарк"
        descriptionText="Объекты, на которые мы поставляем кабельно-проводниковую продукцию. Делаем отгрузки кабельной продукции по всей России. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="Объекты" />
      <ObjectsPage list={arr} />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const arr = [
    {
      id: '111',
      img: gaz.src,
      title: 'АО Газпромнефть-ОНПЗ',
    },
    {
      id: '211',
      img: almaz.src,
      title: 'АО Концерн ВКО Алма́з-Анте́й',
    },
    {
      id: '311',
      img: vergaz.src,
      title: 'АО СПБВЕРГАЗ',
    },
    {
      id: '411',
      img: gaz2.src,
      title: 'АО ТРАНСНЕФТЬ-ПОДВОДСЕРВИС',
    },
    {
      id: '511',
      img: ural.src,
      title: 'АО УРАЛВАГОНЗАВОД',
    },
    {
      id: '611',
      img: piter.src,
      title: 'ГУП Петербургский метрополитен',
    },
    {
      id: '711',
      img: cran.src,
      title: 'ЗБУ АВТОКРАТ',
    },
    {
      id: '811',
      img: cron.src,
      title: 'Кронштадтский Морской завод Минобороны РФ',
    },
  ];
  return {
    props: { canonicalUrl, arr },
  };
};
