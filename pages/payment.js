import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const PaymentDeliveryPage = dynamic(
  () => import('../src/pages/PaymentDelivery/PaymentDeliveryPage'),
  {
    ssr: true,
    loading: () => <SkeletonLayout />,
  },
);

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function PaymentDelivery(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head
        title="Оплата и доставка - Телмарк"
        descriptionText="Информация по способам оплаты и доставки кабельной продукции по всей России. Низкие цены за метр. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />

      <Bredcrumps title="Доставка и оплата" />
      <PaymentDeliveryPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
