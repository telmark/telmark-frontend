import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { fetchCurrentCity } from '@/features/select-city/model/api';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const PrivacyPolicyPage = dynamic(() => import('../src/pages/PrivacyPolicy/PrivacyPolicyPage'), {
  ssr: false,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function PrivacyPolicy(props) {
  const { canonicalUrl, currentCityObj } = props;
  const title = `Политика конфиденциальности в городе ${
    currentCityObj.currentCity ? currentCityObj.currentCity?.name_prepositional : 'Санкт-Петербурге'
  }`;

  return (
    <>
      <Head title={title} descriptionText="" canonicalUrl={canonicalUrl} />

      <Bredcrumps title="Политика конфиденциальности" />
      <PrivacyPolicyPage
        title="Политика в отношении обработки персональных данных"
        isMainCity={currentCityObj?.currentCity?.is_main}
      />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const currentCityObj = await fetchCurrentCity();
  return {
    props: { canonicalUrl, currentCityObj },
  };
};
