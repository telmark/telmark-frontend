import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const ProviderPage = dynamic(() => import('../src/pages/Provider/ProviderPage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Provider(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head
        title="Поставщикам, заводам и производителям кабельно-проводниковой продукции - Телмарк"
        descriptionText="Информация поставщикам кабеля. Делаем отгрузки кабельной продукции по всей России. Весь товар сертифицирован."
        canonicalUrl={canonicalUrl}
      />
      <Bredcrumps title="Поставщики" />
      <ProviderPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
