import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const ReviewsPage = dynamic(() => import('../src/pages/Reviews/ReviewsPage'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

const Bredcrumps = dynamic(() => import('../src/entities/common/bredcrumps'), {
  ssr: true,
  loading: () => <SkeletonLayout />,
});

export default function Reviews(props) {
  const { canonicalUrl } = props;
  return (
    <>
      <Head
        title="Отзывы"
        descriptionText="Полезные отзывы о работе с компанией Телмарк. Ждем вас в рядах наших партнеров!"
        canonicalUrl={canonicalUrl}
      />
      <Bredcrumps title="Отзывы" />
      <ReviewsPage />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  return {
    props: { canonicalUrl },
  };
};
