import makeRequestXHR from '@/shared/api/http';

export default async function Robots() {}

Robots.getInitialProps = async ({ req, res }) => {
  const resR = await makeRequestXHR('get', {
    url: '/common/robot',
    params: {
      domain: req.headers.host,
    },
  });
  res.setHeader('Content-Type', 'text/plain');
  res.write(resR);
  res.end();
};
