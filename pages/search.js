import dynamic from 'next/dynamic';

import SkeletonLayout from '@/entities/common/skeleton-layout';
import Head from '@/entities/seo/Head';
import { doFetchSearch } from '@/features/search-panel/model';
import { createCanonicalUrl } from '@/shared/utils';
import { addHeaders } from '@/shared/utils';

const SearchPage = dynamic(() => import('../src/pages/Search/SearchPage'), {
  ssr: false,
  loading: () => <SkeletonLayout />,
});

export default function Search(props) {
  const { canonicalUrl, marks, products, queryStr } = props;
  return (
    <>
      <Head
        title="Результат поиска"
        descriptionText="Результат поиска"
        canonicalUrl={canonicalUrl}
      />
      <SearchPage marks={marks} products={products} queryStr={queryStr} />
    </>
  );
}
export const getServerSideProps = async (context) => {
  const canonicalUrl = createCanonicalUrl(context);
  addHeaders(context);
  const { query } = context;
  const res = await doFetchSearch(query);
  if (!res.data?.marks || !res.data?.products) {
    return {
      notFound: true,
    };
  }
  const marks = res.data.marks;
  const products = res.data.products;
  const queryStr = res.data.query;

  return {
    props: { canonicalUrl, marks, products, queryStr },
  };
};
