import makeRequestXHR from '@/shared/api/http';

export default async function SitemapXML() {}

SitemapXML.getInitialProps = async ({ req, res }) => {
  const resSitemap = await makeRequestXHR('get', {
    url: '/sitemaps/list',
    params: {
      domain: req.headers.host,
    },
  });
  res.setHeader('Content-Type', 'text/xml');
  res.write(resSitemap);
  res.end();
};
