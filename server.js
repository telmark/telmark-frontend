const { createServer } = require('http');
const { parse } = require('url');
const next = require('next');
const axios = require('axios');
const nextConfig = require('./next.config');

const isDev = process.env.NODE_ENV !== 'production';

const port = 3000;

const app = next({ dev: isDev, ...nextConfig });
const handle = app.getRequestHandler();
const baseUrl = process.env.NEXT_PUBLIC_API_URL;

axios.defaults.baseURL = baseUrl;
axios.defaults.headers = {
  'Access-Control-Allow-Origin': '*',
};
axios.defaults.withCredentials = true;

app.prepare().then(() => {
  const hostname = process.env.NEXT_PUBLIC_HOSTNAME;
  createServer(async (req, res) => {
    try {
      // Be sure to pass `true` as the second argument to `url.parse`.
      // This tells it to parse the query portion of the URL.
      const parsedUrl = parse(req.url, true);
      const { pathname } = parsedUrl;
      const regExp = /\d+/gm;
      if (regExp.test(pathname) && pathname.includes('sitemap')) {
        const sitemapParam = pathname.replace('/', '');
        const resSitemap = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/sitemaps/sitemap`, {
          params: {
            domain: req.headers.host,
            sitemap: sitemapParam,
          },
          headers: { 'Content-Type': 'text/xml' },
        });
        res.setHeader('Content-Type', 'text/xml');
        res.write(resSitemap.data);
        res.end();
      } else {
        await handle(req, res, parsedUrl);
      }
    } catch (err) {
      console.error('Error occurred handling', req.url, err);
      res.statusCode = 500;
      res.end(`internal server error ${err}`);
    }
  })
    .once('error', (err) => {
      console.error(err);
      process.exit(1);
    })
    .listen(port, () => {
      console.log(`> Ready on https://${hostname}:${port}`);
    });
});
