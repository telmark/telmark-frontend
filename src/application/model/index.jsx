import { configure } from 'mobx';
import { enableStaticRendering } from 'mobx-react-lite';
import { createContext } from 'react';

import CartStore from '@/features/cart/model/store';
import CityStore from '@/features/select-city/model';
import TokenStore from '@/features/token/model';

configure({ enforceActions: 'observed' });
enableStaticRendering(typeof window === 'undefined');

class RootStore {
  constructor() {
    this.cart = new CartStore();
    this.token = new TokenStore();
    this.city = new CityStore();
  }
  hydrate = (data) => {
    if (!data) return;
  };
}

export const store = new RootStore();

export const GlobalContext = createContext();

function initializeStore(initialData = null) {
  const _store = store;

  // If your page has Next.js data fetching methods that use a Mobx store, it will
  // get hydrated here, check `pages/ssg.js` and `pages/ssr.js` for more details
  if (initialData) {
    _store.hydrate(initialData);
  }
  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') {
    return _store;
  }

  return _store;
}

export function GlobalContextProvider(props) {
  const { children, hydrationData } = props;
  const hydrationStore = initializeStore(hydrationData);
  return <GlobalContext.Provider value={hydrationStore}>{children}</GlobalContext.Provider>;
}
