import { observer } from 'mobx-react-lite';
import dynamic from 'next/dynamic';
import { Suspense, useEffect, useState } from 'react';

import { fetchCurrentCity } from '@/features/select-city/model/api';
import { useGlobalStore } from '@/shared/hooks';
import { ContainerInner } from '@/shared/ui';
import Skeleton from '@/shared/ui/Skeleton';

import s from './Layout.module.scss';

const Footer = dynamic(() => import('@/widgets/footer'), {
  ssr: false,
});

const Header = dynamic(() => import('@/widgets/header/Header'), {
  ssr: true,
  loading: () => <Skeleton quantityElement={1} count={1} height={140} />,
});

const Sbis = dynamic(() => import('@/widgets/sbis'), {
  ssr: false,
});

const ArrowTop = dynamic(() => import('@/widgets/arrow-top'), {
  ssr: false,
});

export const Layout = observer((props) => {
  const { children, className } = props;
  const { token, city } = useGlobalStore();
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        await token.fetchCsrfToken();
      } catch (e) {
        console.error('e=fetchCsrfToken', e);
      }
    })();
  }, [token]);

  useEffect(() => {
    if (window !== undefined) {
      if (window.matchMedia('(max-width: 997px)').matches) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    }
  }, []);

  useEffect(() => {
    (async () => {
      try {
        const res = await fetchCurrentCity();
        city.setCurrentCity(res);
      } catch (e) {
        console.error('e=fetchCurrentCity', e);
      }
    })();
  }, [city]);

  return (
    <>
      <div className={`${s.wrapper} ${className}`}>
        <Header />
        <main className={s.main}>
          <ContainerInner>{children}</ContainerInner>
        </main>
        {!isMobile ? (
          <Suspense>
            <Sbis />
          </Suspense>
        ) : null}
        <ArrowTop />
        <Footer />
      </div>
    </>
  );
});
