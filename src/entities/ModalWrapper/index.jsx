import Alert from '@/entities/alerts/Alert';
import { useSubmitForm } from '@/shared/hooks';
import Modal from '@/shared/ui/Modal';
import Preloader from '@/shared/ui/Preloader';

/**
 * @ModalWrapper
 * @param {boolean} isOpen - статус состояния
 * @param {function} handlerModalClose - 'функция закрытия модалки
 * @param {function} handler - фнкция отправки
 * @param {component} ContentModalForm - комопнент внутри модалки with onSubmitHandler props
 */

const ModalWrapper = (props) => {
  const {
    isOpen,
    handlerModalClose,
    handler,
    ContentModalForm,
    textMessage,
    formData,
    setFormData,
  } = props;
  const { isLoading, isError, isSuccess, trigger, resetFormStatuses } = useSubmitForm(handler);

  return (
    <>
      {isLoading ? <Preloader /> : null}
      <Modal
        isOpen={isOpen}
        handlerClose={() => {
          handlerModalClose();
          resetFormStatuses();
          setFormData({});
        }}
        content={
          isSuccess || isError ? (
            <Alert
              type={isSuccess ? 'success' : isError ? 'error' : ''}
              textMessage={textMessage}
              callbackOnClose={() => {
                handlerModalClose();
                resetFormStatuses();
              }}
            />
          ) : (
            <ContentModalForm
              onSubmitHandler={trigger}
              defaultValues={formData}
              setFormValues={setFormData}
            />
          )
        }
      />
    </>
  );
};

export default ModalWrapper;
