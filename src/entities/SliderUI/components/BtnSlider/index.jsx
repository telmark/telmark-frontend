import leftArrow from '@/shared/images/general/next.png';
import rightArrow from '@/shared/images/general/prev.png';

import s from './BtnSlider.module.scss';

export default function BtnSlider({ direction, moveSlide }) {
  return (
    <button
      onClick={moveSlide}
      className={direction === 'next' ? `${s.btn} ${s.next}` : `${s.btn} ${s.prev}`}
    >
      <img src={direction === 'next' ? rightArrow : leftArrow} alt="s" />
    </button>
  );
}
