import s from './ItemSlider.module.scss';

export default function ItemSlider({ children, index, slideIndex }) {
  return (
    <div className={slideIndex === index + 1 ? `${s.slide} ${s.active}` : s.slide}>{children}</div>
  );
}
