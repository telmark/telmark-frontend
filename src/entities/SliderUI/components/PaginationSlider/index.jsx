import s from './PaginationSlider.module.scss';

export default function PaginationSlider({ moveDot, slideIndex }) {
  return (
    <div className="container-dots">
      {Array.from({ length: 5 }).map((item, index) => (
        <div
          key={index}
          onClick={() => moveDot(index + 1)}
          className={slideIndex === index + 1 ? 'dot active' : 'dot'}
        ></div>
      ))}
    </div>
  );
}
