import { useState } from 'react';

import s from './SliderUI.module.scss';
import BtnSlider from './components/BtnSlider';
import ItemSlider from './components/ItemSlider';
import PaginationSlider from './components/PaginationSlider';

const dataSlider = [{ id: '1' }, { id: '34' }, { id: '455' }];

export default function SliderUI(props) {
  const { isPagination = true, isBtns = true } = props;
  const [slideIndex, setSlideIndex] = useState(1);

  const nextSlide = () => {
    if (slideIndex !== dataSlider.length) {
      setSlideIndex(slideIndex + 1);
    } else if (slideIndex === dataSlider.length) {
      setSlideIndex(1);
    }
  };

  const prevSlide = () => {
    if (slideIndex !== 1) {
      setSlideIndex(slideIndex - 1);
    } else if (slideIndex === 1) {
      setSlideIndex(dataSlider.length);
    }
  };

  const moveDot = (index) => {
    setSlideIndex(index);
  };

  return (
    <div className={s.container}>
      {dataSlider.map((obj, index) => {
        return (
          <ItemSlider key={obj.id} index={index} slideIndex={slideIndex}>
            adsasd
          </ItemSlider>
        );
      })}
      {isBtns ? (
        <>
          <BtnSlider moveSlide={nextSlide} direction={'next'} />
          <BtnSlider moveSlide={prevSlide} direction={'prev'} />
        </>
      ) : null}

      {isPagination ? <PaginationSlider moveDot={moveDot} slideIndex={slideIndex} /> : null}
    </div>
  );
}
