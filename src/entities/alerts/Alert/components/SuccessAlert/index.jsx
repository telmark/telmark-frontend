import { Button, Typography } from '@/shared/ui';

import s from './SuccessAlert.module.scss';

const SuccessAlert = (props) => {
  const { callbackOnClose, textMessage = 'Заявка отправлена' } = props;

  return (
    <div className={s.wrapper}>
      <svg
        width="60"
        height="60"
        viewBox="0 0 60 60"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_2847_3610)">
          <path
            d="M30 0C13.4315 0 0 13.4315 0 30C0 46.5686 13.4315 60 30 60C46.5686 60 60 46.5686 60 30C60 13.4315 46.5686 0 30 0Z"
            fill="#01B76D"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M46.8287 18.7526C47.6461 19.57 47.6461 20.8952 46.8287 21.7126L27.2938 41.2474C26.9014 41.64 26.369 41.8605 25.8139 41.8605C25.2588 41.8605 24.7264 41.64 24.3339 41.2474L13.1711 30.0846C12.3538 29.2672 12.3538 27.9422 13.1711 27.1248C13.9885 26.3074 15.3137 26.3074 16.1311 27.1248L25.8139 36.8074L43.8689 18.7526C44.6863 17.9352 46.0113 17.9352 46.8287 18.7526Z"
            fill="white"
          />
        </g>
        <defs>
          <clipPath id="clip0_2847_3610">
            <rect width="60" height="60" fill="white" />
          </clipPath>
        </defs>
      </svg>
      <Typography variant="title_sm" boldness="medium">
        {textMessage}
      </Typography>
      <Typography boldness="medium">Менеджер перезвонит вам в течении 15 минут</Typography>
      <Button onClick={callbackOnClose} title="Перейти на главную">
        Перейти на главную
      </Button>
    </div>
  );
};

export default SuccessAlert;
