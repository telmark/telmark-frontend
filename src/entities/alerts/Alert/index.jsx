import { useRouter } from 'next/navigation';

import ErrorAlert from './components/ErrorAlert';
import SuccessAlert from './components/SuccessAlert';

export default function Alert(props) {
  const { type, callbackOnClose, textMessage } = props;
  const router = useRouter();

  const handler = () => {
    router.push('/');
    callbackOnClose();
  };

  return (
    <>
      {type === 'success' ? (
        <SuccessAlert callbackOnClose={handler} textMessage={textMessage} />
      ) : (
        <ErrorAlert callbackOnClose={handler} textMessage={textMessage} />
      )}
    </>
  );
}
