import { useCallback, useState } from 'react';

import Modal from '@/shared/ui/Modal';

import Alert from './Alert';

/**
 * @AlertModal
 * @param {string} type - 'success' | 'error' | ''
 * @param {boolean} defaultStatus  for modal
 * @param {function} callbackOnClose  callback func onClose action
 */
//TODO: заменить на общую обертку модалки с логикой
const AlertModal = (props) => {
  const { type = '', defaultStatus = true, callbackOnClose = () => {}, textMessage } = props;
  const [isOpen, setOpen] = useState(defaultStatus);
  const onClose = useCallback(() => {
    setOpen(false);
    callbackOnClose();
  }, [callbackOnClose]);
  if (type === '') {
    return null;
  }
  return (
    <Modal
      isOpen={isOpen}
      handlerClose={onClose}
      content={<Alert type={type} textMessage={textMessage} callbackOnClose={onClose} />}
    />
  );
};
export default AlertModal;
