import { observer } from 'mobx-react-lite';
import Link from 'next/link';
import { useState } from 'react';

import { useGlobalStore } from '@/shared/hooks';
import { Button, Typography } from '@/shared/ui';
import Modal from '@/shared/ui/Modal';

import s from './ActionError.module.scss';

const ContentModal = (props) => {
  const { callbackOnClose } = props;
  return (
    <div className={s.wrapper}>
      <svg
        width="60"
        height="60"
        viewBox="0 0 60 60"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_2683_6944)">
          <path
            d="M30 0C13.4531 0 0 13.4531 0 30C0 46.5469 13.4531 60 30 60C46.5469 60 60 46.5469 60 30C60 13.4531 46.5469 0 30 0ZM27.4594 11.1C27.4594 9.69375 28.6031 8.56875 30 8.56875C31.4062 8.56875 32.5406 9.69375 32.5406 11.1V36.0094C32.5406 37.4156 31.3969 38.5406 30 38.5406C28.5938 38.5406 27.4594 37.4156 27.4594 36.0094V11.1ZM30 50.1938C27.9844 50.1938 26.3344 48.5812 26.3344 46.5938C26.3344 44.5969 27.975 42.9844 30 42.9844C32.0156 42.9844 33.6656 44.5969 33.6656 46.5938C33.6656 48.5812 32.0156 50.1938 30 50.1938Z"
            fill="#F04844"
          />
        </g>
        <defs>
          <clipPath id="clip0_2683_6944">
            <rect width="60" height="60" fill="white" />
          </clipPath>
        </defs>
      </svg>
      <Typography variant="title_sm" boldness="medium">
        Произошла ошибка!
      </Typography>
      <Typography boldness="medium">
        Повторите попытку снова
        <br /> или свяжитесь с нами по контактам, которые указаны на сайте.
      </Typography>
      <Link className={s.link} href="tel:88006004091">
        <Typography variant="title_sm" boldness="medium">
          8 800 600 40 91
        </Typography>
      </Link>
      <Button onClick={callbackOnClose} title="Закрыть">
        Закрыть
      </Button>
    </div>
  );
};

const ActionError = observer(() => {
  const [isOpen, setOpen] = useState(true);
  const { cart } = useGlobalStore();
  const onClose = () => {
    setOpen(false);
    cart.setCartState();
  };

  return (
    <Modal
      className={s.dialog}
      isOpen={isOpen}
      handlerClose={onClose}
      content={<ContentModal callbackOnClose={onClose} />}
    />
  );
});

export default ActionError;
