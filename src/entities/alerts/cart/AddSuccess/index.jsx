import { Button, Typography } from '@/shared/ui';
import Modal from '@/shared/ui/Modal';

import s from './AddSuccess.module.scss';

const ContentModal = (props) => {
  const { callbackOnClose } = props;
  return (
    <div className={s.wrapper}>
      <Typography variant="title_sm" boldness="medium">
        Товар добавлен
      </Typography>
      <Button
        type="link"
        to="/cart"
        className={s.btn}
        onClick={callbackOnClose}
        title="Перейти в корзину"
      >
        Перейти в корзину
      </Button>
      <Button color="gray" className={s.btn} onClick={callbackOnClose} title="Перейти на главную">
        Продолжить покупки
      </Button>
    </div>
  );
};

const AddSuccess = (props) => {
  const { isOpen, callbackOnClose } = props;

  return (
    <Modal
      className={s.dialog}
      isOpen={isOpen}
      handlerClose={callbackOnClose}
      content={<ContentModal callbackOnClose={callbackOnClose} />}
    />
  );
};

export default AddSuccess;
