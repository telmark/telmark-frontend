import { observer } from 'mobx-react-lite';
import React from 'react';

import { useGlobalStore } from '@/shared/hooks';

import s from './CartTotal.module.scss';

const CartTotal = observer(() => {
  const { cart } = useGlobalStore();
  return (
    <div className={s.cartTotal}>
      Итого: <span>{cart.totalPrice} ₽</span>
    </div>
  );
});

export default CartTotal;
