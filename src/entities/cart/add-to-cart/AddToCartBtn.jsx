import { observer } from 'mobx-react-lite';

import { Button } from '@/shared/ui';

import s from './AddToCartBtn.module.scss';

/**
 * @AddToCartBtn
 * @param {string} handler - onClick;
 * @param {boolean} isInCart - наличие в корзине;
 */

export const AddToCartBtn = observer((props) => {
  const { handler, isInCart } = props;
  const text = isInCart ? 'В корзине' : 'В корзину';
  return (
    <Button
      className={`${isInCart ? s.inCartBtn : ''} ${s.add_to_cartBtn}`}
      title={text}
      onClick={handler}
    >
      {text}
      <svg
        className={s.add_to_cartBtn__icon}
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_899_910)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0 1.125C0 0.50368 0.503679 0 1.125 0H4.5C5.01622 0 5.4662 0.351335 5.59141 0.852147L6.50337 4.5H22.875C23.2187 4.5 23.5435 4.6571 23.7569 4.92652C23.9703 5.19594 24.0488 5.54812 23.9701 5.88267L21.5143 16.3198C21.0758 18.183 19.4132 19.5 17.499 19.5H11.3092C9.43754 19.5 7.80048 18.2398 7.32155 16.4306L4.53348 5.89788L3.62162 2.25H1.125C0.503679 2.25 0 1.74632 0 1.125ZM7.08654 6.75L9.49663 15.8548C9.71432 16.6772 10.4585 17.25 11.3092 17.25H17.499C18.369 17.25 19.1248 16.6514 19.3241 15.8044L21.4546 6.75H7.08654Z"
          />
          <path d="M9 24C9.82842 24 10.5 23.3284 10.5 22.5C10.5 21.6716 9.82842 21 9 21C8.17157 21 7.5 21.6716 7.5 22.5C7.5 23.3284 8.17157 24 9 24Z" />
          <path d="M19.5 24C20.3284 24 21 23.3284 21 22.5C21 21.6716 20.3284 21 19.5 21C18.6716 21 18 21.6716 18 22.5C18 23.3284 18.6716 24 19.5 24Z" />
        </g>
        <defs>
          <clipPath id="clip0_899_910">
            <rect width="24" height="24" />
          </clipPath>
        </defs>
      </svg>
    </Button>
  );
});
