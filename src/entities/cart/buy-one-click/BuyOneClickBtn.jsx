import { Button } from '@/shared/ui';

import s from './BuyOneClickBtn.module.scss';

export const BuyOneClickBtn = (props) => {
  const { handler = () => console.log('worked BuyOneClickBtn') } = props;

  return (
    <Button className={s.buy_one_clickBtn} onClick={handler} titie="Купить в 1 клик">
      Купить в 1 клик
    </Button>
  );
};
