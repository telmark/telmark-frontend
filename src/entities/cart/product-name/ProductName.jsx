import Image from 'next/image';
import Link from 'next/link';
import { memo } from 'react';

import defaultImg from '@/shared/images/category_default.jpg';
import { Typography } from '@/shared/ui';

import s from './ProductName.module.scss';

export const ProductName = memo((props) => {
  const { element } = props;
  return (
    <Link className={s.product__wrapper} href={element.url}>
      <div className={s.product__img}>
        <Image src={defaultImg} width="86" height="86" alt="img" />
      </div>
      <Typography variant="text_xl" boldness="medium" className={s.product__name}>
        {element.name}
      </Typography>
    </Link>
  );
});
