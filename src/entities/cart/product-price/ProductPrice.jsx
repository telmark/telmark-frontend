import { memo } from 'react';

import { Button, Typography } from '@/shared/ui';

export const ProductPrice = memo((props) => {
  const { element, handler } = props;

  if (element.price) {
    const productPrice = element.price?.toFixed(2);
    return (
      <Typography variant="text_xl" boldness="medium">
        {`${productPrice} ₽/метр`}
      </Typography>
    );
  }
  return (
    <Button title="Уточнить цену" onClick={handler}>
      Уточнить цену
    </Button>
  );
});
