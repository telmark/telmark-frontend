import { memo } from 'react';

import s from './ProductRemoveBtn.module.scss';

export const ProductRemoveBtn = memo((props) => {
  const { handler } = props;
  return (
    <button className={s.productRemove} title="Удалить товар" onClick={handler}>
      <svg
        width="13"
        height="13"
        viewBox="0 0 13 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M0.84375 1.55025L1.55086 0.84314L12.1575 11.4497L11.4504 12.1568L0.84375 1.55025Z"
          fill="#2559D9"
        />
        <path
          d="M11.4504 0.84314L12.1575 1.55025L1.55086 12.1568L0.84375 11.4497L11.4504 0.84314Z"
          fill="#2559D9"
        />
      </svg>
    </button>
  );
});
