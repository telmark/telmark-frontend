import { observer } from 'mobx-react-lite';

import { Typography } from '@/shared/ui';

export const ProductTotal = observer((props) => {
  const { element } = props;
  const _price = element.price || 0;
  const totalPrice = +(_price * element._quantity).toFixed();

  if (totalPrice) {
    return (
      <Typography variant="text_xl" boldness="medium">
        {totalPrice} ₽
      </Typography>
    );
  }
  return null;
});
