import Link from 'next/link';
import { memo } from 'react';

import s from './CatalogProductName.module.scss';

export const CatalogProductName = memo((props) => {
  const { productName, link = '' } = props;
  return (
    <Link itemProp="url" className="productTable-name" href={`/cable${link}`}>
      <span className={s.text} itemProp="name">
        {productName}
      </span>
    </Link>
  );
});
