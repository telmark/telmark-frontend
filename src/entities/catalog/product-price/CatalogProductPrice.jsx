import { memo } from 'react';

import { Button } from '@/shared/ui';

export const CatalogProductPrice = memo((props) => {
  const { productPrice, modalFormHandler } = props;
  if (productPrice) {
    return (
      <div itemScope itemProp="offers" itemType="http://schema.org/Offer">
        &nbsp; &nbsp;
        <span itemProp="price" content={productPrice.toFixed()}>
          от {productPrice.toFixed(2)} руб/м
        </span>
        <meta itemProp="priceCurrency" content="RUB" />
      </div>
    );
  }

  return (
    <Button itemProp="price" title="Уточнить цену" onClick={modalFormHandler}>
      Уточнить цену
    </Button>
  );
});
