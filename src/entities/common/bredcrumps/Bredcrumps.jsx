import Link from 'next/link';
import { useRouter } from 'next/router';
import { memo } from 'react';

import s from './Bredcrumps.module.scss';

export const Bredcrumps = memo((props) => {
  const { additionalRoutes = [], title = 'not found' } = props;
  const rootRouteList = [
    {
      href: '/',
      label: 'Главная',
    },
    ...additionalRoutes,
  ];
  const router = useRouter();

  if (router.route === '/') {
    return null;
  }

  return (
    <nav className={s.breadcrumbs} aria-label="breadcrumb">
      <ol className={s.list} itemScope itemType="http://schema.org/BreadcrumbList">
        {rootRouteList.map((el, index) => {
          return (
            <li
              key={el.href}
              className={s.item}
              itemProp="itemListElement"
              itemScope
              itemType="http://schema.org/ListItem"
            >
              <Link itemProp="item" href={el.href} className={s.link}>
                <span itemProp="name">{el.label}</span>
              </Link>
              <meta itemProp="position" content={index + 1} />
            </li>
          );
        })}
        <li key={title} className={s.item}>
          <a itemProp="item">
            <span itemProp="name">{title}</span>
          </a>
          <meta itemProp="position" content={rootRouteList.length + 1} />
        </li>
      </ol>
    </nav>
  );
});
