import debounce from 'debounce';
import { memo, useRef, useState } from 'react';

import s from './CountBlock.module.scss';

/**
 * @CountBlock
 * @prop {object} element
 * @param {function} setUpdateCount сеттер useState
 * @param {function} callback - добавочное действие
 */
const debouncedCallback = debounce((cb, v) => {
  cb(v);
}, 400);

export const CountBlock = memo((props) => {
  const { element = { _quantity: 1 }, setUpdateCount = () => {}, callback } = props;
  const inputRef = useRef(null);
  const [oldQuantity, setOldQuantity] = useState(() => element._quantity);

  const onIncrement = () => {
    if (typeof callback === 'function') {
      callback(++element._quantity);
    } else {
      setUpdateCount((prev) => {
        return { ...prev, _quantity: prev._quantity + 1 };
      });
    }
  };

  const onDecrement = () => {
    if (typeof callback === 'function') {
      callback(--element._quantity);
    } else {
      setUpdateCount((prev) => {
        if (prev._quantity === 1) {
          return prev;
        }
        return { ...prev, _quantity: prev._quantity - 1 };
      });
    }
  };

  const onInput = (e) => {
    inputRef.current.value = inputRef.current.value.replace(/^0+/, '');
    setUpdateCount((prev) => {
      return { ...prev, _quantity: e.target.value };
    });
  };

  const onBlur = () => {
    inputRef.current.value = inputRef.current.value.replace(/^0+/, '');
    if (inputRef.current.value === '') {
      setUpdateCount((prev) => {
        return { ...prev, _quantity: 1 };
      });
      if (oldQuantity !== 1) {
        if (typeof callback === 'function') {
          setOldQuantity(1);
          debouncedCallback(callback, 1);
        }
      }
    } else {
      if (+inputRef.current.value !== oldQuantity) {
        if (typeof callback === 'function') {
          setOldQuantity(+inputRef.current.value);
          debouncedCallback(callback, +inputRef.current.value);
        }
      }
    }
  };

  return (
    <div className={s.countBlock}>
      <button className={s.count__btn} title="Убавить" onClick={onDecrement}></button>
      <label className={s.label}>
        <input
          ref={inputRef}
          name="size"
          type="number"
          className={s.input}
          aria-label="count"
          value={element._quantity}
          onChange={onInput}
          onBlur={onBlur}
        />
        <span className={s.nominal}>м</span>
      </label>
      <button
        className={`${s.count__btn} ${s.count__btnPlus}`}
        title="Добавить"
        onClick={onIncrement}
      ></button>
    </div>
  );
});
