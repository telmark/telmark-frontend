import Image from 'next/image';
import Link from 'next/link';
import { memo } from 'react';

import logo from '@/shared/images/general/logo.webp';

import s from './Logo.module.scss';

export const Logo = memo(() => {
  return (
    <Link className={s.logo} href="/">
      <Image className={s.img} src={logo} width={183} height={53} alt="img" priority={true} />
    </Link>
  );
});
