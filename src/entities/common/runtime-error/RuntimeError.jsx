import { useState } from 'react';

import { Button, Typography } from '@/shared/ui';
import Modal from '@/shared/ui/Modal';

import s from './RuntimeError.module.scss';

/**
 * @RuntimeError runtime error into modal
 */

export const RuntimeError = () => {
  const [isOpen, setModal] = useState(true);

  return (
    <Modal
      isOpen={isOpen}
      handlerClose={() => {
        setModal(false);
      }}
      content={
        <div className={s.wrapper} role="alert">
          <Typography color="alert" variant="title_md">
            Внимание!
          </Typography>
          <Typography>
            На сайте ведутся технические работы, часть функционала недоступна, дополнительную
            информацию уточните по контактам:
            <div>
              Написать нам{' '}
              <a className={s.link} title="Почта" href="mailto:sale@telmark.ru">
                sale@telmark.ru
              </a>
            </div>
            <div>
              Заказать звонок{' '}
              <a className={s.link} title="Телефон" href="tel:+7(800)600-40-91">
                +7(800)600-40-91
              </a>
            </div>
          </Typography>
          <Button title="На главную" onClick={() => setModal(false)}>
            Закрыть
          </Button>
        </div>
      }
    />
  );
};
