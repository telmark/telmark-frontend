import Skeleton from '@/shared/ui/Skeleton';

export const SkeletonLayout = () => {
  return (
    <>
      <Skeleton quantityElement={1} count={1} height={30} width={250} />
      <br />
      <Skeleton quantityElement={1} count={1} height={30} />
      <br />
      <Skeleton quantityElement={3} count={11} />
    </>
  );
};
