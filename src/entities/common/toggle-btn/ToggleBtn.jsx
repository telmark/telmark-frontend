import classNames from 'classnames';

import { Typography } from '@/shared/ui';

import s from './ToggleBtn.module.scss';

/**
 * @ToggleBtn spoiler btn
 * @prop {string} textBtn
 * @prop {string} className
 * @prop {function} handler
 */

export const ToggleBtn = (props) => {
  const { textBtn, handler, className = '' } = props;
  const cls = classNames({
    [s.btn]: true,
    [`${className}`]: className,
  });
  return (
    <>
      <button className={cls} title={textBtn} onClick={handler}>
        <Typography variant="text_xl" boldness="medium" color="attention">
          {textBtn}
        </Typography>
      </button>
    </>
  );
};
