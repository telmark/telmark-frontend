import { useEffect } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import {
  Field,
  FieldEmail,
  FieldFile,
  FieldName,
  FieldPhone,
  FieldTextarea,
  PrivatePolicy,
} from '../components';

/**
 * @FormContext rfc Context
 * @param {string} className - className form
 * @param {function} handler - submit function
 * @param {children} children - react children
 */
//TODO: вынести форму и контекст
export default function FormContext(props) {
  const {
    className,
    children,
    handler,
    defaultValues = {},
    onBlurAction = false,
    hasReset = true,
    setFormValues,
  } = props;

  const methods = useForm({
    defaultValues,
    method: 'onChange',
  });

  const onSubmit = (data) => {
    handler(data);
    if (hasReset) {
      methods.reset();
    }
  };

  const onBlurForm = async () => {
    if (onBlurAction) {
      await methods.trigger();
      const values = methods.getValues();
      const state = methods.formState;
      if (!Object.keys(state.errors).length) {
        handler(values);
      }
    }
  };

  useEffect(() => {
    return () => {
      if (setFormValues) {
        const formValues = methods.getValues();
        setFormValues(formValues);
      }
    };
  }, [setFormValues, methods]);

  return (
    <FormProvider {...methods}>
      <form className={className} onSubmit={methods.handleSubmit(onSubmit)} onBlur={onBlurForm}>
        {children}
      </form>
    </FormProvider>
  );
}
/**
 * @Field
 * @param {string} type - 'text, email etc'
 * @param {string} name - name field
 * @param {string} textError - text in ErrorMessage
 * @param {string} placeholder - placeholder input
 * @param {object} validationOptions - rfc (required, pattern, minLength etc)
 */
FormContext.Field = Field;
FormContext.FieldName = FieldName;
FormContext.FieldPhone = FieldPhone;
FormContext.FieldEmail = FieldEmail;
/**
 * @PrivatePolicy rfc Component
 */
FormContext.PrivatePolicy = PrivatePolicy;
/**
 * @FieldTextarea
 * @param {string} name - name field
 * @param {string} placeholder - placeholder input
 * @param {string} className - placeholder input
 */
FormContext.FieldTextarea = FieldTextarea;
FormContext.FieldFile = FieldFile;
