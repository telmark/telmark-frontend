import { useFormContext } from 'react-hook-form';

import { ErrorMessage, Input, Label } from '@/shared/ui';

/**
 * @Field
 * @param {string} type - 'text, email phone. textarea'
 * @param {string} name - name field
 * @param {string} textError - text in ErrorMessage
 * @param {string} placeholder - placeholder input
 * @param {object} validationOptions - rfc (required, pattern, minLength etc)
 */

export default function Field(props) {
  const {
    type = 'text',
    name,
    className = '',
    onBlur = () => {},
    textError = 'Введите корректное значение',
    placeholder = 'Ваш значение',
    validationOptions = { required: true },
  } = props;
  const {
    register,
    formState: { errors },
  } = useFormContext();

  return (
    <>
      <Label>
        <Input
          classNameInput={className}
          type={type}
          rest={{
            'aria-required': 'true',
            'aria-invalid': errors && errors[name] ? 'true' : 'false',
            onBlur: onBlur,
          }}
          placeholder={placeholder}
          {...register(name, validationOptions)}
        />
      </Label>
      {errors[name] && <ErrorMessage text={textError} />}
    </>
  );
}
