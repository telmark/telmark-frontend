import { useFormContext } from 'react-hook-form';

import { ErrorMessage, Input, Label } from '@/shared/ui';

/**
 * @FieldEmail
 * @param {string} type - 'text, email phone. textarea'
 * @param {string} name - name field
 */

export default function FieldEmail(props) {
  const { type = 'email', name = 'email' } = props;
  const {
    register,
    trigger,
    formState: { errors },
  } = useFormContext();

  return (
    <>
      <Label>
        <Input
          type={type}
          rest={{
            onBlur: () => {
              trigger(name);
            },
          }}
          placeholder={'E-mail *'}
          {...register(name, { required: true, pattern: /^\S+@\S+$/i })}
        />
      </Label>
      {errors[name] && <ErrorMessage text={'Введите корректный email'} />}
    </>
  );
}
