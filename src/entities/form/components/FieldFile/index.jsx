import { useFormContext } from 'react-hook-form';

import { InputFile } from '@/shared/ui';

export default function FieldFile(props) {
  const { name = 'file' } = props;
  const { register, setValue, isSubmitSuccessful, getValues } = useFormContext();

  return (
    <InputFile
      {...register(name)}
      defaultValue={getValues(name)}
      isSubmitSuccessful={isSubmitSuccessful}
      setValue={setValue}
    />
  );
}
