import { useFormContext } from 'react-hook-form';

import { ErrorMessage, Input, Label } from '@/shared/ui';

/**
 * @FieldName
 * @param {string} type - 'text, email phone. textarea'
 * @param {string} name - name field
 */

export default function FieldName(props) {
  const { type = 'text', name = 'name' } = props;
  const {
    register,
    trigger,
    formState: { errors },
  } = useFormContext();
  return (
    <>
      <Label>
        <Input
          type={type}
          rest={{
            onBlur: () => {
              trigger(name);
            },
          }}
          placeholder={'Ваше имя *'}
          {...register(name, {
            required: true,
            minLength: 2,
            pattern: /^[A-zА-я]+$/g,
          })}
        />
      </Label>
      {errors[name] && <ErrorMessage text={'Введите корректное имя'} />}
    </>
  );
}
