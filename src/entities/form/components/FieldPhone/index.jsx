import classNames from 'classnames';
import { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import InputMask from 'react-input-mask';

import { ErrorMessage, Label } from '@/shared/ui';

import s from './FieldPhone.module.scss';

/**
 * @FieldPhone
 * @param {string} name - name field
 * @param {string} className - className
 */

export default function FieldPhone(props) {
  const { name = 'phone', className } = props;
  const {
    register,
    trigger,
    getValues,
    formState: { errors },
  } = useFormContext();

  const cls = classNames({
    [s.main]: true,
    [`${className}`]: className,
  });

  useEffect(() => {
    const fieldValue = getValues(name);
    if (Boolean(fieldValue)) {
      trigger(name);
    }
  }, [name, getValues, trigger]);

  return (
    <>
      <Label>
        <InputMask
          mask={'+7\\ (999)-999-99-99'}
          className={cls}
          alwaysShowMask={false}
          type="text"
          placeholder="Телефон *"
          {...register('phone', {
            required: true,
            pattern: /^\+7 \(\d{3}\)-\d{3}-\d{2}-\d{2}$/gm,
            minLength: 6,
            maxLength: 18,
          })}
          onBlur={() => {
            trigger(name);
          }}
          onFocus={() => {
            trigger(name);
          }}
        />
      </Label>
      {errors[name] && <ErrorMessage text={'Введите корректный номер телефона'} />}
    </>
  );
}
