import { useFormContext } from 'react-hook-form';

import { Textarea } from '@/shared/ui';

export default function FieldTextarea(props) {
  const { name = 'comment', className, placeholder = 'Сообщение' } = props;
  const { register } = useFormContext();
  return (
    <Textarea
      /*     name={name} */
      className={className}
      placeholder={placeholder}
      {...register(name, { minLength: 2 })}
    ></Textarea>
  );
}
