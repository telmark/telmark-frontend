import { forwardRef } from 'react';
import { useFormContext } from 'react-hook-form';

import { ErrorMessage, InputCheckbox, Label, Typography } from '@/shared/ui';

import s from './PrivatePolicy.module.scss';

/**
 * @PrivatePolicy rfc Component
 */

const PrivatePolicy = forwardRef((props, ref) => {
  const { name = 'contact_private_policy' } = props;
  const {
    register,
    setValue,
    getValues,
    formState: { errors },
  } = useFormContext();

  return (
    <>
      <Label className={s.labelCheckbox}>
        <InputCheckbox
          {...register(name, {
            required: 'Необходимо принять пользовательское соглашение',
          })}
          ref={ref}
          setValue={setValue}
          defaultChecked={getValues(name)}
          {...props}
        />
        <Typography variant="text_span">
          Я даю согласие на{' '}
          <a href="/privacy-policy" target="_blank" className={s.labelCheckboxLink}>
            обработку персональных данных
          </a>
        </Typography>
      </Label>
      {errors[name] && <ErrorMessage text={errors[name].message} />}
    </>
  );
});
export default PrivatePolicy;
