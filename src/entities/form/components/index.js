import Field from './Field';
import FieldEmail from './FieldEmail';
import FieldFile from './FieldFile';
import FieldName from './FieldName';
import FieldPhone from './FieldPhone';
import FieldTextarea from './FieldTextarea';
import PrivatePolicy from './PrivatePolicy';

export { Field, FieldPhone, FieldName, FieldEmail, PrivatePolicy, FieldTextarea, FieldFile };
