import Link from 'next/link';
import { memo } from 'react';

import MenuItemWithChildren from '../MenuItemWithChildren';
import s from './MenuItem.module.scss';

const MenuItem = memo((props) => {
  const { title, link, nestedElements } = props;
  if (nestedElements && nestedElements.length) {
    return <MenuItemWithChildren title={title} subMenuItems={nestedElements} />;
  }
  return (
    <li className={s.menu__item}>
      <Link
        className={s.menu__link}
        href={{
          pathname: link,
          query: { pageName: title },
        }}
        as={link}
      >
        {title}
      </Link>
    </li>
  );
});
export default MenuItem;
