import classes from 'classnames';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';

import { useClickOutside } from '@/shared/hooks';

import s from './MenuItemWithChildren.module.scss';

const Dropdown = (props) => {
  const { menuItems } = props;
  return (
    <ul className={s.dropdownMenu}>
      {menuItems.map((el) => {
        return el.nestedElements.length ? (
          <MenuItemWithChildren key={el.id} title={el.title} subMenuItems={el.nestedElements} />
        ) : (
          <li key={el.id} className={s.dropdownMenu__item}>
            <Link
              className={s.menu__link}
              href={{
                pathname: el.link,
                query: { slug: [el.slug] },
              }}
              as={el.slug ? el.slug : el.link}
              onClick={el.handler}
            >
              {el.title}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

const MenuItemWithChildren = (props) => {
  const { title, subMenuItems, iconSrc, className, classNameWrapper } = props;
  const ref = useRef();
  const [isOpen, setOpen] = useState(false);
  const router = useRouter();

  useClickOutside(ref, setOpen);

  useEffect(() => {
    setOpen(false);
  }, [router.asPath]);

  const clsName = classes({
    [s.menu__link]: true,
    [s.menu__link__hasChild]: true,
    [`${className}`]: className,
  });

  const clsNameWrapper = classes({
    [s.menu__item]: true,
    [`${classNameWrapper}`]: classNameWrapper,
  });
  return (
    <li className={clsNameWrapper} ref={ref}>
      <button
        className={clsName}
        title="Открыть еще меню"
        onMouseDown={() => {
          setOpen(!isOpen);
        }}
        /* onTouchEnd={() => {
          setOpen(!isOpen);
        }} */
        aria-expanded={isOpen}
      >
        {iconSrc ? (
          <span role="img" className={s.icon__wrapper}>
            <Image src={iconSrc} alt="" />
          </span>
        ) : null}
        {title}
      </button>
      {isOpen ? <Dropdown menuItems={subMenuItems} /> : null}
    </li>
  );
};
export default MenuItemWithChildren;
