import s from './MobileMenuBtn.module.scss';

/**
 * @MobileMenuBtn
 * @param {boolean} isOpen - статус состояния
 * @param {function} handler - 'функция закрытия/открытия меню
 */

export default function MobileMenuBtn(props) {
  const { isOpen, handler } = props;
  return (
    <>
      <button
        className={`${s.openMenu} ${isOpen ? s.navOpened : ''}`}
        onClick={() => handler(!isOpen)}
        title="Открыть меню"
      >
        <span> </span>
      </button>
    </>
  );
}
