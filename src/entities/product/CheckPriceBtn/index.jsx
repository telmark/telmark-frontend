import { Button, Typography } from '@/shared/ui';

import s from './CheckPriceBtn.module.scss';

export const CheckPriceBtn = (props) => {
  const { handler } = props;

  return (
    <>
      <Typography className={s.contentInfoTitle} variant="title_md" boldness="medium">
        <span className={s.contentInfoTitleText}>Цена: </span>
        <Button className={s.btn} title="Уточнить цену" onClick={handler}>
          Уточнить цену
        </Button>
      </Typography>
    </>
  );
};
