import { memo } from 'react';

import { ContainerHtml } from '@/shared/ui';

import s from './Description.module.scss';

/**
 * @Description
 * @param {HTML} description
 */

export const Description = memo((props) => {
  const { description } = props;
  return (
    <ContainerHtml
      itemProp="description"
      className={s.descriptionBlock}
      htmlContent={`<!--—noindex—-->
    <!--googleoff: all-->${description}<!--—noindex—-->
    <!--googleoff: all-->
    `}
    />
  );
});
