import { Typography } from '@/shared/ui';

import s from './FeatureList.module.scss';

/**
 * @FeatureList
 * @param {Arr} props
 * @param {string} props.name - name
 * @param {string} props.value - function onChange
 */
//FIXME: поправить при рефакторинге
export const FeatureList = (props) => {
  const { featureList } = props;

  if (!featureList.length) {
    return null;
  }

  return (
    <ul className={s.wrapper}>
      {featureList.map((el, index) => {
        return (
          <li key={el.product_characteristic_id || index} className={s.wrapperItem}>
            <Typography className={s.item} boldness="bold">
              {el.key || el.name}
            </Typography>
            <Typography className={s.item}>{el.value}</Typography>
          </li>
        );
      })}
    </ul>
  );
};
