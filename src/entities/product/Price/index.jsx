import { Typography } from '@/shared/ui';

export const Price = (props) => {
  const { price } = props;
  return (
    <>
      <Typography variant="title_md" boldness="medium">
        Цена: от
      </Typography>
      <Typography variant="title_md" boldness="medium">
        <span itemProp="price" content={price.toFixed()}>
          {price.toFixed(2)}{' '}
        </span>
        <span itemProp="priceCurrency" content="RUB">
          руб/м
        </span>
      </Typography>
    </>
  );
};
