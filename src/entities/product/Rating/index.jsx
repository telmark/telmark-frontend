import { Typography } from '@/shared/ui';

import s from './Rating.module.scss';

export function Rating(props) {
  const { value = '5.0' } = props;
  return (
    <section className={s.titleRating}>
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M9.99992 1L12.4302 6.46401C12.5236 6.67387 12.6706 6.85542 12.8564 6.99045C13.0422 7.12548 13.2603 7.20913 13.4888 7.23308L19.4364 7.85596L14.9908 11.8558C14.8201 12.0095 14.6928 12.2053 14.6218 12.4238C14.5508 12.6422 14.5386 12.8755 14.5865 13.1002L15.832 18.9492L10.6541 15.9572C10.4552 15.8423 10.2296 15.7818 9.99992 15.7818C9.77021 15.7818 9.54461 15.8423 9.34573 15.9572L4.16787 18.9492L5.41337 13.1002C5.46122 12.8755 5.44901 12.6422 5.37805 12.4238C5.30705 12.2053 5.17979 12.0095 5.00904 11.8558L0.563477 7.85596L6.51106 7.23308C6.73949 7.20913 6.9576 7.12548 7.14343 6.99045C7.32926 6.85542 7.47624 6.67387 7.56959 6.46401L9.99992 1Z"
          fill="#F4C01E"
        />
      </svg>
      <Typography className={s.textRating} variant="text_xl">
        {value}
      </Typography>
    </section>
  );
}
