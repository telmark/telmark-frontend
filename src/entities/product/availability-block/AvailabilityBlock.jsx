import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './AvailabilityBlock.module.scss';

export const AvailabilityBlock = memo((props) => {
  const { availabilityValue, className = '', customText = '' } = props;
  const text = `В наличии ${availabilityValue}  м`;
  return (
    <Typography variant="text_xl" className={`${s.productAvailability} ${className}`}>
      <link itemProp="availability" href="http://schema.org/InStock" />
      {customText ? customText : text}
    </Typography>
  );
});
