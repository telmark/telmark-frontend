import Image from 'next/image';

import loup from '@/shared/images/general/loup.svg';

import s from './ReviewElement.module.scss';

export function ReviewElement(props) {
  const { minSrc, position, company, description, title, className = '' } = props;
  return (
    <div className={`${s.item} ${className}`}>
      <div className={s.img}>
        <picture>
          <Image src={minSrc} alt="Landscape picture" width={800} height={500} />
        </picture>
        <Image className={s.img__loupe} src={loup} alt="loupe" />
      </div>

      <div className={s.infoWrapper}>
        <div className={s.name}>{title}</div>
        <div className={s.position}>{position}</div>
        <div className={s.company}>{company}</div>
      </div>

      <div className={s.text}>{description}</div>
    </div>
  );
}
