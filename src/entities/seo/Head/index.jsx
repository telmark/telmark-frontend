import { default as NextHead } from 'next/head';

export default function Head(props) {
  const { title = 'not title', descriptionText = '', canonicalUrl, children } = props;
  return (
    <NextHead>
      <title>{title}</title>
      <meta name="description" content={descriptionText} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="icon" href="/favicon.ico" />
      <link rel="canonical" href={canonicalUrl} />
      {children}
    </NextHead>
  );
}
