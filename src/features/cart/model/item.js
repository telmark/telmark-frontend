import { makeAutoObservable } from 'mobx';

export class CartItem {
  constructor(product) {
    makeAutoObservable(this);
    Object.assign(this, product);
  }

  updateQuantity(_quantity) {
    this._quantity = Math.max(1, _quantity);
  }

  get totalPriceProduct() {
    const _price = this.price || 322;
    return +(_price * this._quantity).toFixed();
  }
}
