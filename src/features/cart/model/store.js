import { makeAutoObservable, runInAction, toJS } from 'mobx';

import makeRequestXHR from '@/shared/api/http';

import { CartItem } from './item';

//TODO: заменить POST на нужные методы в свагере

export default class CartStore {
  items = new Map();
  state = null;
  constructor() {
    makeAutoObservable(this);
  }

  *addToCart(productId, _quantity, element) {
    this.state = 'pending';
    try {
      yield makeRequestXHR('post', {
        url: '/cart/cart-items',
        data: {
          product_id: productId,
          count: _quantity,
          price: element.price,
        },
      });

      runInAction(() => {
        this.fetchCart();
        this.state = 'done';
      });
    } catch (error) {
      this.state = 'error';
    }
  }

  *updateItem(cartId, _quantity, _productId) {
    this.state = 'pending';
    try {
      yield makeRequestXHR('put', {
        url: `/cart/cart-items/${cartId}`,
        data: {
          count: _quantity,
        },
      });
      this.items.get(_productId).updateQuantity(_quantity);
      runInAction(() => {
        setTimeout(() => {
          this.fetchCart();
          this.state = 'done';
        }, 500);
      });
    } catch (error) {
      console.error('MobX err0r==', error);
      this.state = 'error';
    }
  }

  *removeFromCart(cartId, _productId) {
    this.state = 'pending';
    try {
      yield makeRequestXHR('delete', {
        url: `/cart/cart-items/${cartId}`,
      });
      this.items.delete(_productId);
      runInAction(() => {
        setTimeout(() => {
          this.fetchCart();
          this.state = 'done';
        }, 500);
      });
    } catch (error) {
      console.error('MobX err0r==', error);
      this.state = 'error';
    }
  }

  *fetchCart() {
    try {
      const response = yield makeRequestXHR('get', {
        url: '/cart',
      });
      runInAction(() => {
        response.products.forEach((el) => {
          const _productId = el.product_id;
          const _quantity = el.count;
          this.items.set(_productId, new CartItem({ ...el, _quantity }));
        });
      });
    } catch (error) {
      console.error('MobX err0r==', error);
      this.state = 'error';
    }
  }

  *sendCart(formData) {
    try {
      const res = yield makeRequestXHR('post', {
        url: '/cart/enquiry',
        data: formData,
      });
      runInAction(() => {
        setTimeout(() => {
          this.items.clear();
          this.fetchCart();
        }, 3000);
      });
      return res;
    } catch (error) {
      console.error('MobX err0r==', error);
      throw new Error(error);
    }
  }

  *deleteCart() {
    this.state = 'pending';
    try {
      yield makeRequestXHR('delete', {
        url: '/cart',
      });
      runInAction(() => {
        setTimeout(() => {
          this.state = 'done';
          this.items.clear();
          this.fetchCart();
        }, 1000);
      });
    } catch (error) {
      console.error('MobX err0r==', error);
      this.state = 'error';
    }
  }

  setCartState(state = null) {
    this.state = state;
  }

  get itemsArray() {
    return Object.values(this.items.toJSON()).map(([k, v]) => v);
  }
  get itemsMap() {
    return toJS(this.items);
  }
  get cartItemsLength() {
    return this.itemsArray.reduce((acc, cartItem) => {
      return (acc += cartItem._quantity);
    }, 0);
  }
  get totalPrice() {
    return this.itemsArray.reduce((acc, item) => acc + +item.totalPriceProduct, 0);
  }
}
