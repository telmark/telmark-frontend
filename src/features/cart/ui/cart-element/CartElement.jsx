import { observer } from 'mobx-react-lite';
import React, { useCallback, useState } from 'react';

import { ProductName, ProductPrice, ProductRemoveBtn, ProductTotal } from '@/entities/cart';
import { CountBlock } from '@/entities/common/count-block';
import { useGlobalStore } from '@/shared/hooks';

import s from './CartElement.module.scss';

export const CartElement = observer((props) => {
  const { cartItem, handler } = props;
  const { cart } = useGlobalStore();
  const [localElement, setUpdateLocalElement] = useState(() => {
    cartItem._quantity = cartItem.count;
    return { ...cartItem };
  });

  const updateQuantity = useCallback(
    (_quantity) => {
      if (_quantity === 0 || _quantity === '') {
        return;
      } else {
        cart.updateItem(cartItem.cart_item_id, _quantity, cartItem.product_id);
      }
    },
    [cart, cartItem],
  );

  const deleteFromCart = useCallback(() => {
    cart.removeFromCart(cartItem.cart_item_id, cartItem.product_id);
  }, [cartItem, cart]);

  console.log('localElement==', localElement);
  return (
    <div className={s.tableBody__row}>
      <div className={s.tableBody__cell}>
        <ProductName element={cartItem} />
      </div>
      <div className={s.tableBody__cell}>
        <div className={s.table__count}>
          <CountBlock
            element={localElement}
            setUpdateCount={setUpdateLocalElement}
            callback={updateQuantity}
          />
        </div>
      </div>
      <div className={s.tableBody__cell}>
        <ProductPrice element={cartItem} handler={handler} />
      </div>
      <div className={s.tableBody__cell}>
        <ProductTotal element={cartItem} />
      </div>
      <ProductRemoveBtn handler={deleteFromCart} />
    </div>
  );
});
