import { observer } from 'mobx-react-lite';

import FormContext from '@/entities/form/FormContext';
import { Button, Typography } from '@/shared/ui';

import s from './CartFormSend.module.scss';

export const CartFormSend = observer((props) => {
  const { onSubmitHandler } = props;
  const onSubmit = (data) => {
    onSubmitHandler({ ...data, url: document.URL });
  };

  return (
    <>
      <div className={s.contactForm__content}>
        <Typography variant="title_lg" boldness="bold">
          Оформить заказ
        </Typography>
        <FormContext className={s.contactForm__form} handler={onSubmit}>
          <FormContext.FieldName />
          <FormContext.FieldPhone />
          <FormContext.FieldEmail />
          <FormContext.FieldTextarea />
          <FormContext.PrivatePolicy />
          <Button type="submit" className={s.button} title="Отправить">
            Отправить
          </Button>
        </FormContext>
      </div>
    </>
  );
});
