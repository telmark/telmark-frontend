import makeRequestXHR from '@/shared/api/http';

export async function fetchSelectedCategory(categorySlug) {
  try {
    const response = await makeRequestXHR('get', {
      url: `/categories/by-filters?slug=${encodeURI(categorySlug)}&with_marks=1`,
    });
    return response.data;
  } catch (error) {
    console.error('error fetchSelectedCategory', error);
    return error;
  }
}

export async function filterCatalogList(params) {
  try {
    const response = await makeRequestXHR('get', {
      url: `/products/filter?${params.paramsFilter}&core=${params.core}&section=${
        params.section
      }&page=${params.page || 1}&per_page=${params.per_page || 10}`,
    });
    return response;
  } catch (error) {
    console.error('error filterCatalogList', error);
    return error;
  }
}
