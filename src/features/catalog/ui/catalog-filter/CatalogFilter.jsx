import debounce from 'debounce';
import { useRouter } from 'next/router';
import { memo } from 'react';

import FormContext from '@/entities/form/FormContext';
import { Typography } from '@/shared/ui';

import s from './CatalogFilter.module.scss';

const deb = debounce((v, handler) => {
  if ((v.core && v.section) || (!v.core && !v.section)) {
    //FIXME: настроить преобразование
    //const core = +v.core.trim();
    //const section = +v.section.trim();
    handler(v.core, v.section);
  }
}, 800);

export const CatalogFilter = memo((props) => {
  const { handler } = props;
  const { query } = useRouter();
  const regExpCore = /^\d{1,6}$/g;
  const regExpSection = /^[0-9]{1,5}([,.][0-9]{1,6})?$/g;

  const handleOnSubmit = (data) => {
    if (query.core === data.core && query.section === data.section) {
      return;
    }
    deb(data, handler);
  };

  return (
    <FormContext
      className={s.wrapper}
      handler={handleOnSubmit}
      hasReset={false}
      defaultValues={{
        core: query.core,
        section: query.section,
      }}
      onBlurAction={true}
    >
      <div className={s.title}>
        <Typography variant="title_md" boldness="bold">
          Введите сечение:
        </Typography>
      </div>
      <div className={s.inputWrapper}>
        <FormContext.Field
          className={s.input}
          name="core"
          placeholder=""
          textError="Введите целое число"
          validationOptions={{ required: true, pattern: regExpCore }}
        />
      </div>
      <Typography variant="title_sm" boldness="medium">
        X
      </Typography>
      <div className={s.inputWrapper}>
        <FormContext.Field
          className={s.input}
          name="section"
          placeholder=""
          textError="Введите число"
          validationOptions={{ required: true, pattern: regExpSection }}
        />
      </div>
      <input type="submit" className={s.hiddenInput} />
    </FormContext>
  );
});
