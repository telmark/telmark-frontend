import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { memo } from 'react';

import s from './CatalogPagination.module.scss';
import { useCountPagination } from './utils/useCountPagination';

/**
 * @Pagination
 * @param {number} currentPage - номер страницы в пагинации;
 * @param {number} totalProducts - количесто товаров
 * @param {number} totalPages - всего страниц
 * @param {function} handleChangePage - относительный URL категории товара;
 */

export const CatalogPagination = memo((props) => {
  const { currentPage, totalPages } = props;
  const searchParams = useSearchParams();
  const params = new URLSearchParams(searchParams);
  const pathname = usePathname();
  const { replace } = useRouter();

  const paginationRange = useCountPagination({
    currentPage,
    totalPageCount: totalPages,
  });

  const onNext = () => {
    params.set('page', currentPage + 1);
    replace(`${pathname}?${params.toString()}`);
  };

  const onPrevious = () => {
    params.set('page', currentPage - 1);
    replace(`${pathname}?${params.toString()}`);
  };

  const onPagination = (v) => {
    params.set('page', v);
    return `${pathname}?${params.toString()}`;
  };

  if (totalPages === 1) {
    return null;
  }

  return (
    <ul className={s.wrapper}>
      <li>
        <button className={s.btn} onClick={onPrevious} title="Назад" disabled={currentPage === 1}>
          <svg
            width="10"
            height="14"
            viewBox="0 0 10 14"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M8.38027 13.8056C8.55759 13.9336 8.79212 14.0032 9.03445 13.9999C9.27677 13.9966 9.50798 13.9206 9.67936 13.7879C9.85074 13.6552 9.94891 13.4762 9.95319 13.2886C9.95746 13.101 9.86751 12.9195 9.70229 12.7822L2.25658 7.01806L9.70229 1.25392C9.79419 1.18763 9.8679 1.10769 9.91902 1.01886C9.97015 0.930031 9.99764 0.834142 9.99986 0.736911C10.0021 0.639681 9.97897 0.543102 9.93192 0.452934C9.88488 0.362766 9.81485 0.280857 9.72603 0.212094C9.63721 0.143331 9.53141 0.0891218 9.41493 0.0527019C9.29846 0.0162811 9.17371 -0.00160319 9.04811 0.00011151C8.92252 0.00182716 8.79865 0.0231084 8.68391 0.0626868C8.56917 0.102265 8.4659 0.159329 8.38027 0.230475L0.273557 6.50634C0.0983885 6.64211 -2.0807e-07 6.82616 -2.28014e-07 7.01806C-2.47959e-07 7.20996 0.0983884 7.39401 0.273556 7.52979L8.38027 13.8056Z"
              fill="#2559D9"
            />
          </svg>
        </button>
      </li>
      {paginationRange?.map((pageNumber, index) => {
        // If the pageItem is a DOT, render the DOTS unicode character
        if (pageNumber === '...') {
          return (
            <li key={`${index}_...`} className={s.page}>
              &#8230;
            </li>
          );
        }
        // Render our Page Pills
        return (
          <li key={index} type="pageItem">
            <a
              href={onPagination(pageNumber)}
              className={`${s.page} ${pageNumber === currentPage ? s.currPage : ''}`}
              title={`Перейти на страницу ${pageNumber}`}
            >
              {pageNumber}
            </a>
          </li>
        );
      })}
      <li>
        <button
          className={s.btn}
          title="Вперед"
          onClick={onNext}
          disabled={totalPages <= currentPage}
        >
          <svg
            width="10"
            height="14"
            viewBox="0 0 10 14"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1.61973 0.194351C1.44241 0.0664395 1.20788 -0.00319736 0.965555 0.000112675C0.723225 0.00342271 0.492015 0.0794212 0.320636 0.212096C0.149256 0.344771 0.0510866 0.523763 0.046811 0.711364C0.0425354 0.898965 0.132487 1.08053 0.297715 1.2178L7.74342 6.98194L0.297717 12.7461C0.205816 12.8124 0.132104 12.8923 0.0809794 12.9811C0.0298548 13.07 0.00236429 13.1659 0.000148328 13.2631C-0.00206763 13.3603 0.0210364 13.4569 0.0680815 13.5471C0.115127 13.6372 0.18515 13.7191 0.273973 13.7879C0.362796 13.8567 0.468599 13.9109 0.585072 13.9473C0.701544 13.9837 0.826299 14.0016 0.951894 13.9999C1.07749 13.9982 1.20135 13.9769 1.31609 13.9373C1.43083 13.8977 1.5341 13.8407 1.61973 13.7695L9.72645 7.49366C9.90161 7.35788 10 7.17383 10 6.98194C10 6.79004 9.90161 6.60599 9.72645 6.47021L1.61973 0.194351Z"
              fill="#2559D9"
            />
          </svg>
        </button>
      </li>
    </ul>
  );
});
