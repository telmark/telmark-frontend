import { observer } from 'mobx-react-lite';
import { useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import ActionError from '@/entities/alerts/cart/ActionError';
import AddSuccess from '@/entities/alerts/cart/AddSuccess';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import { useGlobalStore } from '@/shared/hooks';
import Preloader from '@/shared/ui/Preloader';
import THead from '@/shared/ui/THead';

import s from './CatalogPanel.module.scss';
import { CatalogBodyItem } from './components/item/CatalogBodyItem';
import { checkItemsInCart } from './utils';

/**
 * @CatalogPanel
 * @param {arr} mapCatalogBody
 * @prop {string} product_id - id
 * @prop {string} raw_name - название товара
 * @prop {string} markSlug - слаг марки
 * @prop {string} slug - слаг товара
 * @prop {string} price - цена
 */

export const CatalogPanel = observer((props) => {
  const { mapCatalogBody, image } = props;
  const { cart } = useGlobalStore();
  const checkedCatalogItemsInCart = checkItemsInCart(mapCatalogBody, cart.itemsMap);
  const [isOpenAlert, setOpenAlert] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);
  const handlerModalOpen = useCallback(() => {
    setIsOpen(true);
  }, []);
  const mapHead = [
    {
      id: '1e',
      value: 'Наименование',
    },
    {
      id: '2e',
      value: 'Наличие',
    },
    {
      id: '3e',
      value: 'Цена за ед. ( с НДС )',
    },
    {
      id: '4e',
      value: 'счетчик',
      hide: true,
    },
    {
      id: '5e',
      value: 'Добавить в корзину',
      hide: true,
    },
  ];

  return (
    <>
      <article>
        {cart.state === 'pending' ? <Preloader /> : null}
        {cart.state === 'error' ? <ActionError /> : null}
        <table className={s.productTable} itemScope itemType="http://schema.org/ItemList">
          <THead mapHeadItems={mapHead} isTable={true} />
          <tbody>
            {checkedCatalogItemsInCart?.map((catalogProduct) => {
              return (
                <CatalogBodyItem
                  key={catalogProduct.product_id}
                  element={catalogProduct}
                  modalHandler={setOpenAlert}
                  modalFormHandler={handlerModalOpen}
                  image={image}
                />
              );
            })}
          </tbody>
        </table>
        <AddSuccess isOpen={isOpenAlert} callbackOnClose={() => setOpenAlert(false)} />
      </article>
      <ModalWrapper
        isOpen={isOpen}
        handlerModalClose={handlerModalClose}
        handler={submitContactForm}
        ContentModalForm={(props) => <FeedbackForm {...props} />}
      />
    </>
  );
});
