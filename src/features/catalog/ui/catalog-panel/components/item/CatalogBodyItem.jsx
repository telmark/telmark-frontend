import { observer } from 'mobx-react-lite';
import Image from 'next/image';
import { useCallback, useEffect, useState } from 'react';

import { AddToCartBtn } from '@/entities/cart';
import { CatalogProductName, CatalogProductPrice } from '@/entities/catalog';
import { CountBlock } from '@/entities/common/count-block';
import { useGlobalStore } from '@/shared/hooks';
import markDefaultImage from '@/shared/images/markDefaultImage.jpg';

import s from './CatalogBodyItem.module.scss';

/**
 * @CatalogBodyItem
 * @param {obj} element
 * @prop {string} product_id - id
 * @prop {string} raw_name - название товара
 * @prop {string} markSlug - слаг марки
 * @prop {string} slug - слаг товара
 * @prop {string} price - цена
 */

export const CatalogBodyItem = observer((props) => {
  const { element, modalHandler, modalFormHandler, image } = props;
  const { cart } = useGlobalStore();
  const img = image ? encodeURI(image.trim()) : markDefaultImage.src;
  const [localElement, setUpdateLocalElement] = useState(() => {
    element._quantity = 1;
    return { ...element, _raw_name: element.raw_name || element.name };
  });
  const productLink = `/${localElement.markSlug}/${localElement.slug}`;

  const addToCart = useCallback(async () => {
    if (localElement._quantity === 0 || localElement._quantity === '') {
      setUpdateLocalElement((prev) => {
        return { ...prev, _quantity: 1 };
      });
      return;
    }
    await cart.addToCart(localElement.product_id, localElement._quantity, localElement);
    modalHandler(true);
    setUpdateLocalElement((prev) => {
      return { ...prev, _quantity: 1 };
    });
  }, [localElement, cart, modalHandler]);

  useEffect(() => {
    setUpdateLocalElement((prev) => {
      element._quantity = 1;
      return { ...prev, ...element };
    });
  }, [element]);

  return (
    <tr itemScope itemProp="itemListElement" itemType="http://schema.org/Product">
      <td className={s.seoImgBock}>
        <div itemProp="image" itemScope itemType="http://schema.org/ImageObject">
          <Image
            itemProp="url contentUrl"
            src={img}
            width={300}
            height={288}
            title={'catalog image'}
            alt={'catalog image'}
          />
        </div>
      </td>
      <td id={s.productTable__name}>
        <CatalogProductName productName={localElement._raw_name} link={productLink} />
      </td>
      <td className={s.productTable__availability}>
        <div className={s.item} itemProp="availability" href="http://schema.org/InStock">
          Есть в наличии
        </div>
      </td>
      <td className={s.productTable__price}>
        <CatalogProductPrice
          productPrice={localElement.price}
          modalFormHandler={modalFormHandler}
        />
      </td>
      <td className={s.productTable__count}>
        <div className={s.productTable__countBlock}>
          <CountBlock element={localElement} setUpdateCount={setUpdateLocalElement} />
        </div>
      </td>
      <td>
        <AddToCartBtn handler={addToCart} isInCart={localElement._onCart} />
      </td>
    </tr>
  );
});
