export function checkItemsInCart(arr = [], cartArr = {}) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    if (cartArr.has(arr[i].product_id)) {
      const o = { ...arr[i] };
      o._onCart = true;
      result.push(o);
    } else {
      result.push(arr[i]);
    }
  }
  return result;
}
