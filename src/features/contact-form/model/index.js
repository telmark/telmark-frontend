import makeRequestXHR from '@/shared/api/http';

export async function submitContactForm(formData) {
  const response = await makeRequestXHR('post', {
    url: '/common/contact/email',
    data: formData,
    headers: { 'Content-Type': 'multipart/form-data' },
  });
  return response.data;
}
