import Image from 'next/image';

import AlertModal from '@/entities/alerts/AlertModal';
import FormContext from '@/entities/form/FormContext';
import { useSubmitForm } from '@/shared/hooks';
import img from '@/shared/images/ContactForm.jpg';
import { Button, Typography } from '@/shared/ui';
import Preloader from '@/shared/ui/Preloader';

import { submitContactForm } from '../../model';
import s from './ContactForm.module.scss';

const ContactForm = () => {
  const { isLoading, isError, isSuccess, trigger, resetFormStatuses } =
    useSubmitForm(submitContactForm);

  const formDataDefault = {
    name: '',
    phone: '',
    email: '',
    comment: '',
    contact_private_policy: true,
    file: undefined,
  };

  const onSubmit = (data) => {
    // eslint-disable-next-line no-undef
    ym(56820658, 'reachGoal', 'request');
    trigger({ ...data, url: document.URL });
  };

  return (
    <>
      {isSuccess && <AlertModal type={'success'} callbackOnClose={resetFormStatuses} />}
      {isError && <AlertModal type={'error'} callbackOnClose={resetFormStatuses} />}
      {isLoading ? <Preloader /> : null}
      <div className={s.contactForm}>
        <div className={s.contactForm__wrapper}>
          <div className={s.contactForm__header}>
            <Typography variant="title_lg" boldness="bold">
              Поможем подобрать продукцию
            </Typography>
            <Typography color="fade">Наш специалист свяжется с Вами в течении 15 минут</Typography>
          </div>
          <FormContext
            className={s.contactForm__form}
            handler={onSubmit}
            defaultValues={formDataDefault}
          >
            <div className={s.contactForm__formSectionWrapper}>
              <div className={s.contactForm__formSection}>
                <FormContext.FieldName />
                <FormContext.FieldPhone />
                <FormContext.FieldEmail />
              </div>
              <div className={s.contactForm__formSection}>
                <FormContext.FieldTextarea className={s.textarea} />
                <FormContext.FieldFile />
              </div>
            </div>
            <div className={s.contactForm__formSection}>
              <div className="form__block">
                <FormContext.PrivatePolicy />
              </div>
              <div className="form__block">
                <Button type="submit" title="Отправить">
                  Отправить
                </Button>
              </div>
            </div>
          </FormContext>
        </div>
        <div className={s.contactFormImg__wrapper}>
          <Image src={img} alt="img" priority={true} />
        </div>
      </div>
    </>
  );
};

export default ContactForm;
