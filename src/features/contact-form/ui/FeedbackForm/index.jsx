import FormContext from '@/entities/form/FormContext';
import { Button, Typography } from '@/shared/ui';

import s from './FeedbackForm.module.scss';

const FeedbackForm = (props) => {
  const { onSubmitHandler, defaultValues, setFormValues } = props;
  const onSubmit = (data) => {
    // eslint-disable-next-line no-undef
    ym(56820658, 'reachGoal', 'request');
    onSubmitHandler({ ...data, url: document.URL });
  };

  return (
    <div className={s.contactForm__content}>
      <Typography variant="title_lg" boldness="bold">
        Отправить заявку
      </Typography>
      <FormContext
        className={s.contactForm__form}
        handler={onSubmit}
        defaultValues={defaultValues}
        setFormValues={setFormValues}
      >
        <FormContext.FieldName />
        <FormContext.FieldPhone />
        <FormContext.FieldEmail />
        <FormContext.FieldTextarea />
        <FormContext.FieldFile />
        <FormContext.PrivatePolicy />
        <Button type="submit" className={s.button} title="Отправить">
          Отправить
        </Button>
      </FormContext>
    </div>
  );
};
export default FeedbackForm;
