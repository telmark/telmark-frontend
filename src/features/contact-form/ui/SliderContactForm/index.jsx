import { useEffect, useState } from 'react';

import AlertModal from '@/entities/alerts/AlertModal';
import FormContext from '@/entities/form/FormContext';
import { useSubmitForm } from '@/shared/hooks';
import { Button } from '@/shared/ui';
import Preloader from '@/shared/ui/Preloader';

import { submitContactForm } from '../../model';
import s from './SliderContactForm.module.scss';

const SliderContactForm = () => {
  const [isMobile, setIsMobile] = useState(false);
  const { isLoading, isError, isSuccess, trigger, resetFormStatuses } =
    useSubmitForm(submitContactForm);

  const formDataDefault = {
    name: '',
    phone: '',
    email: '',
    comment: '',
    contact_private_policy: true,
    file: undefined,
  };

  const onSubmit = (data) => {
    // eslint-disable-next-line no-undef
    ym(56820658, 'reachGoal', 'request');
    trigger({ ...data, url: document.URL });
  };

  useEffect(() => {
    if (window !== undefined) {
      if (window.matchMedia('(max-width: 767px)').matches) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    }
  }, []);

  if (isMobile) {
    return null;
  }

  return (
    <>
      {isSuccess && <AlertModal type={'success'} callbackOnClose={resetFormStatuses} />}
      {isError && <AlertModal type={'error'} callbackOnClose={resetFormStatuses} />}
      {isLoading ? <Preloader /> : null}
      <div className={s.slider_form}>
        <div className={s.slider_form__wrapper}>
          <div className={s.slider_form__title}>Получить спецпредложение</div>
          <FormContext handler={onSubmit} defaultValues={formDataDefault}>
            <div className={s.slider_form__labels}>
              <FormContext.FieldName />
              <FormContext.FieldPhone />
              <FormContext.FieldEmail />
              <FormContext.FieldTextarea />
              <FormContext.FieldFile />
            </div>
            <div className={s.slider_form__policy}>
              <FormContext.PrivatePolicy />
            </div>
            <Button type="submit" className={s.button} title="Отправить">
              Отправить
            </Button>
          </FormContext>
        </div>
      </div>
    </>
  );
};

export default SliderContactForm;
