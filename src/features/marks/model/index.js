import makeRequestXHR from '@/shared/api/http';

export async function fetchProductsByMark(
  params = {
    mark_id: 1640,
    per_page: 15,
    page: 1,
  },
) {
  const response = await makeRequestXHR('get', {
    url: '/marks/products',
    params: params,
  });
  return response.data;
}

export async function fetchSelectedMarkList(markId = 1640) {
  const response = await makeRequestXHR('get', {
    url: `/marks/${markId}`,
  });
  return response.data;
}

export async function fetchMarkListByCategory(categoryId) {
  const response = await makeRequestXHR('get', {
    url: '/marks',
    params: {
      category_id: categoryId,
    },
  });
  return response.data;
}

export async function fetchSelectedMark(params) {
  try {
    const response = await makeRequestXHR('get', {
      url: `/marks/mark?markSlug=${encodeURI(params.markSlug)}`,
    });
    return response;
  } catch (error) {
    console.error('error  Mark=', error);
    return error;
  }
}
