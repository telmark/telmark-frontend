import { useState } from 'react';

import { ToggleBtn } from '@/entities/common/toggle-btn';
import { ContainerHtml } from '@/shared/ui';

import s from './MarkDescription.module.scss';

export default function MarkDescription(props) {
  const { title, description, short_desc, children } = props;
  const [toggle, setToggle] = useState(false);
  const textBtn = toggle ? `Скрыть подробнее про Кабель ${title}` : `Подробнее про Кабель ${title}`;

  if (description && short_desc) {
    return (
      <section>
        <ContainerHtml className={s.short_description} htmlContent={short_desc} />
        {children}
        <ToggleBtn
          className={s.wrapper}
          handler={() => setToggle((prev) => !prev)}
          textBtn={textBtn}
        />
        <>
          <div>
            {toggle ? (
              <div
                className="catalog-seo"
                dangerouslySetInnerHTML={{
                  __html: `<!--—noindex—-->
              <!--googleoff: all-->${description}<!--/googleoff: all-->
              <!--—/noindex—-->`,
                }}
              />
            ) : null}
          </div>
        </>
      </section>
    );
  } else if (description) {
    return (
      <>
        {children}
        <section>
          <div
            className="catalog-seo"
            dangerouslySetInnerHTML={{
              __html: `<!--—noindex—-->
            <!--googleoff: all-->
            ${description}
            <!--/googleoff: all-->
            <!--—/noindex—-->`,
            }}
          />
        </section>
      </>
    );
  }
  return (
    <>
      {short_desc ? (
        <section className={s.short_description}>
          <ContainerHtml htmlContent={short_desc} />
          {children}
        </section>
      ) : null}
    </>
  );
}
