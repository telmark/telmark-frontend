import Link from 'next/link';
import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './MarkList.module.scss';

/**
 * @MarkList
 * @param {arr} markList
 * @prop {string} slug - ссылка на страницу выбранной марки
 * @prop {string} name - название марки
 */

export const MarkList = memo((props) => {
  const { markList = [] } = props;

  if (!markList.length) {
    return null;
  }
  const sortedAlphabeticallyList = markList.sort((a, b) => {
    if (a.slug < b.slug) {
      return -1;
    }
    if (a.slug > b.slug) {
      return 1;
    }
    return 0;
  });
  return (
    <div className={s.wrapper}>
      <ul className={s.list}>
        {sortedAlphabeticallyList?.map((el, index) => {
          return (
            <li key={index} className={s.item}>
              <Link href={'/cable/' + el.slug} className={s.link}>
                <Typography boldness="medium" className={s.text}>
                  {el.name}
                </Typography>
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
});
