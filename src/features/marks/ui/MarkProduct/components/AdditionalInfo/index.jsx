import { useState } from 'react';

import { Typography } from '@/shared/ui';
import Modal from '@/shared/ui/Modal';

import s from './AdditionalInfo.module.scss';

export default function AdditionalInfo() {
  const [isOpen, setOpen] = useState(false);
  return (
    <>
      <section className={s.additionalInfo}>
        <ul className={s.additionalInfoList}>
          <li
            onClick={() => {
              setOpen(true);
            }}
          >
            <Typography className={`${s.garant} ${s.additionalInfoItem}`} boldness="medium">
              Гарантия качества
            </Typography>
          </li>
          <li>
            <Typography className={`${s.delivery} ${s.additionalInfoItem}`} boldness="medium">
              Доставка в срок
            </Typography>
          </li>
          <li>
            <Typography className={`${s.insure} ${s.additionalInfoItem}`} boldness="medium">
              Страхование груза
            </Typography>
          </li>
          <li>
            <Typography className={`${s.price} ${s.additionalInfoItem}`} boldness="medium">
              Честная цена
            </Typography>
          </li>
        </ul>
        <div className={s.additionalInfoContact}>
          <Typography className={s.additionalInfoContactTitle}>Есть вопросы?</Typography>
          <Typography className={s.additionalInfoContactItem} boldness="medium">
            Пришлите заявку:
            <a href="mailto:sale@telmark.ru" className={s.additionalInfoContactLink}>
              sale@telmark.ru
            </a>
          </Typography>
          <Typography className={s.additionalInfoContactItem} boldness="medium">
            Позвоните:
            <a href="tel:88006004091" className={s.additionalInfoContactLink}>
              8 800 600 40 91
            </a>
          </Typography>
          <Typography className={s.additionalInfoContactItem} boldness="medium">
            Напишите в чат:
            <a
              href="https://wa.me/89030940954"
              target="_blank"
              className={`${s.contactLinkSocial} ${s.whatapp}`}
              rel="noreferrer"
            >
              WhatsApp
            </a>
            <a
              href="https://t.me/TelmarkSPB"
              className={`${s.contactLinkSocial} ${s.telegram}`}
              target="_blank"
              rel="noreferrer"
            >
              Telegram
            </a>
          </Typography>
        </div>
      </section>
      <Modal
        isOpen={isOpen}
        className={s.modal}
        handlerClose={() => {
          setOpen(false);
        }}
        content={
          <div className={s.modalBody}>
            <Typography variant="title_md">Гарантия</Typography>
            <Typography>
              Вся кабельная продукция соответствует требованиям качества согласно ГОСТа, ТУ. При
              отгрузке на все товары предоставляются необходимые сертификаты соответствия и паспорта
              качества.
            </Typography>
            <Typography>
              «Телмарк» предоставляет гарантийный срок на реализуемую продукцию, установленный
              заводом-изготовителем, который действует при соблюдении условий транспортирования,
              хранения, прокладки, монтажа и эксплуатации.
            </Typography>
            <Typography>
              Кабельная продукция ненадлежащего качества может быть возвращена продавцу в
              гарантийный срок. Продукция надлежащего качества, в соответствии с Постановлением
              Правительства РФ от 19.01.1998 г. №55, возврату не подлежит.
            </Typography>
            <Typography>
              Подробная информация о сроках гарантии предоставляется при приобретении конкретного
              изделия.
            </Typography>
          </div>
        }
      />
    </>
  );
}
