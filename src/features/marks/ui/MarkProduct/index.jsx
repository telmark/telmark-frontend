import Image from 'next/image';
import Link from 'next/link';
import { memo, useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import { AvailabilityBlock, CheckPriceBtn } from '@/entities/product';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import markDefaultImage from '@/shared/images/markDefaultImage.jpg';
import { Button, Typography } from '@/shared/ui';

import s from './MarkProduct.module.scss';
import AdditionalInfo from './components/AdditionalInfo';

export const MarkProduct = memo((props) => {
  const { marks } = props;
  const [isOpen, setIsOpen] = useState(false);
  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);
  const handlerModalOpen = useCallback(() => {
    setIsOpen(true);
  }, []);
  const img = marks?.image ? encodeURI(marks?.image.trim()) : markDefaultImage.src;
  return (
    <>
      <section className={s.wrapper}>
        <div
          className={s.imgWrapper}
          itemProp="image"
          itemScope
          itemType="http://schema.org/ImageObject"
        >
          <Image
            itemProp="url contentUrl"
            className={s.img}
            src={img}
            width={300}
            height={288}
            title={'mark image'}
            alt={'mark image'}
          />
        </div>
        <div className={s.content}>
          <section className={s.contentInfo}>
            <Typography variant="title_sm" boldness="medium">
              Марка:{' '}
              <Link href={marks.slug} className={s.markName}>
                {marks.name}
              </Link>
            </Typography>
            {marks.mark_min_price ? (
              <>
                <div className={s.priceBlock}>
                  <Typography variant="title_md" boldness="medium">
                    Цена от {marks.mark_min_price.toFixed(2)} руб/м
                  </Typography>
                  <Typography className={s.contentInfoDesc}>
                    *Предварительная цена при оптовом заказе.
                  </Typography>
                </div>
                <AvailabilityBlock className={s.contentInfoAvailability} customText="В наличии" />
                <Button className={s.btn} title="Заказать" onClick={handlerModalOpen}>
                  Заказать
                </Button>
              </>
            ) : (
              <>
                <CheckPriceBtn handler={handlerModalOpen} />
              </>
            )}
          </section>
          <AdditionalInfo />
        </div>
      </section>
      <ModalWrapper
        isOpen={isOpen}
        handlerModalClose={handlerModalClose}
        handler={submitContactForm}
        ContentModalForm={(props) => <FeedbackForm {...props} />}
      />
    </>
  );
});
