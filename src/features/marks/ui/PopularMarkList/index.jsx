import Link from 'next/link';
import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './PopularMarkList.module.scss';

/**
 * @PopularMarkList
 * @param {string} title - название марки
 * @param {arr} popularMarkList
 * @prop {string} markSlug - ссылка на страницу выбранной марки
 * @prop {string} productRawName - название марки
 */

export default memo(function PopularMarkList(props) {
  const { popularMarkList = [], title } = props;
  if (!popularMarkList.length) {
    return null;
  }

  return (
    <section>
      <Typography className={s.title} variant="title_md" boldness="bold">
        Популярные варианты кабеля {title}
      </Typography>
      <ul className={`${s.list} _new_listWithCirles`}>
        {popularMarkList?.map((el, index) => {
          return (
            <li key={index}>
              <Link href={`${el.markSlug}/${el.productSlug}`}>
                <Typography className={s.text}>{el.productRawName}</Typography>
              </Link>
            </li>
          );
        })}
      </ul>
    </section>
  );
});
