import { observer } from 'mobx-react-lite';
import Image from 'next/image';

import MenuItem from '@/entities/menu/MenuItem';
import SelectCity from '@/features/select-city/ui';
import { useGlobalStore } from '@/shared/hooks';
import tel from '@/shared/images/svg/telegramHeader.svg';
import ws from '@/shared/images/svg/wsHeader.svg';
import { ContainerInner } from '@/shared/ui';

import { mapMenuData } from '../mapMenuData';
import s from './DesktopNavigation.module.scss';

const DesktopNavigation = observer((props) => {
  const { isHide } = props;
  const { city } = useGlobalStore();
  return (
    <div className={s.wrapper}>
      <ContainerInner>
        <nav>
          <ul className={s.menu}>
            <li className={s.selectCityDesktop}>
              <SelectCity className={s.desktopSelectCityBtn}>
                {city?.primaryCity?.is_main ? 'Выберите город' : city?.primaryCity?.name}
              </SelectCity>
            </li>
            <li className={s.socials}>
              <a href="https://wa.me/79030940954" title="Написать нам в whatsapp">
                <Image src={ws.src} alt="whatsapp" width={20} height={20} />
              </a>
              <a href="https://t.me/TelmarkSPB" title="Написать нам в telegram">
                <Image src={tel.src} alt="telegram" width={20} height={20} />
              </a>
            </li>
            {!isHide &&
              mapMenuData.map((el) => {
                return (
                  <MenuItem
                    key={el.id}
                    title={el.title}
                    nestedElements={el.nestedElements}
                    link={el.link}
                  />
                );
              })}
          </ul>
        </nav>
      </ContainerInner>
    </div>
  );
});
export default DesktopNavigation;
