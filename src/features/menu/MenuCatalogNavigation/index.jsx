import classes from 'classnames';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useRef, useState } from 'react';

import { useClickOutside } from '@/shared/hooks';
import catalog_icon from '@/shared/images/menu_catalog.svg';

import s from './MenuCatalogNavigation.module.scss';

const Dropdown = (props) => {
  const { menuItems, isOpen } = props;
  const router = useRouter();
  const clsNameDropdown = classes({
    [s.dropdownMenu]: true,
    [s.dropdownMenuHide]: !isOpen,
  });

  return (
    <ul className={clsNameDropdown}>
      {menuItems.map((el) => {
        return el.nestedElements.length ? (
          <MenuCatalogNavigation
            key={el.id}
            title={el.title}
            subMenuItems={el.nestedElements}
            onClickHandler={el.type === 'link' ? () => router.push(el.link) : () => {}}
          />
        ) : (
          <li key={el.id} className={s.dropdownMenu__item}>
            <Link
              className={s.menu__link}
              href={{
                pathname: el.link,
                query: { slug: [el.slug] },
              }}
              as={el.slug ? el.slug : el.link}
              onClick={el.handler}
            >
              {el.title}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

const MenuCatalogNavigation = (props) => {
  const { title, subMenuItems, className, classNameWrapper, iconSrc, onClickHandler } = props;
  const ref = useRef();
  const [isOpen, setOpen] = useState(false);
  const router = useRouter();

  const onClick = () => {
    if (typeof onClickHandler !== 'function') {
      setOpen(!isOpen);
    } else {
      onClickHandler();
    }
  };

  const onHover = useCallback(() => {
    if (typeof onClickHandler === 'function') {
      setOpen(true);
    }
  }, [onClickHandler]);

  const onOut = useCallback(
    (e) => {
      if (typeof onClickHandler === 'function') {
        if (ref.current && ref.current.contains(e.target)) {
          return;
        } else {
          setOpen(false);
        }
      }
    },
    [onClickHandler],
  );

  const onMouseEnter = useCallback(
    (e) => {
      if (ref.current && ref.current.contains(e.target)) {
        return;
      } else {
        setOpen(false);
      }
    },
    [setOpen, ref],
  );

  useEffect(() => {
    document.addEventListener('mouseenter', onMouseEnter);
    document.addEventListener('mouseover', onOut);
    return () => {
      document.removeEventListener('mouseenter', onMouseEnter);
      document.removeEventListener('mouseover', onOut);
    };
  }, [onMouseEnter, onOut]);

  useEffect(() => {
    setOpen(false);
  }, [router.asPath]);

  useClickOutside(ref, setOpen);

  const clsName = classes({
    [s.menu__link]: true,
    [s.menu__link__hasChild]: typeof onClickHandler !== 'function',
    [s.menuCatalogNav]: true,
    [`${className}`]: className,
  });

  const clsNameWrapper = classes({
    [s.menu__item]: true,
    [`${classNameWrapper}`]: classNameWrapper,
  });

  return (
    <li className={clsNameWrapper} ref={ref}>
      <button
        className={clsName}
        title="Открыть еще меню"
        onMouseDown={onClick}
        onTouchStart={onClick}
        onMouseEnter={onHover}
        aria-expanded={isOpen}
      >
        {iconSrc ? (
          <span role="img" className={s.icon__wrapper}>
            <Image src={catalog_icon} alt="" />
          </span>
        ) : null}
        {title}
      </button>
      <Dropdown menuItems={subMenuItems} isOpen={isOpen} />
    </li>
  );
};

export default MenuCatalogNavigation;
