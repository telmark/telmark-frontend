import { observer } from 'mobx-react-lite';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';

import MenuItem from '@/entities/menu/MenuItem';
import MenuItemWithChildren from '@/entities/menu/MenuItemWithChildren';
import MobileMenuBtn from '@/entities/menu/MobileMenuBtn';
import SelectCity from '@/features/select-city/ui';
import { useClickOutside, useGlobalStore } from '@/shared/hooks';
import catalog_icon from '@/shared/images/menu_catalog.svg';
import { ContainerInner } from '@/shared/ui';

import { mapMenuData, mapMobileCatalog } from '../mapMenuData';
import s from './MobileNavigation.module.scss';

const MobileNavigation = observer(() => {
  const [isSidebarOpen, setSidebarOpen] = useState(false);
  const ref = useRef();
  const router = useRouter();
  const { city } = useGlobalStore();

  useEffect(() => {
    if (isSidebarOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = '';
    }
  }, [isSidebarOpen]);

  useEffect(() => {
    setSidebarOpen(false);
  }, [router.pathname]);

  useClickOutside(ref, setSidebarOpen);

  return (
    <div className={s.wrapper}>
      <ContainerInner>
        <MobileMenuBtn isOpen={isSidebarOpen} handler={setSidebarOpen} />
        <aside>
          <nav className={`${s.nav} ${isSidebarOpen ? s.navOpened : ''}  `}>
            <ul className={s.menu} ref={ref}>
              <li>
                <SelectCity
                  classNameWrapper={s.mobileSelectCityWrapper}
                  className={s.mobileHeaderSelectCityBtn}
                >
                  {city?.primaryCity?.is_main ? 'Выберите город' : city?.primaryCity?.name}
                </SelectCity>
              </li>
              <MenuItemWithChildren
                title="Каталог"
                iconSrc={catalog_icon}
                subMenuItems={mapMobileCatalog['nestedElements']}
              />
              {mapMenuData.map((el) => {
                return (
                  <MenuItem
                    key={el.id}
                    title={el.title}
                    nestedElements={el.nestedElements}
                    link={el.link}
                  />
                );
              })}
            </ul>
          </nav>
        </aside>
      </ContainerInner>
    </div>
  );
});
export default MobileNavigation;
