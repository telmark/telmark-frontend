import makeRequestXHR from '@/shared/api/http';

export async function fetchViewProducts() {
  const resProduct = await makeRequestXHR('get', {
    url: `/products/viewed`,
  });
  return resProduct.data;
}

export async function fetchSelectedProduct(params) {
  try {
    const productSlug = params[1];
    const markSlug = params[0];
    const resProduct = await makeRequestXHR('get', {
      url: `/products/${productSlug}?markSlug=${markSlug}`,
    });
    return resProduct.data;
  } catch (error) {
    console.error(error);
    return error;
  }
}
