import { observer } from 'mobx-react-lite';
import Image from 'next/image';
import Link from 'next/link';
import { useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import AddSuccess from '@/entities/alerts/cart/AddSuccess';
import { AddToCartBtn } from '@/entities/cart';
import { CountBlock } from '@/entities/common/count-block';
import { AvailabilityBlock, CheckPriceBtn, Price } from '@/entities/product';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import { useGlobalStore } from '@/shared/hooks';
import markDefaultImage from '@/shared/images/markDefaultImage.jpg';
import { Typography } from '@/shared/ui';
import Preloader from '@/shared/ui/Preloader';

import s from './ProductInfo.module.scss';

const ProductInfo = observer((props) => {
  const { productData } = props;
  const { name = 'Наименование марки', price = 0, quantity = '0' } = productData;
  //TODO: вынести стейт, так как отрабатывает из-за условия
  const [isOpen, setIsOpen] = useState(false);
  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);
  const handlerModalOpen = useCallback(() => {
    setIsOpen(true);
  }, []);

  const markLink = `/cable/${productData?.mark.slug}`;
  const { cart } = useGlobalStore();
  const [localElement, setUpdateLocalElement] = useState(() => {
    productData._quantity = 1;
    return productData;
  });
  const [isLoading, setIsLoading] = useState(false);

  const img = productData?.image
    ? productData.image
    : productData.mark?.image
      ? encodeURI(productData.mark?.image.trim())
      : markDefaultImage.src;

  const addToCart = useCallback(async () => {
    setIsLoading(true);
    await cart.addToCart(localElement.product_id, localElement._quantity, localElement);
    setIsOpen(true);
    setUpdateLocalElement((prev) => {
      return { ...prev, _quantity: 1 };
    });
    setIsLoading(false);
  }, [localElement, cart]);

  return (
    <section className={s.product__section}>
      {isLoading ? <Preloader /> : null}
      <header itemProp="name">
        <Typography variant="title_xl" boldness="bold">
          Кабели {productData?.raw_name}
        </Typography>
      </header>
      <section className={s.product__body}>
        <div
          itemScope
          itemProp="image"
          itemType="http://schema.org/ImageObject"
          className={s.product__imgWrapper}
        >
          <Image
            itemProp="url contentUrl"
            className={s.img}
            src={img}
            width={300}
            height={288}
            title={`Кабели ${productData?.raw_name}`}
            alt={`Кабели ${productData?.raw_name}`}
          />
        </div>
        <div className={s.product__contentWrapper}>
          <section className={s.product__contentMarkWrapper}>
            <Typography variant="title_sm" boldness="medium">
              Марка:{' '}
              <Link className={s.link} href={markLink}>
                {productData?.mark.name}
              </Link>
            </Typography>
            <Typography itemProp="name" variant="title_sm" boldness="medium">
              {name.value}
            </Typography>
          </section>
          {price ? (
            <>
              <div
                className={s.product__contentBlockWrapper}
                itemProp="offers"
                itemScope
                itemType="http://schema.org/Offer"
              >
                <section className={s.product__contentPriceWrapper}>
                  <Price price={price} />
                </section>
                <section className={s.product__contentAvailabilityWrapper}>
                  <AvailabilityBlock availabilityValue={quantity} />
                </section>
              </div>
              <section className={s.product__contentBlockWrapper}>
                <div className={s.product__countBlockWrapper}>
                  <CountBlock element={localElement} setUpdateCount={setUpdateLocalElement} />
                </div>
                <AddToCartBtn handler={addToCart} />
                {/*  <BuyOneClickBtn /> */}
              </section>
              <AddSuccess isOpen={isOpen} callbackOnClose={handlerModalClose} />
            </>
          ) : (
            <>
              <CheckPriceBtn handler={handlerModalOpen} />
              <ModalWrapper
                isOpen={isOpen}
                handlerModalClose={handlerModalClose}
                handler={submitContactForm}
                ContentModalForm={(props) => <FeedbackForm {...props} />}
              />
            </>
          )}
          <footer>
            <Typography>
              *Предварительная цена при оптовом заказе. Цены и наличие указанные на сайте не
              являются публичной офертой. Цена зависит от количества, производителя и сроков
              поставки продукции. Для уточнения цены и наличия в вашем городе обращайтесь в отдел
              продаж
            </Typography>
          </footer>
        </div>
      </section>
    </section>
  );
});
export default ProductInfo;
