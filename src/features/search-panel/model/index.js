import makeRequestXHR from '@/shared/api/http';

export async function onSearchingProcess(params, signal) {
  const response = makeRequestXHR('get', {
    url: `/search/list/?query=${params.query}`,
    signal,
  });
  return response;
}

export async function doFetchSearch(params, signal) {
  const requestData = makeRequestXHR('get', {
    url: '/search',
    params: params,
    signal,
  });
  return requestData;
}
