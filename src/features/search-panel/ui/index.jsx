import debounce from 'debounce';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';

import { useClickOutside } from '@/shared/hooks';
import img from '@/shared/images/search.svg';
import Preloader from '@/shared/ui/Preloader';

import { onSearchingProcess } from '../model';
import s from './SearchPanel.module.scss';

const regExp = /[А-яA-z\d]{3,}/i;

const SearchDropdown = (props) => {
  const { result, setLoadingOutput, setSuggestions, setValue } = props;

  return (
    <ul className={s.listTips}>
      {result.length ? (
        result.map((el, index) => {
          return (
            <li key={index}>
              <Link
                href={el.url}
                onClick={() => {
                  setLoadingOutput(true);
                  setSuggestions([]);
                  setValue('');
                }}
              >
                {el.name}
              </Link>
            </li>
          );
        })
      ) : (
        <li>По запросу ничего не найдено</li>
      )}
    </ul>
  );
};

const updateSuggestion = debounce((value, setSuggestions, setLoading, abortController) => {
  abortController?.abort?.();
  if (value.length > 1 && regExp.test(value)) {
    setLoading(true);
    onSearchingProcess({ query: value } /* , signal */)
      .then((res) => {
        setSuggestions(res.data);
      })
      .catch((err) => {
        console.error('error', err);
      })
      .finally(() => {
        setLoading(false);
      });
  } else {
    setSuggestions([]);
  }
}, 800);

//FIXME: обернуть в FormPROVIDER и разнести в хук
const SearchPanel = () => {
  const [value, setValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isLoadingOutput, setLoadingOutput] = useState(false);
  const router = useRouter();
  const abortController = useRef(null);
  const ref = useRef(null);

  const onChange = async (event) => {
    abortController.current?.abort?.();
    abortController.current = new AbortController();
    setValue(event.target.value);
    updateSuggestion(event.target.value, setSuggestions, setLoading, abortController.current);
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    if (value.length > 1 && regExp.test(value)) {
      setLoadingOutput(true);
      // eslint-disable-next-line no-undef
      ym(56820658, 'reachGoal', 'search');
      setTimeout(() => {
        router.push({
          pathname: '/search',
          query: {
            query: value,
          },
        });
        setSuggestions([]);
      }, 1000);
    }
  };

  const onResetWhileClickOutside = () => {
    setSuggestions([]);
    setValue('');
  };

  useClickOutside(ref, onResetWhileClickOutside);

  useEffect(() => {
    return () => abortController.current?.abort?.();
  }, []);

  useEffect(() => {
    router.events.on('routeChangeComplete', () => {
      setLoadingOutput(false);
      setValue('');
    });
  }, [router]);

  return (
    <div className={s.wrapper} ref={ref}>
      {isLoadingOutput ? <Preloader /> : null}
      <form id="search" className={s.headerSearch} onSubmit={onSubmit}>
        <input
          id="live_search"
          className={s.headerSearch__input}
          placeholder="Введите марку или сечение"
          value={value}
          onChange={onChange}
          autoComplete="off"
        />
        <button className={s.headerSearch__btn} type="submit" form="search" title="Искать">
          <Image src={img} width="13" height="13" alt="search" />
        </button>
      </form>

      {isLoading ? (
        <ul className={s.listTips}>
          <li>Идет поиск...</li>
        </ul>
      ) : value ? (
        <SearchDropdown
          result={suggestions}
          setSuggestions={setSuggestions}
          setValue={setValue}
          setLoadingOutput={setLoadingOutput}
        />
      ) : null}
    </div>
  );
};

export default SearchPanel;
