import makeRequestXHR from '@/shared/api/http';

export async function fetchCurrentCity() {
  try {
    const response = await makeRequestXHR('get', {
      url: '/common/city-info',
    });
    return response;
  } catch (error) {
    console.error('errror', error);
    return error;
  }
}

export async function fetchCityList() {
  try {
    const response = makeRequestXHR('get', {
      url: '/common/city-list',
    });
    return response;
  } catch (error) {
    console.error('errror', error);
    return error;
  }
}
