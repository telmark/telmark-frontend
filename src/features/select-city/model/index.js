import { makeAutoObservable, toJS } from 'mobx';

import makeRequestXHR from '@/shared/api/http';

export default class CityStore {
  cityInfo = {
    list: [],
    currentCity: null,
    primaryCity: null,
  };
  state = null;

  constructor() {
    makeAutoObservable(this);
  }

  *fetchCityList() {
    this.state = 'pending';
    try {
      const response = yield makeRequestXHR('get', {
        url: '/common/city-list',
      });
      this.state = 'done';
      this.cityInfo['list'] = response.cityList;
    } catch (error) {
      console.error('error-fetchCityList-', error);
      this.state = 'error';
      throw new Error(error);
    }
  }

  setCurrentCity(serverData) {
    if (!serverData?.currentCity && !serverData?.primaryCity) {
      this.state = 'error';
      console.error('error-serverData-', serverData);
    } else {
      this.cityInfo['currentCity'] = serverData.currentCity;
      this.cityInfo['primaryCity'] = serverData.primaryCity;
    }
  }

  get cityList() {
    return toJS(this.cityInfo['list']);
  }

  get primaryCity() {
    const info = toJS(this.cityInfo['currentCity'])
      ? toJS(this.cityInfo['currentCity'])
      : toJS(this.cityInfo['primaryCity']);
    return info;
  }

  get fullAddress() {
    const info = toJS(this.cityInfo);
    let str = null;
    if (!info?.currentCity && !info?.primaryCity) {
      str =
        '196247, г. Санкт-Петербург, вн. тер. г. Муниципальный округ Новоизмайловское,пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24';
      return str;
    }

    if (info?.currentCity?.is_main === 0) {
      str = `${info?.currentCity.postcode}, г. ${info?.currentCity.name}, ${info?.currentCity.address}`;
    } else {
      str =
        '196247, г. Санкт-Петербург, вн. тер. г. Муниципальный округ Новоизмайловское,пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24';
    }

    return str;
  }
}
