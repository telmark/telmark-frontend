import { observer } from 'mobx-react-lite';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { RuntimeError } from '@/entities/common/runtime-error';
import { useGlobalStore } from '@/shared/hooks';
import Skeleton from '@/shared/ui/Skeleton';

import s from './Dropdown.module.scss';

const LoadingSkeleton = () => {
  return (
    <ul className={s.menu}>
      <Skeleton className={s.link} quantityElement={10} count={1} TagName="li" height={40} />
      <li>
        <hr className={s.hr} />
      </li>
      <Skeleton className={s.link} quantityElement={1} count={1} TagName="li" />
    </ul>
  );
};

const Dropdown = observer(() => {
  const { city } = useGlobalStore();
  const router = useRouter();

  useEffect(() => {
    if (city.cityList.length === 0) {
      (async () => {
        try {
          await city.fetchCityList();
        } catch (e) {
          console.error(e);
        }
      })();
    }
  }, [city]);

  if (city.state === 'pending') {
    return <LoadingSkeleton />;
  }

  if (city.state === 'error') {
    return <RuntimeError />;
  }

  return (
    <ul className={s.menu}>
      {city.cityList.map((el) => {
        return (
          <li key={el.city_id}>
            <a href={el.fullDomain + router.asPath} rel="nofollow noopener">
              <span className={s.link}>{el.name}</span>
            </a>
          </li>
        );
      })}
    </ul>
  );
});

export default Dropdown;
