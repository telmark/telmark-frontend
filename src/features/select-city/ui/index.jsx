import classes from 'classnames';
import dynamic from 'next/dynamic';
import { useEffect, useRef, useState } from 'react';

import { useClickOutside } from '@/shared/hooks';

import s from './SelectCity.module.scss';

const Dropdown = dynamic(() => import('./components/Dropdown', { ssr: false }));

const SelectCity = (props) => {
  const { children, classNameWrapper, className } = props;
  const ref = useRef();

  const [isOpen, setOpen] = useState(false);
  const onClose = () => {
    setOpen(false);
  };
  const clsNameWrapper = classes({
    [s.headerCity]: true,
    [`${classNameWrapper}`]: classNameWrapper,
  });

  const clsName = classes({
    [s.btn]: true,
    [`${className}`]: className,
  });

  useClickOutside(ref, setOpen);

  useEffect(() => {
    document.addEventListener('scroll', onClose);
    return () => document.addEventListener('scroll', onClose);
  }, []);

  if (!children) {
    return null;
  }

  return (
    <div className={clsNameWrapper} ref={ref}>
      <button
        className={clsName}
        title="Выбрать город"
        onClick={() => setOpen(!isOpen)}
        aria-expanded={isOpen}
      >
        {children}
      </button>
      {isOpen ? <Dropdown /> : null}
    </div>
  );
};
export default SelectCity;
