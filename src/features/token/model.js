// /api/token
import { makeAutoObservable, runInAction } from 'mobx';

import makeRequestXHR from '@/shared/api/http';

const expiredPeriod = 2 * 60 * 1000;

export default class TokenStore {
  csrf = null;
  state = null;
  constructor() {
    makeAutoObservable(this);
  }
  *fetchCsrfToken() {
    this.state = 'pending';
    try {
      let res = yield makeRequestXHR('post', {
        url: '/token',
        data: {},
      });
      //res = {};
      //this.csrf = res.token;
      if (!res?.token) {
        this.state = 'error';
        throw new Error(res);
      }
      this.csrf = res.token;
      this.isValidCsrfToken = true;
      this.state = 'done';
      runInAction(() => {
        const id = setTimeout(() => {
          clearTimeout(id);
          this.fetchCsrfToken();
        }, expiredPeriod);
      });
    } catch (error) {
      this.state = 'error';
      console.error('MobX err0r=fetchCsrfToken=', error);
      throw new Error(error);
    }
  }
}
