import Link from 'next/link';

import { Typography } from '@/shared/ui';

import s from './ArticlesPage.module.scss';

function ArticlesPage(props) {
  const { articleList } = props;
  return (
    <section className={s.wrapper}>
      <Typography variant="title_lg" boldness="bold">
        Статьи
      </Typography>
      <ul className={s.list__wrapper}>
        {articleList.map((article) => {
          return (
            <li key={article.article_id}>
              <Link
                href={{
                  pathname: `/articles/[slug]`,
                  query: { slug: article.slug },
                }}
              >
                <div className={s.article}>
                  <Typography variant="text_xl">{article.publish_date}</Typography>
                  <Typography variant="title_md" boldness="bold">
                    {article.title}
                  </Typography>
                  <Typography variant="text_xl">{article.short_desc}</Typography>
                </div>
              </Link>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
export default ArticlesPage;
