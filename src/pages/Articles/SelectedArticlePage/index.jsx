import logo from '@/shared/images/general/logo.webp';
import { ContainerHtml, Typography } from '@/shared/ui';

/* eslint-disable @next/next/no-img-element */
function SelectedArticlePage(props) {
  const { articleInfo } = props;
  return (
    <section itemScope itemType="https://schema.org/Article">
      <Typography itemProp="headline" variant="title_xxl" boldness="bold">
        {articleInfo.h1}
      </Typography>
      <img itemProp="image" src={articleInfo.img_url} style={{ display: 'none' }} alt="" />
      <meta itemScope itemProp="mainEntityOfPage" itemType="https://schema.org/WebPage" />
      <div
        itemProp="publisher"
        itemScope
        itemType="https://schema.org/Organization"
        style={{ display: 'none' }}
      >
        <div itemProp="logo" itemScope itemType="https://schema.org/ImageObject">
          <img itemProp="url image" src={logo} width={175} height={60} alt="Телмарк" />
        </div>
        <meta itemProp="name" content="Телмарк" />
      </div>
      <ContainerHtml itemProp="articleBody" tag="article" htmlContent={articleInfo.text} />
    </section>
  );
}
export default SelectedArticlePage;
