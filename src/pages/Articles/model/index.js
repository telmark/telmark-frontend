import makeRequestXHR from '@/shared/api/http';

export async function fetchArticleList() {
  const response = await makeRequestXHR('get', {
    url: '/articles',
  });
  return response.data;
}

export async function fetchSelectedArticle(articleId) {
  const response = await makeRequestXHR('get', {
    url: `/articles/${articleId}`,
  });
  return response.data;
}
