import { Typography } from '@/shared/ui';
import { CableSliderWidget } from '@/widgets/cable-slider/CableSliderWidget';

const CablePage = (props) => {
  const { catalogList } = props;

  return (
    <section>
      <Typography variant="title_lg" boldness="bold">
        Кабельно-проводниковая продукция
      </Typography>
      <CableSliderWidget catalogList={catalogList} />
    </section>
  );
};

export default CablePage;
