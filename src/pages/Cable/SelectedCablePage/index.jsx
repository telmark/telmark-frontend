import dynamic from 'next/dynamic';
import { useSearchParams } from 'next/navigation';

import { MarkList, MarkProduct } from '@/features/marks/ui';
import { ContainerHtml, Typography } from '@/shared/ui';
import Catalog from '@/widgets/catalog';

import s from './SelectedCablePage.module.scss';

const PopularMarkList = dynamic(() => import('@/features/marks/ui/PopularMarkList'), {
  ssr: true,
});

const MarkDescription = dynamic(() => import('@/features/marks/ui/MarkDescription'), {
  ssr: true,
});

function SelectedCablePage(props) {
  const { selectedPageInfo, catalogInfo } = props;
  const searchParams = useSearchParams();
  const pageQuery = searchParams.get('page');
  //console.log('selectedPageInfo==', selectedPageInfo);
  return (
    <>
      <section className={s.wrapper}>
        <Typography variant="title_xl" boldness="bold">
          {selectedPageInfo.title}
        </Typography>
        {selectedPageInfo.isMark ? (
          <MarkProduct marks={selectedPageInfo.marks} />
        ) : (
          <div>
            <Typography className={s.markTitle} variant="title_md" boldness="medium">
              Выберите марку
            </Typography>
            <MarkList markList={selectedPageInfo.marks} />
          </div>
        )}

        <Catalog
          catalogInfo={catalogInfo}
          image={selectedPageInfo.isMark ? selectedPageInfo.marks.image : null}
        />

        {selectedPageInfo.isMark && !pageQuery ? (
          <>
            <MarkDescription
              title={selectedPageInfo.title}
              description={selectedPageInfo?.marks?.description}
              short_desc={selectedPageInfo?.marks?.short_desc}
            >
              <PopularMarkList
                popularMarkList={selectedPageInfo.popularProducts}
                title={selectedPageInfo.title}
              />
            </MarkDescription>
          </>
        ) : (
          <>
            {selectedPageInfo?.categories.description ? (
              <section>
                <div
                  className="catalog-seo"
                  dangerouslySetInnerHTML={{ __html: selectedPageInfo?.categories.description }}
                />
              </section>
            ) : null}
          </>
        )}

        {!pageQuery ? (
          <ContainerHtml htmlContent={selectedPageInfo?.meta?.page_default_text} />
        ) : null}
      </section>
    </>
  );
}
export default SelectedCablePage;
