import dynamic from 'next/dynamic';

import { Description, FeatureList } from '@/entities/product';
import ProductInfo from '@/features/product/ui/ProductInfo';
import { ContainerHtml, TabsContent } from '@/shared/ui';

import s from './SelectedProductPage.module.scss';

const ContactForm = dynamic(() => import('@/features/contact-form/ui/ContactForm'), {
  ssr: false,
});

const TrackBlock = dynamic(() => import('@/widgets/track-block'), {
  ssr: false,
});

const mapTabsHeader = ['Описание', 'Характеристики'];

const SelectedProductPage = (props) => {
  const { productInfo } = props;
  const characteristicList =
    productInfo?.productCharacteristics.length > 0
      ? productInfo?.productCharacteristics
      : productInfo?.feature;

  const mapTabContent = productInfo?.description
    ? [
        <Description key={0} description={productInfo?.description} />,
        <FeatureList key={1} featureList={characteristicList} />,
      ]
    : [<FeatureList key={2} featureList={characteristicList} />];

  const tabHeaders = productInfo?.description ? mapTabsHeader : [mapTabsHeader[1]];

  return (
    <article>
      <article itemScope itemType="http://schema.org/Product" className={s.productInfo__section}>
        <ProductInfo productData={productInfo} />
        {productInfo?.short_desc ? (
          <section>
            <ContainerHtml htmlContent={productInfo?.short_desc} />
          </section>
        ) : null}
        {characteristicList.length > 0 ? (
          <>
            <section className={s.catalogItem__wrapper}>
              <TabsContent
                componentsTab={mapTabContent}
                tabsHeader={tabHeaders}
                classNameList={s.tabList__item}
              />
            </section>
          </>
        ) : null}
        <ContainerHtml htmlContent={productInfo?.meta?.page_default_text} />
      </article>
      <ContactForm />
      <TrackBlock title="Bы смотрели" />
    </article>
  );
};
export default SelectedProductPage;
