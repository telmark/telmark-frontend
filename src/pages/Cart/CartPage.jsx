import { observer } from 'mobx-react-lite';
import { useCallback, useEffect, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import ActionError from '@/entities/alerts/cart/ActionError';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import { useGlobalStore } from '@/shared/hooks';
import { Typography } from '@/shared/ui';
import Preloader from '@/shared/ui/Preloader';
import Skeleton from '@/shared/ui/Skeleton';

import s from './CartPage.module.scss';
import { CartPanel } from './components/CartPanel';
import { EmptyCart } from './components/EmptyCart';

const Content = observer((props) => {
  const { handlerModalOpen } = props;
  const { cart } = useGlobalStore();
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setLoaded(true);
    }, 1000);
  }, []);

  return (
    <>
      {cart.state === 'pending' ? <Preloader /> : null}
      {cart.state === 'error' ? <ActionError /> : null}
      <div className={s.cartPage__header}>
        <Typography variant="title_lg" boldness="bold">
          Корзина
        </Typography>

        <Typography>
          Предварительная цена при оптовом заказе. Цены и наличие указанные на сайте не являются
          публичной офертой. Цена зависит от количества, производителя и сроков поставки продукции.
          Для уточнения цены и наличия в вашем городе обращайтесь к менеджерам по телефону.
        </Typography>
      </div>
      {!isLoaded ? (
        <div>
          <Skeleton quantityElement={3} count={11} />
        </div>
      ) : cart.cartItemsLength ? (
        <CartPanel handlerModalOpen={handlerModalOpen} />
      ) : (
        <EmptyCart />
      )}
    </>
  );
});

const CartPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);
  const handlerModalOpen = useCallback(() => {
    setIsOpen(true);
  }, []);

  return (
    <article className={s.cartPage__wrapper}>
      <Content handlerModalOpen={handlerModalOpen} />
      <ModalWrapper
        isOpen={isOpen}
        handlerModalClose={handlerModalClose}
        handler={submitContactForm}
        ContentModalForm={(props) => <FeedbackForm {...props} />}
      />
    </article>
  );
};

export default CartPage;
