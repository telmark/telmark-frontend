import { CartBottom } from '@/widgets/cart/cart-bottom';
import { CartTable } from '@/widgets/cart/cart-table';

export const CartPanel = (props) => {
  const { handlerModalOpen } = props;
  return (
    <>
      <CartTable handlerModalOpen={handlerModalOpen} />
      <CartBottom />
    </>
  );
};
