import { memo } from 'react';

import { Button, Typography } from '@/shared/ui';

import s from './EmptyCart.module.scss';

export const EmptyCart = memo(() => {
  return (
    <div className={s.emptyCart__wrapper}>
      <Typography variant="title_lg" boldness="bold">
        Ваша корзина пуста
      </Typography>

      <Button type="link" to="/cable">
        Перейти в каталог
      </Button>
    </div>
  );
});
