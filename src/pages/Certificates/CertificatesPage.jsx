import { Typography } from '@/shared/ui';

import s from './CertificatesPage.module.scss';
import { CertificatesList, Description } from './components';

function CertificatesPage() {
  return (
    <article className={s.certificates__block}>
      <Typography variant="title_xxl" boldness="bold">
        Сертификаты и паспорта на кабель
      </Typography>
      <Description />
      <CertificatesList />
    </article>
  );
}
export default CertificatesPage;
