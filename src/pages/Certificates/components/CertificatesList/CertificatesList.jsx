import { memo, useState } from 'react';

import { Typography } from '@/shared/ui';
import Lightbox from '@/shared/ui/Lightbox';

import s from './CertificatesList.module.scss';
import { mapCertificateItems, mapGalleryDefault } from './data';

export const CertificatesList = memo(() => {
  const [open, setOpen] = useState(mapGalleryDefault);
  const lightBoxHandler = (key, value) => {
    setOpen((prev) => {
      prev[key] = value;
      return {
        ...prev,
      };
    });
  };
  return (
    <section>
      <ul className={s.list}>
        {mapCertificateItems.map((el) => {
          return (
            <li key={el.key} className={s.listItem}>
              <Typography variant="title_sm" boldness="medium">
                {el.title}
              </Typography>
              <ul className={s.wrapper}>
                {el.items.map((cert) => {
                  return (
                    <li key={cert.key}>
                      <picture>
                        <img
                          className={s.listItem__img}
                          src={cert.src}
                          alt="Сертификат"
                          onClick={() => lightBoxHandler(el.key, true)}
                        />
                      </picture>
                    </li>
                  );
                })}
              </ul>
              <Lightbox
                slideList={el.items}
                isOpen={open[el.key]}
                onClose={() => lightBoxHandler(el.key, false)}
              />
            </li>
          );
        })}
      </ul>
    </section>
  );
});
