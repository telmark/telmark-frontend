import ppng6 from '@/shared//images/general/Certificate/PvPG6.jpg';
import cert1 from '@/shared/images/general/Certificate/1.jpg';
import cert2 from '@/shared/images/general/Certificate/2.jpg';
import cert3 from '@/shared/images/general/Certificate/3.jpg';
import cert4 from '@/shared/images/general/Certificate/4.jpg';
import cert5 from '@/shared/images/general/Certificate/5.jpg';
import cert6 from '@/shared/images/general/Certificate/6.jpg';
import cert7 from '@/shared/images/general/Certificate/7.jpg';
import num from '@/shared/images/general/Certificate/NUM.jpg';
import ppng1 from '@/shared/images/general/Certificate/PvPG1.jpg';
import ppng2 from '@/shared/images/general/Certificate/PvPG2.jpg';
import ppng3 from '@/shared/images/general/Certificate/PvPG3.jpg';
import ppng4 from '@/shared/images/general/Certificate/PvPG4.jpg';
import ppng5 from '@/shared/images/general/Certificate/PvPG5.jpg';
import ppng7 from '@/shared/images/general/Certificate/PvPG7.jpg';
import ppng8 from '@/shared/images/general/Certificate/PvPG8.jpg';
import sip1 from '@/shared/images/general/Certificate/sip-1-2-4.jpg';
import sip2 from '@/shared/images/general/Certificate/sip-2-4-ss.jpg';
import sipKz from '@/shared/images/general/Certificate/sip-3-kz.jpg';
import sip3 from '@/shared/images/general/Certificate/sip-3.jpg';
import aac from '@/shared/images/general/Certificate/А-АС-М.jpg';
import pvc1 from '@/shared/images/general/Certificate/ПВС1.jpg';
import pvc2 from '@/shared/images/general/Certificate/ПВС2.jpg';
import pvc3 from '@/shared/images/general/Certificate/ПВС3.jpg';

export const mapCertificateItems = [
  {
    key: '1',
    title: `Кабель силовой с ПВХ изоляцией: ВВГ, АВВГ, АВБШв, АВВГнг(А), ВВГнг(А), ВБШв, АВБШвнг(А),
        ВБШвнг(А), КГВВ, КГВВЭ`,
    items: [
      {
        key: '1.1',
        src: cert1.src,
      },
      {
        key: '1.2',
        src: cert2.src,
      },
      {
        key: '1.3',
        src: cert3.src,
      },
      {
        key: '1.4',
        src: cert4.src,
      },
    ],
  },
  {
    key: '2',
    title: `Кабель контрольный: КВВГ,КВВГЦ,КВВГЭ,КВВГЭЦ,КВБбШв, КВБбШвЦ, КВВГнг(А), КВВГЦнг(А),
    КВВГЭнг(А),КВВГЭЦнг(А),КВБбШвнг(А), КВБбШвЦнг(А)`,
    items: [
      {
        key: '2.1',
        src: cert5.src,
      },
      {
        key: '2.2',
        src: cert6.src,
      },
      {
        key: '2.3',
        src: cert7.src,
      },
    ],
  },
  {
    key: '3',
    title: `Кабель гибкий: КГТП, КГТП-ХЛ`,
    items: [
      {
        key: '3.1',
        src: cert7.src,
      },
    ],
  },
  {
    key: '4',
    title: `Провода соединительные: ПВС, ПВСн, ПВСнг(А), ПВСнг(А)-LS`,
    items: [
      {
        key: '4.1',
        src: pvc1.src,
      },
      {
        key: '4.2',
        src: pvc2.src,
      },
      {
        key: '4.3',
        src: pvc3.src,
      },
    ],
  },
  {
    key: '5',
    title: `Кабель с изоляцией и оболочкой из полимерных композиций: ППГНГ(А)-HF,
    ППГНГ(А)-FRHF,ПВПГНГ(А)-HF, ПВПГНГ(А)-FRHF`,
    items: [
      {
        key: '5.1',
        src: ppng1.src,
      },
      {
        key: '5.2',
        src: ppng2.src,
      },
      {
        key: '5.3',
        src: ppng3.src,
      },
      {
        key: '5.4',
        src: ppng4.src,
      },
      {
        key: '5.5',
        src: ppng5.src,
      },
      {
        key: '5.6',
        src: ppng6.src,
      },
      {
        key: '5.7',
        src: ppng7.src,
      },
      {
        key: '5.8',
        src: ppng8.src,
      },
    ],
  },
  {
    key: '6',
    title: `Кабель силовой с ПВХ изоляцией: NYM, NUM-j, NUM-o`,
    items: [
      {
        key: '6.1',
        src: num.src,
      },
    ],
  },
  {
    key: '7',
    title: `Провода изолированные для воздушных линий: СИП-1, СИП-2, СИП-3, СИП-4`,
    items: [
      {
        key: '7.1',
        src: sip1.src,
      },
      {
        key: '7.2',
        src: sip2.src,
      },
      {
        key: '7.3',
        src: sip3.src,
      },
      {
        key: '7.4',
        src: sipKz.src,
      },
    ],
  },
  {
    key: '8',
    title: `Провода неизолированные: А, АС, М`,
    items: [
      {
        key: '8.1',
        src: aac.src,
      },
      {
        key: '8.2',
        src: aac.src,
      },
      {
        key: '8.3',
        src: aac.src,
      },
    ],
  },
];
export const mapGalleryDefault = (() => {
  const obj = Object.create(null);
  mapCertificateItems.forEach((el) => (obj[el.key] = false));
  return obj;
})();
