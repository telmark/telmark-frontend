import { memo } from 'react';

import { Typography } from '@/shared/ui';

export const Description = memo(() => {
  return (
    <section>
      <Typography variant="text_default" mb="mb">
        Компания Телмарк несет ответственность за качество поставляемой кабельной продукции. Все
        наши товары отгружаются в соответствии с законодательством.
      </Typography>
      <Typography variant="text_default" mb="mb">
        Вместе с продукцией вы получаете:
      </Typography>
      <ul className="_new_listWithCirles">
        <li>
          <Typography variant="text_default" mb="mb">
            Комплект отгрузочных документов
          </Typography>
        </li>
        <li>
          <Typography variant="text_default" mb="mb">
            Обязательный или добровольный сертификат соответствия
          </Typography>
        </li>
        <li>
          <Typography variant="text_default" mb="mb">
            Для всех кабелей: Сертификат пожарной безопасности
          </Typography>
        </li>
        <li>
          <Typography variant="text_default" mb="mb">
            Для судовых кабелей: Сертификат речного регистра
          </Typography>
        </li>
        <li>
          <Typography variant="text_default" mb="mb">
            Паспорт качества от производителя
          </Typography>
        </li>
      </ul>
      <Typography variant="text_default" mb="mb">
        Состав документации зависит от поставляемой марки кабеля.
      </Typography>
      <Typography variant="text_default" boldness="medium" mb="mb">
        <Typography variant="text_span" color="alert">
          ВНИМАНИЕ!
        </Typography>{' '}
        Сертификаты и паспорта предоставляются после оплаты товара. <br />
        Ниже представлены образцы сертификатов.
      </Typography>
    </section>
  );
});
