import ContactForm from '@/features/contact-form/ui/ContactForm';
import { Typography } from '@/shared/ui';

import s from './CompanyPage.module.scss';
import CertList from './components/CertList';
import GostCards from './components/GostCards';
import TextSection from './components/TextSection';

function CompanyPage() {
  return (
    <article className={s.companyPage__wrapper}>
      <Typography variant="title_xxl" boldness="bold">
        О компании
      </Typography>
      <div className={s.company__wrapper}>
        <section>
          <TextSection />
        </section>
        <section>
          <Typography variant="title_sm" boldness="medium">
            Мы гарантирует высокое качество продукции.
          </Typography>
          <Typography variant="title_sm" boldness="medium">
            Все изделия производятся в соответствии с требованиями ГОСТ
          </Typography>
          <GostCards />
        </section>
        <section>
          <Typography variant="title_sm" boldness="medium">
            Наши сертификаты
          </Typography>
          <CertList />
        </section>
        <ContactForm />
      </div>
    </article>
  );
}
export default CompanyPage;
