import Image from 'next/image';
import { memo, useState } from 'react';

import loup from '@/shared/images/general/loup.svg';
import newCert from '@/shared/images/newCert.jpg';
import newCertFull from '@/shared/images/newCertFull.jpg';
import newLetter from '@/shared/images/newLetter.jpg';
import newLetterFull from '@/shared/images/newLetterFull.jpg';
import { Typography } from '@/shared/ui';
import Lightbox from '@/shared/ui/Lightbox';

import s from './CertList.module.scss';

/**
 * @CertList
 */

const certList = [
  {
    key: '1.1',
    title: 'УНИВЕРСАЛКАБЕЛЬ',
    src: newLetterFull.src,
    minSrc: newLetter.src,
  },
  {
    key: '1.2',
    title: 'ЭЛЕКТРОТЕХМАШ',
    src: newCertFull.src,
    minSrc: newCert.src,
  },
];

const CertList = memo(() => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <ul className={s.list}>
        {certList.map((el, index) => {
          return (
            <li key={index}>
              <div>
                <div className={s.img} onClick={() => setOpen(true)}>
                  <picture>
                    <Image src={el.minSrc} alt="Landscape picture" width={800} height={500} />
                  </picture>
                  <Image className={s.img__loupe} src={loup} alt="loupe" />
                </div>

                <Typography className={s.title} variant="title_sm" boldness="medium">
                  {el.title}
                </Typography>
              </div>
            </li>
          );
        })}
      </ul>
      <Lightbox slideList={certList} isOpen={open} onClose={() => setOpen(false)} />
    </>
  );
});

export default CertList;
