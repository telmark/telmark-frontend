import s from './GostCards.module.scss';
import gostList from './data';

/**
 * @GostCards
 */

const GostCards = () => {
  return (
    <ul className={s.list}>
      {gostList.map((el, index) => {
        return (
          <li key={index} className={s.item}>
            <h4 className={s.title}>{el.title}</h4>
            <div>
              {el.list.map((item) => {
                return (
                  <a
                    key={item.link}
                    href={item.link}
                    className={s.link}
                    title="Скачать договор"
                    download
                  >
                    {item.title}
                  </a>
                );
              })}
            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default GostCards;
