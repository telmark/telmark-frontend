import { observer } from 'mobx-react-lite';

import { Typography } from '@/shared/ui';

import s from './ContactPage.module.scss';
import { BranchesInfo, CompanyCard, ContactLinks, MainInfo, Requisites } from './components';

const ContactPage = observer((props) => {
  const { title, isMainCity } = props;
  return (
    <article className={s.contactPage__wrapper}>
      <article className={s.contactPage__wrapperContacts}>
        <Typography variant="title_xxl" className={s.titlePage} boldness="bold">
          {title}
        </Typography>
        <MainInfo />
        <Requisites />
        <ContactLinks />
      </article>
      <CompanyCard />
      <BranchesInfo isMainCity={isMainCity} />
    </article>
  );
});
export default ContactPage;
