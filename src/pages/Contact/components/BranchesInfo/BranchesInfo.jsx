import { Typography } from '@/shared/ui';

import s from './BranchesInfo.module.scss';

export function BranchesInfo() {
  return (
    <article className={s.branchInfo__block}>
      <Typography variant="title_xxl" boldness="bold">
        Филиалы-склады в регионах РФ
      </Typography>
      <section className={s.branchInfo__section}>
        <div className={s.branches__text}>
          <Typography variant="text_default" mb="mb">
            Наш головной офис расположен в Санкт-Петербурге по адресу: г. Санкт-Петербург, пл.
            Конституции, д. 2 литера а, помещение 19н ч.п. 24.
            <br /> Связаться с работниками склада можно по номеру:{' '}
            <a href="tel:88006004091" className={`${s.branches__textLink} ${s.branches__Phone}`}>
              <Typography variant="text_span" boldness="medium">
                8 800 600 40 91
              </Typography>
            </a>
          </Typography>
          <Typography variant="text_default" mb="mb">
            У склада большая территория с изолированными и открытыми площадями хранения. На складе
            выполняются все виды погрузочно-разгрузочных работ, а также работ по упаковке товаров
            перед отправкой:
          </Typography>
          <ul className="_new_listWithCirles">
            <li>
              <Typography variant="text_default" mb="mb">
                Современные вилочные манипуляторы и погрузчики с разной грузоподъемностью, а также
                козловые краны позволяют аккуратно погружать упакованный груз без ущерба для его
                целостности.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                Высокоточное сертифицированное автоматическое оборудование с современными
                измерительными системами позволяют отмерить и перемотать необходимое количество
                кабеля. Приборы работают на высоких скоростях, что позволяет в минимальные сроки
                подготовить товар к отправке в пункт назначения. Ежегодно проводится поверка
                оборудования, чтобы обеспечить максимальную точность и поддерживать высокие
                стандарты работы.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                Автомобили для доставки оснащены дополнительными инструментами для надежной фиксации
                во время транспортировки грузов.
              </Typography>
            </li>
          </ul>
          <p>
            У компании также есть склады и в других городах России, где также соблюдены
            вышеуказанные стандарты работы. Единый номер:{' '}
            <a href="tel:88006004091" className={s.branches__textLink}>
              <Typography variant="text_span" boldness="medium">
                8 800 600 40 91
              </Typography>
            </a>
          </p>
        </div>
        <div className={s.branchInfo__wrapper}>
          <ul className={s.branches__list}>
            <li className={s.branches__listItem}>г. Москва ул. Адмирала Макарова, 2, стр. 19</li>
            <li className={s.branches__listItem}>г. Волгоград ул. Ткачёва, 30</li>
            <li className={s.branches__listItem}>г. Екатеринбург ул. Сакко и Ванцетти, 62</li>
            <li className={s.branches__listItem}>г. Казань ул. Родины, 7, корп. 13</li>
            <li className={s.branches__listItem}>г. Нижний Новгород ул. Родионова, 17</li>
            <li className={s.branches__listItem}>г. Новосибирск Большая ул., 280</li>
            <li className={s.branches__listItem}>г. Омск Енисейская ул., 1/9</li>
            <li className={s.branches__listItem}>г. Ростов-на-Дону Волоколамская ул., 10</li>
          </ul>
          <ul className={s.branches__list}>
            <li className={s.branches__listItem}>г. Самара ул. Стара Загора, 147</li>
            <li className={s.branches__listItem}>г. Уфа ул. Юрия Гагарина, 10</li>
            <li className={s.branches__listItem}>г. Челябинск Копейское ш., 25</li>
            <li className={s.branches__listItem}>
              г. Красноярск ул. Академика Вавилова, 1,
              <br /> стр. 67
            </li>
            <li className={s.branches__listItem}>г. Краснодар Ростовское ш., 26/2</li>
            <li className={s.branches__listItem}>г. Воронеж ул. Землячки, 15</li>
            <li className={s.branches__listItem}>г. Пермь ул. Космонавта Леонова, 86</li>
          </ul>
        </div>
      </section>
    </article>
  );
}
