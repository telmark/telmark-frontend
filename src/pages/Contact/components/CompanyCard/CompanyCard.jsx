// import Image from 'next/image';
import { memo, useRef, useState } from 'react';

import { useCopyToClipboard } from '@/shared/hooks';
import { Typography } from '@/shared/ui';

import s from './CompanyCard.module.scss';
import IconCopy from './IconCopy';

export const CompanyCard = memo(() => {
  const copyToClipboard = useCopyToClipboard();
  const [activeButtonId, setActiveButtonId] = useState(null);
  const timers = useRef({});

  const handleCopyClick = (text, buttonNumber) => {
    copyToClipboard(text);

    if (timers.current[buttonNumber]) {
      clearTimeout(timers.current[buttonNumber]);
    }

    setActiveButtonId(buttonNumber);

    timers.current[buttonNumber] = setTimeout(() => {
      setActiveButtonId(null);
      delete timers.current[buttonNumber];
    }, 2000);
  };

  const buttonsData = [
    {
      text: 'Общество с ограниченной ответственностью «Телмарк»',
      number: '1',
      title: 'Наименование организации',
      isLong: true,
    },
    { text: 'ООО «Телмарк»', number: '2', title: 'Сокращенное наименование', isLong: true },
    { text: '7806464579', number: '3', title: 'ИНН', isLong: false },
    { text: '780501001', number: '4', title: 'КПП', isLong: false },
    { text: '30656979', number: '5', title: 'ОКПО', isLong: false },
    { text: '1117847459223', number: '6', title: 'ОГРН', isLong: false },
    {
      text: 'Российская Федерация, 198216, Город Санкт-Петербург, вн.тер.г. муниципальный округ Княжево, пр-кт Трамвайный, д. 32, литера А, помещ. 3-Н офис 115',
      number: '7',
      title: 'Юридический адрес',
      isLong: true,
    },
    {
      text: 'Российская Федерация, 198216, Город Санкт-Петербург, вн.тер.г. муниципальный округ Княжево, пр-кт Трамвайный, д. 32, литера А, помещ. 3-Н офис 115',
      number: '8',
      title: 'Фактический адрес',
      isLong: true,
    },
    {
      text: 'Киселев Игорь Владимирович действует на основании Устава',
      number: '9',
      title: 'Генеральный директор',
      isLong: true,
    },
    { text: '407 028 108 551 300 05 326', number: '10', title: 'Расчетный счет', isLong: false },
    {
      text: '301 018 105 000 000 00 653',
      number: '11',
      title: 'Корреспондентский счет',
      isLong: false,
    },
    {
      text: 'СЕВЕРО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК г. Санкт-Петербург',
      number: '12',
      title: 'Банк',
      isLong: false,
    },
    { text: '044030653', number: '13', title: 'БИК', isLong: false },
    { text: '+7 812 603 49 51', number: '14', title: 'Телефон/ факс', isLong: false },
    { text: 'sale@telmark.ru', number: '15', title: 'E-mail', isLong: false },
  ];

  return (
    <section className={s.requisites__wrapper}>
      <Typography variant="title_md" boldness="semibold" className={s.requisites__title}>
        УЧЕТНАЯ КАРТОЧКА ООО «Телмарк»
      </Typography>
      <div className={s.requisites__grid}>
        {buttonsData.map((button) => (
          <div
            key={button.number}
            className={`${s.requisites__gridItem} ${button.isLong ? s.requisites__gridItemLong : ''}`}
          >
            <Typography
              variant="title_sm"
              boldness="medium"
              className={s.requisites__gridItemTitle}
            >
              {button.title}
            </Typography>
            <div className={s.requisites__gridItemTextWrapper}>
              <Typography
                variant="text_default"
                boldness="medium"
                className={s.requisites__gridItemText}
              >
                {button.text}
              </Typography>
              <button
                className={`${s.requisites__gridItemCopy} ${activeButtonId === button.number ? s.active : ''}`}
                onClick={() => handleCopyClick(button.text, button.number)}
              >
                <IconCopy className={s.requisites__gridItemCopyIcon} />
                <div className={s.requisites__gridItemCopyTip}>Скопировано!</div>
              </button>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
});
