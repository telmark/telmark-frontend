const IconCopy = ({ className }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    className={className}
  >
    <path
      d="M7.3125 9.81281C7.3125 9.14969 7.57593 8.51372 8.04482 8.04482C8.51372 7.57593 9.14969 7.3125 9.81281 7.3125H17.9372C18.6003 7.3125 19.2363 7.57593 19.7052 8.04482C20.1741 8.51372 20.4375 9.14969 20.4375 9.81281V17.9372C20.4375 18.6003 20.1741 19.2363 19.7052 19.7052C19.2363 20.1741 18.6003 20.4375 17.9372 20.4375H9.81281C9.14969 20.4375 8.51372 20.1741 8.04482 19.7052C7.57593 19.2363 7.3125 18.6003 7.3125 17.9372V9.81281Z"
      stroke="black"
      strokeOpacity="0.4"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M4.51125 16.4409C4.22376 16.277 3.98463 16.0402 3.81804 15.7542C3.65145 15.4683 3.5633 15.1434 3.5625 14.8125V5.4375C3.5625 4.40625 4.40625 3.5625 5.4375 3.5625H14.8125C15.5156 3.5625 15.8981 3.92344 16.2188 4.5"
      stroke="black"
      strokeOpacity="0.4"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default IconCopy;
