import { Button } from '@/shared/ui';

import s from './ContactLinks.module.scss';

export function ContactLinks() {
  return (
    <div className={s.contactsLinks__wrapper}>
      <a href="/uploads/УЧЕТНАЯ_КАРТОЧКА_ООО_«Телмарк»_НОВАЯ_4.doc" download>
        <Button title="Скачать реквизиты">Скачать реквизиты</Button>
      </a>
      <a href="/uploads/ТМ_Договор_поставки_утвержденная_редакция_1.docx" download>
        <Button title="Скачать договор">Скачать договор</Button>
      </a>
    </div>
  );
}
