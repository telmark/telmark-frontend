import { Map, Placemark, YMaps } from '@pbe/react-yandex-maps';

import { observer } from 'mobx-react-lite';
import Link from 'next/link';
import { useMemo } from 'react';

import { useGlobalStore } from '@/shared/hooks';
import { Typography } from '@/shared/ui';

import s from './MainInfo.module.scss';

export const MainInfo = observer(() => {
  const { city } = useGlobalStore();
  const defaultState = useMemo(() => {
    return {
      center: [city.primaryCity?.latitude || 59.853695, city.primaryCity?.longitude || 30.30313],
      controls: ['zoomControl', 'fullscreenControl'],
      zoom: 10,
    };
  }, [city.primaryCity]);

  return (
    <div className={s.wrapper}>
      <ul className={s.mainInfo__list}>
        <li>
          <div className={`${s.mainInfo__item} ${s.mainInfo__itemPhone}`}>
            <div className={s.mainInfo__item__name}>
              <Typography variant="text_span">Телефон:</Typography>
            </div>
            <Link href={`tel:${city.primaryCity?.phone}`}>
              <Typography
                variant="text_span"
                boldness="semibold"
                className={s.mainInfo__item__value}
              >
                {city.primaryCity?.phone}
              </Typography>
            </Link>
          </div>
        </li>
        <li>
          <div className={`${s.mainInfo__item} ${s.mainInfo__itemMail}`}>
            <div className={s.mainInfo__item__name}>
              <Typography variant="text_span">Почта для заказа:</Typography>
            </div>
            <Link href="mailto:sale@telmark.ru">
              <Typography
                variant="text_span"
                boldness="semibold"
                className={s.mainInfo__item__value}
              >
                sale@telmark.ru
              </Typography>
            </Link>
            {/*  <div className={s.mainInfo__item__name}>
              <Typography variant="text_span">Контакты менеджеров:</Typography>
            </div>
            <ul>
              <li>
                <Link href="mailto:sale105@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    sale105@telmark.ru
                  </Typography>{' '}
                  Сергей Сергеев
                </Link>
              </li>
              <li>
                <Link href="mailto:sale111@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    sale111@telmark.ru
                  </Typography>{' '}
                  Татьяна Осипова
                </Link>
              </li>
              <li>
                <Link href="mailto:sale109@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    sale109@telmark.ru
                  </Typography>{' '}
                  Кошелева Юлия
                </Link>
              </li>
              <li>
                <Link href="mailto:sale115@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    sale115@telmark.ru
                  </Typography>{' '}
                  Слащёва Юлия
                </Link>
              </li>
            </ul> */}
            <div className={s.mainInfo__item__name}>
              <Typography variant="text_span">Отдел закупок:</Typography>
            </div>
            <ul>
              <li>
                <Link href="mailto:zp110@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    zp110@telmark.ru
                  </Typography>{' '}
                  Благинин Юрий
                </Link>
              </li>
              <li>
                <Link href="mailto:sale117@telmark.ru">
                  <Typography
                    variant="text_span"
                    boldness="semibold"
                    className={s.mainInfo__item__value}
                  >
                    sale117@telmark.ru
                  </Typography>{' '}
                  Дорошенко Игорь
                </Link>
              </li>
            </ul>
          </div>
        </li>
        <li>
          <div className={`${s.mainInfo__item} ${s.mainInfo__itemAddress}`}>
            <div className={s.mainInfo__item__name}>
              <Typography variant="text_span">Адрес:</Typography>
            </div>
            <Typography variant="text_default" boldness="semibold">
              {city.fullAddress}
            </Typography>
          </div>
        </li>
      </ul>
      <div className={s.mainInfo__mapWrapper}>
        <YMaps>
          <Map
            defaultState={defaultState}
            width="100%"
            height="100%"
            modules={['control.ZoomControl', 'control.FullscreenControl']}
          >
            <Placemark
              geometry={[
                city.primaryCity?.latitude || 59.853695,
                city.primaryCity?.longitude || 30.30313,
              ]}
            />
          </Map>
        </YMaps>
      </div>
    </div>
  );
});
