import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './Requisites.module.scss';

export const Requisites = memo(() => {
  return (
    <div className={s.requisites__wrapper}>
      <section className={s.requisites__item}>
        <Typography variant="text_xl" boldness="bold" className={s.card__title}>
          Юридический адрес:
        </Typography>
        <Typography variant="text_default">
          196247, г. Санкт-Петербург, вн. тер. г. Муниципальный округ
          <br /> Новоизмайловское, Пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24
        </Typography>
      </section>
      <section className={s.requisites__item}>
        <Typography variant="text_xl" boldness="bold" className={s.card__title}>
          Почтовый адрес:
        </Typography>
        <Typography variant="text_default">
          196247, г. Санкт-Петербург, вн. тер. г. Муниципальный округ
          <br /> Новоизмайловское, Пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24
        </Typography>
      </section>
      <section className={s.requisites__item}>
        <Typography variant="text_xl" boldness="bold" className={s.card__title}>
          Реквизиты ООО “ТЕЛМАРК”
        </Typography>
        <Typography variant="text_default">
          ИНН/КПП 7806464579/781001001 Банк: СЕВЕРО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК г. Санкт-Петербург
          <br /> БИК: 044030653
          <br /> ОГРН: 1117847459223
        </Typography>
      </section>
    </div>
  );
});
