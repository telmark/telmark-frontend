import { useMemo } from 'react';

import ContactForm from '@/features/contact-form/ui/ContactForm';

import s from './HomePage.module.scss';
import AboutSection from './components/About/AboutSection';
import AdvantagesSection from './components/Advantages/AdvantagesSection';
import CatalogSection from './components/Catalog/CatalogSection';
import IntroSection from './components/Intro/IntroSection';
import PartnersSection from './components/Partners/PartnersSection';
import PopularList from './components/Popular/PopularList';
import ReviewsSection from './components/Reviews/ReviewsSection';

const HomePage = (props) => {
  const { catalogCategory } = props;
  // for common Catalog's contract
  const modified = useMemo(() => {
    const obj = {};
    Object.entries(catalogCategory).forEach(([k, v]) => {
      obj[k] = v.map((el) => ({
        ...el,
        raw_name: el.raw_name,
        markSlug: el.mark?.slug,
      }));
    });
    return obj;
  }, [catalogCategory]);

  return (
    <div className={s.wrapper}>
      <IntroSection />
      <CatalogSection catalogCategory={modified} />
      <PopularList popularCategory={catalogCategory.popularCategories} />
      <AdvantagesSection />
      <ContactForm />
      <ReviewsSection />
      <AboutSection />
      <PartnersSection />
    </div>
  );
};

export default HomePage;
