import Image from 'next/image';
import { memo } from 'react';

import about from '@/shared/images/general/mainAbout.png';
import { Typography } from '@/shared/ui';

import s from './AboutSection.module.scss';

const AboutSection = memo(() => {
  return (
    <article className={s.mainAbout}>
      <Typography variant="title_lg" boldness="bold">
        О нас
      </Typography>
      <div className={s.wrapper}>
        <section>
          <Typography>
            Магазин кабеля ООО &laquo;Телмарк&raquo; - это широкий ассортимент кабеля, свето- и
            электротехники, инструментов для монтажа по доступным ценам!
          </Typography>
          <Typography>
            Если вы ищете качественный товар по низким ценам, с гарантией качества и быстрой
            доставкой &ndash; добро пожаловать в &laquo;Телмарк&raquo;. Магазин уже более 8 лет
            является лидером в своей сфере и за годы работы успел зарекомендовать себя как надежного
            и гарантированного поставщика.
          </Typography>
          <Typography>
            За годы работы мы сформировали команду профессионалов, которая поможет вам подобрать
            товары высокого качества в соответствии с вашим бюджетом и предпочтениями.
            <br />
            &nbsp;Служба доставка быстро обрабатывает заказы и доставляет их к четко указанному
            сроку в любой город страны, а также за ее пределы. Весь товар доставляется целым и
            невредимым.
          </Typography>
          <Typography>
            У нас самые доступные цены без дополнительных накруток &ndash; мы работаем напрямую с
            лучшими поставщиками России, список которых постоянно расширяется и обновляется.
            Постоянным клиентам предоставляются дополнительные скидки.
          </Typography>
          <Typography>
            У нас собственные склады по всей территории страны, где установлено современное
            оборудование, чтобы быстро отмерить, перемотать и отгрузить необходимый клиенту товар.
          </Typography>
        </section>
        <div className={s.img}>
          <Image
            src={about}
            width={457}
            height={436}
            alt="img"
            priority={false}
            style={{ width: 457, height: 436 }}
          />
        </div>
      </div>
    </article>
  );
});

export default AboutSection;
