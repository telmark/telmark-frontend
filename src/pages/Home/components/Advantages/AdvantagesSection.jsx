import Image from 'next/image';
import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './AdvantagesSection.module.scss';
import { mapAdvantagesItems } from './data';

const AdvantagesSection = memo(() => {
  return (
    <article className={s.mainAdvantages}>
      <Typography variant="title_lg" boldness="bold">
        Наши преимущества
      </Typography>
      <ul className={s.list}>
        {mapAdvantagesItems.map((el) => {
          return (
            <li key={el.id} className={s.mainAdvantagesList__item}>
              <div className={s.title}>
                <Image className={s.img} src={el.img} width="47" height="55" alt="img" />
                <Typography variant="title_md" boldness="medium">
                  {el.title}
                </Typography>
              </div>
              <Typography className={s.txt}>{el.text}</Typography>
            </li>
          );
        })}
      </ul>
    </article>
  );
});

export default AdvantagesSection;
