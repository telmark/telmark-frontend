import adv1 from '@/shared/images/general/advantages-1.svg';
import adv2 from '@/shared/images/general/advantages-2.svg';
import adv3 from '@/shared/images/general/advantages-3.svg';
import adv4 from '@/shared/images/general/advantages-4.svg';
import adv5 from '@/shared/images/general/advantages-5.svg';
import adv6 from '@/shared/images/general/advantages-6.svg';

export const mapAdvantagesItems = [
  {
    id: 1,
    img: adv1,
    title: 'Качество',
    text: `Вся представленная нами продукция соответствует требованиям нормативных документов
    РФ`,
  },
  {
    id: 2,
    img: adv2,
    title: 'Страхование',
    text: `Страхуем грузы в компании надежного страхового партнера`,
  },
  {
    id: 3,
    img: adv3,
    title: 'Честная цена',
    text: `Являясь дилером и официальным партнером российских заводов, мы предлагаем выгодные
    цены`,
  },
  {
    id: 4,
    img: adv4,
    title: 'Доставка/оплата',
    text: `Осуществляем доставку продукции по всей территории РФ, даже в самые отдаленные
    уголки страны`,
  },
  {
    id: 5,
    img: adv5,
    title: 'Возврат',
    text: `Гарантируем возврат или обмен товара согласно действующему законодательству РФ`,
  },
  {
    id: 6,
    img: adv6,
    title: 'Ответ за 15 минут',
    text: `Оставьте заявку любым удобным способом, и в течении 15 минут менеджер свяжется с
    Вами`,
  },
];
