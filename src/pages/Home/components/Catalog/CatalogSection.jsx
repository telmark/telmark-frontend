import { CatalogPanel } from '@/features/catalog';
import { Button, TabsContent } from '@/shared/ui';

import s from './CatalogSection.module.scss';

const mapTabsHeader = ['Всегда в наличии', 'Популярное', 'Акция'];

const CatalogSection = (props) => {
  const { catalogCategory } = props;
  const mapContentCatalog = [
    <CatalogPanel key={0} mapCatalogBody={catalogCategory.adviceProducts} />,
    <CatalogPanel key={1} mapCatalogBody={catalogCategory.popularProducts} />,
    <CatalogPanel key={2} mapCatalogBody={catalogCategory.saleProducts} />,
  ];

  return (
    <article className={s.mainCatalog}>
      <TabsContent tabsHeader={mapTabsHeader} componentsTab={mapContentCatalog} />
      <Button type="link" to="/cable" className={s.catalogBtn}>
        Перейти в каталог
      </Button>
    </article>
  );
};

export default CatalogSection;
