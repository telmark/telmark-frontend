import Link from 'next/link';
import { memo, useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import { Button } from '@/shared/ui';
import Slider from '@/shared/ui/Slider';

import s from './IntroSection.module.scss';

const IntroSection = memo(() => {
  const [isOpen, setIsOpen] = useState(false);

  const [formData, setFormData] = useState({
    name: '',
    phone: '',
    email: '',
    comment: '',
    contact_private_policy: true,
    file: undefined,
  });

  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const handlerModalOpen = useCallback(() => {
    setIsOpen(true);
  }, []);

  const List = [
    <>
      <div className={`${s.introSlide} ${s.introSlide__first}`}>
        <div className={s.introSlideTop}>
          <h2 className={s.introSlideTitle}>
            Мороз не помеха! <br />
            Подключайте уверенно <br />с
            <span className={s.introSlideTitle__blue}> хладостойкими кабелями</span>
          </h2>
          <div className={s.introSlideCatalogLinks}>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/pgva-xl">
              ПГВА-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/kgtp-xl">
              КГтп-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/elkaflex-kgn-xl">
              ELKAFLEX КГН-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/pv3-xl">
              ПВ3-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/kpg-xl">
              КПГ-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__item} href="/cable/pugv-xl">
              ПуГВ-ХЛ
            </Link>
            <Link className={s.introSlideCatalogLinks__itemMore} href="/cable/kabel-hl">
              и другие
            </Link>
          </div>
        </div>
        <div className={s.introSlideContacts}>
          <a className={s.introSlideContacts__item} href="mailto:sale13@telmark.ru">
            sale13@telmark.ru
          </a>
          <a className={s.introSlideContacts__item} href="tel:+7(800)600-40-91">
            8 800 600 40 91
          </a>
        </div>
      </div>
    </>,
    <>
      <div className={`${s.introSlide} ${s.introSlide__second}`}>
        <div className={s.introSlideTop}>
          <h2 className={s.introSlideTitle}>
            Всё для <span className={s.introSlideTitle__blue}>вашего проекта</span> — <br />
            быстро и надежно
          </h2>
          <p className={s.introSlideDescription}>
            Комплексный подбор кабельной продукции <br />с гарантией качества
          </p>
        </div>
        <Button onClick={handlerModalOpen} title="Отправить заявку" className={s.introSlideBtn}>
          Обсудить проект
        </Button>
        <div className={s.introSlideContacts}>
          <a className={s.introSlideContacts__item} href="mailto:sale13@telmark.ru">
            sale13@telmark.ru
          </a>
          <a className={s.introSlideContacts__item} href="tel:+7(800)600-40-91">
            8 800 600 40 91
          </a>
        </div>
      </div>
    </>,
  ];

  return (
    <article>
      <Slider
        classNameWrapper={s.introSliderWrapper}
        sliderList={List}
        breakpoints={{
          300: {
            slidesPerView: 1,
          },
          576: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 1,
            pagination: {
              bulletClass: 'swiper-pagination-bullet',
              clickable: true,
            },
          },
          1023: {
            slidesPerView: 1,
          },
        }}
      />

      <ModalWrapper
        isOpen={isOpen}
        formData={formData}
        setFormData={setFormData}
        handlerModalClose={handlerModalClose}
        handler={submitContactForm}
        ContentModalForm={(props) => <FeedbackForm {...props} />}
      />
    </article>
  );
});

export default IntroSection;
