import Image from 'next/image';
import { memo, useEffect, useState } from 'react';

import { Typography } from '@/shared/ui';
import Slider from '@/shared/ui/Slider';

import s from './PartnerSection.module.scss';
import { mapPartnerItems } from './data';

const PartnersSection = memo(() => {
  const [domLoaded, setDomLoaded] = useState(false);
  const List = mapPartnerItems.map((el, index) => {
    return (
      <div key={index} className={s.mainPartners__slide}>
        <Image src={el.img} alt={el.alt} width={240} height={65} />
      </div>
    );
  });

  useEffect(() => {
    setDomLoaded(true);
  }, []);

  return (
    <article className={s.mainPartners}>
      <Typography variant="title_lg" boldness="bold" className={s.mainPartners__title}>
        Наши партнеры
      </Typography>
      {domLoaded ? (
        <Slider
          classNameWrapper={s.sliderWrapper}
          navigation={false}
          sliderList={List}
          spaceBetween={54}
          breakpoints={{
            300: {
              slidesPerView: 1,
            },
            576: {
              slidesPerView: 2,
            },
            768: {
              slidesPerView: 3,
            },
            1023: {
              slidesPerView: 4,
            },
          }}
        />
      ) : null}
    </article>
  );
});

export default PartnersSection;
