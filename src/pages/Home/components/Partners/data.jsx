import partner1 from '@/shared/images/partner1.svg';
import partner2 from '@/shared/images/partner2.svg';
import partner3 from '@/shared/images/partner3.svg';
import partner4 from '@/shared/images/partner4.svg';
import partner5 from '@/shared/images/partner5.png';
import partner6 from '@/shared/images/partner6.png';
import partner7 from '@/shared/images/partner7.jpeg';

export const mapPartnerItems = [
  {
    id: 1,
    img: partner1.src,
    alt: 'partner1',
  },
  {
    id: 2,
    img: partner2.src,
    alt: 'partner2',
  },
  {
    id: 3,
    img: partner3.src,
    alt: 'partner3',
  },
  {
    id: 4,
    img: partner4.src,
    alt: 'partner4',
  },
  {
    id: 5,
    img: partner5.src,
    alt: 'partner5',
  },
  {
    id: 6,
    img: partner6.src,
    alt: 'partner6',
  },
  {
    id: 7,
    img: partner7.src,
    alt: 'partner7',
  },
];
