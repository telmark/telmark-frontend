import Link from 'next/link';

import { Typography } from '@/shared/ui';

import s from './PopularList.module.scss';
import { mapImagePopularCategory } from './data';

const PopularList = (props) => {
  const { popularCategory } = props;
  return (
    <article className={s.mainPopular__wrapper}>
      <Typography variant="title_lg" boldness="bold">
        Популярные категории
      </Typography>
      <ul className={s.mainPopular__list}>
        {popularCategory?.map((elCategory, index) => {
          return (
            <li key={index} className={s.card}>
              <div
                className={s.card__img}
                style={{
                  backgroundImage: `url(${mapImagePopularCategory[elCategory['name']]}) `,
                }}
              ></div>
              <div className={s.card__content}>
                <div className={s.card__title}>{elCategory.name}</div>
                <ul className={s.card__list}>
                  {elCategory.categories.map((el) => {
                    return (
                      <li key={el.category_id}>
                        <Link
                          href={{
                            pathname: '/cable/[...slug]',
                            query: { pageName: el.singular_name },
                          }}
                          as={`/cable/${el.slug}`}
                          className={s.card__link}
                        >
                          {el.singular_name}
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </li>
          );
        })}
      </ul>
    </article>
  );
};

export default PopularList;
