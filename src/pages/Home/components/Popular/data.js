import cable2 from '@/shared/images/typeCabelMainPage2.webp';
import cable3 from '@/shared/images/typeCabelMainPage3.webp';
import cable from '@/shared/images/typeCabelMainPage.webp';

export const mapImagePopularCategory = {
  'По типу': cable.src,
  'По исполнению': cable2.src,
  'По применению': cable3.src,
};
