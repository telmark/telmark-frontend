import Link from 'next/link';
import { memo, useState } from 'react';

import { ReviewElement, mapReviewItems } from '@/entities/review';
import { Typography } from '@/shared/ui';
import Lightbox from '@/shared/ui/Lightbox';
import Slider from '@/shared/ui/Slider';

import s from './ReviewsSection.module.scss';

const ReviewsSection = memo(() => {
  const List = mapReviewItems.map((el, index) => (
    <div key={index} onClick={() => setOpen(true)}>
      <ReviewElement className={s.item} {...el} />
    </div>
  ));
  const [open, setOpen] = useState(false);

  return (
    <article className={s.reviewsSlider}>
      <div className={s.titleWrapper}>
        <Typography variant="title_lg" boldness="bold">
          Отзывы
        </Typography>
        <Link
          href={{
            pathname: 'reviews',
            query: { name: 'Отзывы' },
          }}
          as="reviews"
          className={s.titleLink}
        >
          Смотреть все
        </Link>
      </div>
      <Slider
        sliderList={List}
        classNameWrapper={s.reviewsSlider__wrapper}
        breakpoints={{
          300: {
            // width: 576,
            slidesPerView: 1,
          },
          576: {
            // width: 576,
            slidesPerView: 1,
          },
          768: {
            // width: 768,
            slidesPerView: 1,
          },
          1023: {
            slidesPerView: 1,
          },
        }}
      />
      <Lightbox isOpen={open} onClose={() => setOpen(false)} slideList={mapReviewItems} />
    </article>
  );
});

export default ReviewsSection;
