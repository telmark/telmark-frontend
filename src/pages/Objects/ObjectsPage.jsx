import Image from 'next/image';

import { Typography } from '@/shared/ui';

import s from './ObjectsPage.module.scss';

const ObjectsPage = (props) => {
  const { list } = props;
  return (
    <article className={s.wrapper}>
      <Typography variant="title_lg" boldness="bold">
        Объекты
      </Typography>
      <Typography variant="title_md" boldness="medium">
        Объекты, на которые мы поставляем кабельно - проводниковую продукцию.
      </Typography>
      <ul className={s.list}>
        {list.map((el) => {
          return (
            <li key={el.id} className={s.item}>
              <div className={s.img}>
                <Image
                  className={s.imgWrapper}
                  src={el.img}
                  width={300}
                  height={200}
                  alt="gaz"
                  priority
                />
              </div>
              <Typography boldness="medium" className={s.title}>
                {el.title}
              </Typography>
              <Typography>{el.content}</Typography>
              <time dateTime={el.time}>{el.time}</time>
            </li>
          );
        })}
      </ul>
    </article>
  );
};

export default ObjectsPage;
