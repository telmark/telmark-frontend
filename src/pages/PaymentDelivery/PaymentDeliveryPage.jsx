import ContactForm from '@/features/contact-form/ui/ContactForm';
import { TabsContent } from '@/shared/ui';

import s from './PaymentDeliveryPage.module.scss';
import { Delivery, Payment, Self } from './components';

const mapTabsHeader = ['Оплата', 'Доставка', 'Самовывоз'];

function PaymentDeliveryPage() {
  const mapTabContent = [<Payment key={0} />, <Delivery key={1} />, <Self key={2} />];

  return (
    <article className={s.paymentDelivery__block}>
      <div className={s.paymentDelivery__wrapper}>
        <TabsContent componentsTab={mapTabContent} tabsHeader={mapTabsHeader} />
      </div>
      <ContactForm />
    </article>
  );
}
export default PaymentDeliveryPage;
