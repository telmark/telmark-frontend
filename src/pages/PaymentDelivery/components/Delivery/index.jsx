import Image from 'next/image';

import t1 from '@/shared/images/transport1.svg';
import t2 from '@/shared/images/transport2.svg';
import t3 from '@/shared/images/transport3.svg';
import t4 from '@/shared/images/transport4.svg';
import t5 from '@/shared/images/transport5.svg';
import t6 from '@/shared/images/transport6.svg';
import { Typography } from '@/shared/ui';

import s from './Delivery.module.scss';

export function Delivery() {
  return (
    <div>
      <header className={s.delivery__header}>
        <Typography variant="text_default" mb="mb">
          Доставка осуществляется по всей территории Санкт-Петербургу и Ленинградской области,
          Москве и Московской области, и по всей территории России. Сроки доставки согласовываются с
          каждым клиентом отдельно.
        </Typography>
      </header>
      <Typography variant="text_xl" boldness="medium">
        Транспортные компании
      </Typography>
      <ul>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t1} alt="img1" />
          </div>
          <Typography variant="text_default" mb="mb">
            Деловые Линии &ndash; это компания, которая не первый год занимается перевозкой
            кабельно-проводниковой продукции. В связи с этим, сотрудники знакомы со всеми тонкостями
            и нюансами перевозок, а также консультируются с сотрудниками компании об условиях
            перевозки.
          </Typography>
        </li>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t2} alt="img2" />
          </div>
          <Typography variant="text_default" mb="mb">
            У компании есть представительства в более чем 80 городах России. Компания
            &laquo;Байкал-Сервис&raquo; работает в сфере перевозок уже более 24 лет и за это время
            проявила себя как надежного и внимательного перевозчика, заслуживающего доверия как
            малого, так и крупного бизнеса. Водители знакомы со всеми деталями перевозок, а
            транспортные средства оснащены должным образом для перевозки и надежной фиксации кабеля.
            Байкал-Сервис работает быстро и надежно.
          </Typography>
        </li>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t3} alt="img3" />
          </div>
          <Typography variant="text_default" mb="mb">
            Компания &laquo;Кашалот&raquo; выполняет перевозки не только по России, но и в соседние
            страны &ndash; Армению, Беларусь, Казахстан, Кыргызстан и др. Опыт работы составляет 15
            лет. Филиалы компании расположены более чем в 237 городах. В день отправляется более
            25.000 посылок. На сайте компании можно рассчитать стоимость доставки вашего заказа.
          </Typography>
        </li>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t4} alt="img4" />
          </div>
          <Typography variant="text_default" mb="mb">
            Заказывая перевозку кабеля через компанию ПЭК, вы можете быть уверены в том, что груз
            прибудет в целости и сохранности. У компании более 72-х филиалов, что значительно
            упрощает доставку даже в самые маленькие города России. Доставка выполняется по
            доступной цене и в короткие сроки.
          </Typography>
        </li>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t5} alt="img5" />
          </div>
          <Typography variant="text_default" mb="mb">
            Самые низкие цены на перевозки сборного груза на данный момент предоставляет компания
            ВОЗОВОЗ, которая появилась на рынке относительно недавно. Компания сотрудничает как с
            физическими, так и с юридическими лицами. Процесс оформления заявки и получения груза
            занимает считанные минуты.
          </Typography>
        </li>
        <li className={s.delivery__item}>
          <div className={s.img}>
            <Image src={t6} alt="img6" />
          </div>
          <Typography variant="text_default" mb="mb">
            Компания работает по всей территории РФ, а также за ее пределами. Перевозка
            кабельно-проводниковой продукции &ndash; это одно из направлений перевозок ТК Энергия.
            Ваш груз будет надежно зафиксирован во время перевозки, прибудет в целости и
            сохранности. Получить его вы сможете уже через пару дней после оформления доставки.
          </Typography>
        </li>
      </ul>
    </div>
  );
}
