import Link from 'next/link';

import { Typography } from '@/shared/ui';

export function Self() {
  return (
    <div>
      <Typography variant="text_default" mb="mb">
        Клиент может самостоятельно забрать свой заказ на складе или в офисе компании, расположенном
        в вашем городе.
      </Typography>
      <Typography variant="text_default">
        С адресами складов можно ознакомиться{' '}
        <Link href="/contact" style={{ color: '#2559d9' }}>
          тут
        </Link>
      </Typography>
    </div>
  );
}
