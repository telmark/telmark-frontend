import { Typography } from '@/shared/ui';

import s from './ProviderPage.module.scss';
import { ProviderBlock } from './components/ProviderBlock';

function ProviderPage() {
  return (
    <article className={s.providerPage__wrapper}>
      <Typography variant="title_lg" boldness="bold">
        Поставщикам
      </Typography>
      <div className={s.providerPage__block}>
        <section>
          <Typography variant="text_default" mb="mb">
            Мы всегда открыты для новых идей и рады сотрудничеству с новыми поставщиками! В нашей
            работе для нас главное – качество.
          </Typography>
          <Typography variant="text_default" mb="mb">
            Если вы готовы сотрудничать – отправьте нам письмо по электронной почте{' '}
            <a href="mailto:sale@telmark.ru" style={{ textDecoration: 'underline' }}>
              <Typography variant="text_span" boldness="medium">
                sale@telmark.ru
              </Typography>
            </a>{' '}
            или свяжитесь с нами по телефону{' '}
            <a href="tel:88006004091" style={{ textDecoration: 'underline' }}>
              <Typography variant="text_span" boldness="medium">
                8 800 600 40 91{' '}
              </Typography>
            </a>{' '}
          </Typography>
          <Typography variant="text_default" mb="mb">
            У нас самые выгодные условия для сотрудничества.
          </Typography>

          <Typography variant="text_default" mb="mb">
            <b>Почему стоит продавать свой товар именно у нас:</b>
          </Typography>
          <ul className="_new_listWithCirles">
            <li>
              <Typography variant="text_default" mb="mb">
                Большой опыт работы в данной сфере &ndash; 8 лет.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                За года работы мы смогли зарекомендовать себя как команду профессионалов с широким
                ассортиментом товаров по доступным ценам.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                У нас сотни довольных клиентов, которые обращаются только к нам.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                У нас высокий товарооборот &ndash; в месяц мы заключаем более 200 сделок.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                У нас 540 постоянных клиентов, которые доверяют нам уже долгие годы.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                У магазина собственные склады, что позволяет снизить стоимость конечного продукта и
                привлечь большее количество покупателей.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                Мы работаем по полной предоплате, что упрощает процесс покупки нам и нашим клиентам.
                В некоторых ситуациях возможна отсрочка платежа. Отсрочка может быть до одного
                месяца. Оформление отсрочки обсуждается с менеджером компании индивидуально для
                каждого клиента.
              </Typography>
            </li>
            <li>
              <Typography variant="text_default" mb="mb">
                Быстрая обработка заявок и отгрузка товаров.
              </Typography>
            </li>
          </ul>
          <Typography variant="text_default" mb="mb">
            Если вы хотите стать нашим партнером, свяжитесь с нами удобным для вас способом для
            передачи вашего прайс-листа. Мы проведем анализ ценовой политики и внесем ваш товар в
            нашу базу данных. Если покупатель выберет вашу цену &ndash; будет продан именно ваш
            товар.
          </Typography>
          <Typography variant="text_default" mb="mb">
            С нами вы получите стабильные продажи и высокий товарооборот, который принесет высокую
            прибыль. Мы сделаем так, чтобы о вас узнали как можно больше людей!
          </Typography>
          <Typography variant="text_default" mb="mb">
            Мы уже сотрудничаем с большим количеством компаний из России и стран ближнего зарубежья.
          </Typography>
          <ProviderBlock />
        </section>
      </div>
    </article>
  );
}
export default ProviderPage;
