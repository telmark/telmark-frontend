import React, { useState } from 'react';

import { ToggleBtn } from '@/entities/common/toggle-btn';
import { Typography } from '@/shared/ui';

import s from './ProviderBlock.module.scss';
import { mapProviders } from './data';

function ProviderList() {
  return (
    <div className={s.provider__wrapper}>
      <ul className={s.provider__list}>
        {mapProviders.left.map((el, index) => (
          <li key={index}>
            <a href={el.link} target="_blank" rel="noreferrer">
              <Typography variant="text_xl" className={s.provider__item} boldness="medium">
                {el.name}
              </Typography>
            </a>
          </li>
        ))}
      </ul>
      <ul className={s.provider__list}>
        {mapProviders.right.map((el, index) => (
          <li key={index}>
            <a href={el.link} target="_blank" rel="noreferrer">
              <Typography variant="text_xl" className={s.provider__item} boldness="medium">
                {el.name}
              </Typography>
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

export function ProviderBlock() {
  const [toggle, setToggle] = useState(false);
  const textBtn = toggle ? 'Скрыть список производителей' : 'Показать список производителей';
  return (
    <>
      <ToggleBtn handler={() => setToggle((prev) => !prev)} textBtn={textBtn} />
      {toggle ? <ProviderList /> : null}
    </>
  );
}
