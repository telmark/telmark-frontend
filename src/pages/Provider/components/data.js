const left = [
  {
    link: 'https://amurcable.ru/',
    name: 'Амурский кабельный завод (Амуркабель)',
  },
  {
    link: 'http://cable88.chat.ru/index.html',
    name: 'Армавирский завод связи (АЗС)',
  },
  {
    link: 'http://vdkabel.ru/',
    name: 'Волгодонский кабельный завод (ВКЗ)',
  },
  {
    link: 'http://opticenergo.ru/company.html',
    name: 'Группа Компаний «Оптикэнерго»:',
  },
  {
    link: 'http://www.emcable.ru',
    name: 'ООО «ЭМ-КАБЕЛЬ»',
  },
  {
    link: 'http://www.sarko.ru/about-us.html',
    name: '«Сарансккабель-Оптика»',
  },
  {
    link: 'http://www.mkm.ru/',
    name: 'Группа компаний Москабельмет',
  },
  {
    link: 'http://www.tikrf.org/wp-content/uploads/2013/09/The-Group-of-Companies-%C2%ABTechnology-of-Metals%C2%BB.pdf',
    name: 'Группа компаний «Технология металлов»',
  },
  {
    link: 'http://www.pskovkabel.ru/about/overview/',
    name: 'ОАО «Псковский Кабельный Завод»',
  },
  {
    link: 'http://www.pskovgeokabel.ru/about/overview/',
    name: 'ООО Псковгеокабель',
  },
  {
    link: 'https://cabletm.ru/company/index.php',
    name: 'ООО «ТЕХНОКАБЕЛЬ М»',
  },
  {
    link: 'http://www.dmitrovkabel.ru/index.php',
    name: 'Дмитровский кабельный завод (Дмитров-Кабель)',
  },
  {
    link: 'http://www.ludinovocable.ru/',
    name: 'ЗАО Завод Людиновокабель',
  },
  {
    link: 'http://kavkazcabel.ru/about',
    name: 'ЗАО Кабельный завод Кавказкабель',
  },
  {
    link: 'http://www.elkab.ru/',
    name: 'ЗАО Кабельный завод «Элкаб»',
  },
  {
    link: 'http://kubancabel.ru/about/',
    name: 'ЗАО Кубанькабель',
  },
  {
    link: 'http://www.borets.ru/about/plants/kurgan-plant/',
    name: 'Курганский кабельный завод',
  },
  {
    link: 'http://www.metallistcable.ru/',
    name: 'ЗАО Металлист',
  },
  {
    link: 'http://www.ocs01.ru/',
    name: 'ЗАО ОКС 01',
  },
  {
    link: 'https://elprovod.ru/',
    name: 'ЗАО Электропровод',
  },
  {
    link: 'http://www.energokab.ru/zavod/',
    name: 'ЗАО «Завод «Энергокабель»',
  },
  {
    link: 'http://www.kzktm.ru/',
    name: 'ЗАО «Кавказкабель ТМ»',
  },
  {
    link: 'http://www.mpkabel.ru/',
    name: 'ЗАО «МАРПОСАДКАБЕЛЬ»',
  },
  {
    link: 'https://polimet-cable.ru/about/',
    name: 'ЗАО «Полимет»',
  },
  {
    link: 'http://www.pskabel.ru/company/',
    name: 'ЗАО «Промстройкабель»',
  },
  {
    link: 'https://www.rekacables.com/about-reka/',
    name: 'ЗАО «Река Кабель»',
  },
  {
    link: 'http://www.spkb.ru',
    name: 'ЗАО «СПКБ Техно»',
  },
  {
    link: 'http://www.samaracable.ru/about/history/',
    name: 'ЗАО «Самарская кабельная компания» (ЗАО СКК)',
  },
  {
    link: 'http://www.soccom.ru/company/',
    name: 'ЗАО «Самарская оптическая кабельная компания» (ЗАО «СОКК»)',
  },
  {
    link: 'http://termocabel.ru/page30925267.html',
    name: 'ЗАО «Термокабель Электропечь»',
  },
  {
    link: 'https://www.nexans.com/',
    name: 'Завод Nexans (Нексанс Россия) ООО «Нексанс СНГ»',
  },
  {
    link: 'http://kvantcable.ru/',
    name: 'Завод КвантКабель',
  },
  {
    link: 'http://www.cabeltov.ru/company/about/',
    name: 'Кабельный завод «Кабельтов» (Кабельтов)',
  },
  {
    link: 'http://konturcable.ru/',
    name: 'Кабельный завод «КонтурКабель»',
  },
  {
    link: 'http://kcab.ru/',
    name: 'Калужский кабельный завод (ККЗ)',
  },
  {
    link: 'https://ru.prysmiangroup.com/',
    name: 'Компания Prysmian Group',
  },
  {
    link: 'http://www.spcable.ru/company.html',
    name: 'НПП Спецкабель',
  },
  {
    link: 'https://www.elec.ru/catalog/teploskat/',
    name: 'НТЦ Теплоскат',
  },
  {
    link: 'http://www.nkz-nsk.ru/',
    name: 'Новосибирский кабельный завод (НКЗ)',
  },
  {
    link: 'https://microprovod.ru/o-kompanii/',
    name: 'ОАО Завод Микропровод',
  },
  {
    link: 'https://podolskkabel.ru/about/np/',
    name: 'ОАО НП Подольсккабель',
  },
  {
    link: 'http://www.okbkp.ru/now.html',
    name: 'ОАО ОКБ КП',
  },
  {
    link: 'http://www.expocable.ru/',
    name: 'ОАО «Подольский опытно-экспериментальный кабельный завод» (ОАО «Экспокабель»)',
  },
  {
    link: 'http://www.rosskat.ru',
    name: 'ОАО «РОССКАТ»',
  },
  {
    link: 'http://www.ufimcabel.ru',
    name: 'ОАО «Уфимкабель»',
  },
  {
    link: 'http://www.avtoprovod.com/about/company',
    name: 'ОАО «Щучинский завод «Автопровод»',
  },
  {
    link: 'http://www.tvercable.ru/',
    name: 'ОАО “Тверьэнергокабель”',
  },
  {
    link: 'http://vim-cable.com/about.php',
    name: 'ООО ВИМ Кабель',
  },
];

const right = [
  {
    link: 'http://vokz.ru/',
    name: 'ООО Верхнеокский кабельный завод',
  },
  {
    link: 'https://dedal-provod.ru/about/',
    name: 'ООО Дедал-провод',
  },
  {
    link: 'http://www.eurocabel-1.ru/?id=1',
    name: 'ООО Еврокабель I',
  },
  {
    link: 'http://www.agrocabel.ru/',
    name: 'ООО Завод Агрокабель',
  },
  {
    link: 'http://www.intg.ru/production/',
    name: 'ООО Интегра Кабельные Системы',
  },
  {
    link: 'http://magna.vluki.ru/main.php?page=about',
    name: 'ООО Магна',
  },
  {
    link: 'http://www.mcable.ru/',
    name: 'ООО Можайский кабель',
  },
  {
    link: 'http://www.liparcable.ru/about',
    name: 'ООО Новомосковский кабельный завод Липаркабель',
  },
  {
    link: 'https://www.paritet.su/o-zavode/',
    name: 'ООО Торгово-промышленный Дом Паритет',
  },
  {
    link: 'https://holdcable.com/ru/holding/',
    name: 'ООО Холдинг Кабельный Альянс',
  },
  {
    link: 'http://www.elcable.ru/about/history/',
    name: 'ОАО «Электрокабель» Кольчугинский завод»',
  },
  {
    link: 'http://www.sibkabel.ru/ru/',
    name: 'ЗАО Сибкабель',
  },
  {
    link: 'http://www.uralcable.ru/ru/about/',
    name: 'ЗАО «Уралкабель»',
  },
  {
    link: 'http://www.niki.ru/company/',
    name: 'ОАО Научно-исследовательский, проектно-конструкторский и технологический кабельный институт (НИКИ)',
  },
  {
    link: 'http://altayok.ru/about/',
    name: 'ООО «АлтайОптикаКабель»',
  },
  {
    link: 'http://opten.spb.ru/ru/about-company/stranitsa-istorii.html',
    name: 'ООО «Группа компаний Оптен»',
  },
  {
    link: 'http://www.expert-cable.ru/',
    name: 'ООО «Кабельный Завод «ЭКСПЕРТ-КАБЕЛЬ»',
  },
  {
    link: 'https://alur.ru/o-zavode',
    name: 'ООО «Кабельный завод «АЛЮР»',
  },
  {
    link: 'http://kcab.ru/',
    name: 'ООО «Калужский кабельный завод»',
  },
  {
    link: 'http://www.kamkabel.ru/about/abouttoday/',
    name: 'ООО «Камский кабель»',
  },
  {
    link: 'http://www.coaxial.ru/company/',
    name: 'ООО «Коаксиал»',
  },
  {
    link: 'http://nym.ru/',
    name: 'ООО «Конкорд»',
  },
  {
    link: 'http://mezhregionprom.ru/about/',
    name: 'ООО «МЕЖРЕГИОНАЛЬНАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ КОМПАНИЯ»',
  },
  {
    link: 'http://informsystema.com/',
    name: 'ООО «НПП Информсистема»',
  },
  {
    link: 'http://n-optics.com/',
    name: 'ООО «Новомосковсккабель-оптика»',
  },
  {
    link: 'http://rkz.ru/company/',
    name: 'ООО «Рыбинский кабельный завод»(Рыбинсккабель)',
  },
  {
    link: 'http://wolmag.ru/index/0-2',
    name: 'ООО «СП Волмаг»',
  },
  {
    link: 'http://saranskkabel.ru/index.php/categories/category/history',
    name: 'ООО «Сарансккабель»',
  },
  {
    link: 'https://severkabel.ru/',
    name: 'ООО «Северный кабель»',
  },
  {
    link: 'https://zavod-tatcable.ru/company/about-company/',
    name: 'ООО «ТАТКАБЕЛЬ»',
  },
  {
    link: 'https://www.tomskcable.ru/about/',
    name: 'ООО «Томский кабельный завод» Томсккабель',
  },
  {
    link: 'http://www.entecable.ru/',
    name: 'ООО «ЭНТЭ»',
  },
  {
    link: 'http://elixcable.com/ru/about/',
    name: 'ООО «Эликс-Кабель»',
  },
  {
    link: 'http://estralin.com/about/',
    name: 'ООО «Эстралин ЗВК»',
  },
  {
    link: 'http://www.nkz-nn.ru/index.php?option=com_content&amp;view=article&amp;id=8&amp;Itemid=8',
    name: 'ООО НКЗ «Электрокабель НН»',
  },
  {
    link: 'https://www.galva-mgn.ru/company/',
    name: 'ООО НПЦ «Гальва»',
  },
  {
    link: 'http://xn--80acbmcd2alljax9n.xn--p1ai/',
    name: 'ООО ПКФ «Воронежкабель»',
  },
  {
    link: 'http://www.ozkab.ru/',
    name: 'Озерский кабельный завод (ОКЗ)',
  },
  {
    link: 'http://orelkabel.ru/okz',
    name: 'Орловский кабельный завод (КАМИТ)',
  },
  {
    link: 'http://incab.ru/incab/',
    name: 'Пермский завод по производству оптического кабеля (Инкаб)',
  },
  {
    link: 'http://www.tkprok.ru/services/about.html',
    name: 'Петербургский кабельно-проводниковый завод «ПРОК» (ТК ПРОК)',
  },
  {
    link: 'http://rezhcable.ru/',
    name: 'Режевской кабельный завод (ЗАО «Режкабель»)',
  },
  {
    link: 'http://www.electroservis.ru/',
    name: 'Рязанский завод кабельной арматуры (РЗКА)',
  },
  {
    link: 'http://www.zaoskz.smolensk.ru',
    name: 'Смоленский кабельный завод',
  },
  {
    link: 'http://segmentenergo.ru/',
    name: 'Холдинговая Компания «СегментЭНЕРГО»',
  },
  {
    link: 'https://chuvashcable.ru/about/',
    name: 'Чебоксарский завод кабельных изделий (Чувашкабель)',
  },
];

export const mapProviders = {
  right,
  left,
};
