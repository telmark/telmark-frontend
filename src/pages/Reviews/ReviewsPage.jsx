import { useState } from 'react';

import { ReviewElement, mapReviewItems } from '@/entities/review';
import { Typography } from '@/shared/ui';
import Lightbox from '@/shared/ui/Lightbox';

import s from './ReviewsPage.module.scss';

function ReviewsPage() {
  const [open, setOpen] = useState(false);
  return (
    <>
      <article className="review">
        <div className={s.content}>
          <Typography variant="title_lg" boldness="bold">
            Отзывы
          </Typography>
          <ul>
            {mapReviewItems.map((el) => (
              <li key={el.key} onClick={() => setOpen(true)}>
                <ReviewElement {...el} />
              </li>
            ))}
          </ul>
        </div>
        <Lightbox isOpen={open} onClose={() => setOpen(false)} slideList={mapReviewItems} />
      </article>
    </>
  );
}
export default ReviewsPage;
