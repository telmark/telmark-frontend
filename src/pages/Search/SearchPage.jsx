import dynamic from 'next/dynamic';
import { memo, useMemo } from 'react';

import { MarkList } from '@/features/marks/ui';
import { Button, Typography } from '@/shared/ui';
import Catalog from '@/widgets/catalog';

import s from './SearchPage.module.scss';

const TrackBlock = dynamic(() => import('@/widgets/track-block'), {
  ssr: false,
});

const EmptyResult = (props) => {
  const { query } = props;
  return (
    <div className={s.wrapperEmpty}>
      <Typography variant="title_sm">
        По вашему запросу <strong>"{query}"</strong> ничего не нашлось
      </Typography>
      <Typography variant="title_sm">
        Попробуйте изменить запрос или поискать в каталоге.
      </Typography>
      <Button type="link" to="/cable">
        Перейти в каталог
      </Button>
    </div>
  );
};

const Result = memo((props) => {
  const { marks, products, queryStr } = props;
  const catalogInfo = useMemo(() => {
    return {
      ...products,
      data: products.data.filter((el, index) => index <= 9),
    };
  }, [products]);

  return (
    <div className={s.wrapperResult}>
      <Typography variant="title_lg" boldness="bold">
        Вы искали: "{queryStr}"
      </Typography>
      <MarkList markList={marks.data} />
      <Catalog catalogInfo={catalogInfo} />
      <TrackBlock title="Bы смотрели" />
    </div>
  );
});

const SearchPage = (props) => {
  const { marks, products, queryStr } = props;

  if (marks.data.length || products.data.length) {
    return <Result marks={marks} products={products} queryStr={queryStr} />;
  }

  return (
    <article>
      <EmptyResult query={queryStr} />
    </article>
  );
};

export default SearchPage;
