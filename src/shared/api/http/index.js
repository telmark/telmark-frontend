import { instance } from './instance';

export default function makeRequestXHR(method, options) {
  return instance
    .request({ method, url: options.url, data: options.data, ...options })
    .then((res) => {
      const code = Number(res.status);
      if (code !== 200) {
        throw res;
      }
      return res.data;
    })
    .catch((errorAxios) => {
      console.log('errorAxios', errorAxios);
      return errorAxios;
      //throw errorAxios;
    });
}
