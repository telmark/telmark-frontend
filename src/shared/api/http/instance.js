import axios from 'axios';

import { store } from '@/application/model';
import { API_URL } from '@/shared/consts';

export const instance = axios.create({
  baseURL: API_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
  withCredentials: true,
});

instance.interceptors.request.use(
  (config) => {
    const csrf = store.token.csrf;
    const { headers } = config;
    if (csrf && headers) {
      headers['X-CSRF-TOKEN'] = csrf;
    }
    // add headers for client requests
    if (typeof window !== 'undefined') {
      headers['x-domain'] = window.location.host.split('.')[0];
    }
    return config;
  },
  (error) => {
    console.log('req==error', error);
    return Promise.reject(error);
  },
);

instance.interceptors.response.use(
  async (response) => {
    return response;
  },
  function (error) {
    if (error.code === 'ERR_CANCELED') {
      return;
    }
    return Promise.reject((error.response && error.response.data) || 'Error');
  },
);
