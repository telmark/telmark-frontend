import { useCallback, useEffect } from 'react';

/**
 * @useClickOutside
 * @param {any} props.ref React ref
 * @param {number} props.handler react setState
 * @return {void}
 */

export function useClickOutside(ref, handler) {
  const handleDropdownClick = useCallback(
    (e) => {
      if (ref.current && ref.current.contains(e.target)) {
        return;
      } else {
        handler(false);
      }
    },
    [handler, ref],
  );

  useEffect(() => {
    document.addEventListener('mousedown', handleDropdownClick);
    //document.addEventListener('touchend', handleDropdownClick);
    return () => {
      document.removeEventListener('mousedown', handleDropdownClick);
      //document.removeEventListener('touchend', handleDropdownClick);
    };
  }, [handleDropdownClick]);
}
