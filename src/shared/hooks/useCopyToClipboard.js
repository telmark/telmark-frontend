import { useCallback } from 'react';

/**
 * @function useCopyToClipboard
 * @return {function} copyToClipboard
 */

export const useCopyToClipboard = () => {
  const copyToClipboard = useCallback((text) => {
    navigator.clipboard
      .writeText(text)
      .then(() => {
        console.log('Текст скопирован в буфер обмена:', text);
      })
      .catch((err) => {
        console.error('Не удалось скопировать текст: ', err);
      });
  }, []);

  return copyToClipboard;
};
