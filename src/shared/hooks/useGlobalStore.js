import { useContext } from 'react';

import { GlobalContext } from '@/application';

export function useGlobalStore() {
  const context = useContext(GlobalContext);
  if (context === undefined) {
    throw new Error('useStore must be used within StoreProvider');
  }

  return context;
}
