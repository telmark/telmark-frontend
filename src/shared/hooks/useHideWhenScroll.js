import { useEffect, useState } from 'react';

export function useHideWhenScroll(height = 300, isNeedInMobile = true) {
  const [isHide, setIsHide] = useState(false);
  const [isMobile, setMobile] = useState(false);

  useEffect(() => {
    let condition = isNeedInMobile || !isMobile;
    if (condition) {
      const listenToScroll = () => {
        let heightToHideFrom = height;
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop;

        setIsHide(winScroll > heightToHideFrom);
      };

      window.addEventListener('scroll', listenToScroll);
      return () => window.removeEventListener('scroll', listenToScroll);
    }
  }, [height, isNeedInMobile, isMobile]);

  useEffect(() => {
    setMobile(window.matchMedia('(max-width: 767px)').matches);
  }, []);

  return isHide;
}
