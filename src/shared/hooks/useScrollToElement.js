import { useCallback, useEffect, useState } from 'react';

export function useScrollToElement(ref) {
  const [isMobile, setMobile] = useState(false);
  const handler = useCallback(() => {
    window.scrollTo({ behavior: 'smooth', top: ref.current.offsetTop - isMobile });
  }, [ref, isMobile]);
  useEffect(() => {
    const headerHeight = window.matchMedia('(max-width: 767px)').matches ? 147 : 236;
    setMobile(headerHeight);
  }, []);
  return handler;
}
