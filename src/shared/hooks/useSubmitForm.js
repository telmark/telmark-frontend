import { useCallback, useState } from 'react';

/**
 * @useSubmitForm
 * @param {function} handler
 * @return {object}
 * @prop {function} trigger - trigger*
 * @prop {boolean} isLoading - loading status*
 * @prop {boolean} isSuccess - sucess*
 * @prop {boolean} isError - error*
 * @prop {function} resetFormStatuses - reset* status form
 */

export function useSubmitForm(handler) {
  const [isLoading, setLoading] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [isError, setError] = useState(false);

  const onSubmitHandler = useCallback(
    async (data) => {
      setLoading(true);
      try {
        await handler(data);
        setSuccess(true);
      } catch (error) {
        console.error(error);
        setError(true);
      } finally {
        setLoading(false);
      }
    },
    [handler],
  );
  const onResetForm = useCallback(() => {
    setSuccess(false);
    setLoading(false);
    setError(false);
  }, []);

  return {
    isLoading,
    isSuccess,
    isError,
    trigger: onSubmitHandler,
    resetFormStatuses: onResetForm,
  };
}
