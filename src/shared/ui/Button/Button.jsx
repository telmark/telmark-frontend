import classes from 'classnames';
import Link from 'next/link';

import s from './Button.module.scss';

/* enum ETypeBtn {
  submit = 'submit',
  button = 'button',
  reset = 'reset',
}

enum EColorBtn {
  red = 'red',
  blue = 'blue',
  blue_outline = 'blue_outline',
  red_outline = 'red_outline',
  gray = 'gray',
}

enum ESizeBtn {
  xl = 'xl',
  lg = 'lg',
  sm = 'sm',
}

type TypeButton = {
  children: React.ReactNode;
  onClick?: (arg?: any) => void;
  disabled?: boolean;
  type?: keyof typeof ETypeBtn;
  className?: string;
  color?: keyof typeof EColorBtn;
  size?: keyof typeof ESizeBtn;
}; */

/**
 * @Button
 * @param props
 * @param {TypeButton} props
 * @param {function} props.onClick функция действией
 * @param {boolean} props.disabled активная кнопка или нет
 * @param {ETypeBtn} props.type тип кнопки
 * @param {string} props.className кастомная стилизация
 * @param {EColorBtn} props.color цвет кнопки
 * @param {ESizeBtn} props.size размер кнопки
 */

export const Button = (props) => {
  const {
    className,
    color = 'blue',
    children,
    disabled,
    onClick,
    to = '/',
    type = 'button',
    /*  size = 'lg', */
    title = 'none',
  } = props;

  const clsName = classes({
    [s.main]: true,
    [s[color]]: color,
    [`${className}`]: className,
    //[style[size]]: size,
  });

  if (type === 'link') {
    return (
      <Link className={clsName} onClick={onClick && onClick} href={to}>
        {children}
      </Link>
    );
  }

  return (
    <button
      className={clsName}
      disabled={disabled && disabled}
      onClick={onClick && onClick}
      type={type}
      title={title}
    >
      {children}
    </button>
  );
};
