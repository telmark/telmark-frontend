import classNames from 'classnames';

import s from './ContainerHtml.module.scss';

/**
 * @ContainerHtml
 * @param props
 * @param {html string} htmlContent
 * @param {string} className
 */

export function ContainerHtml(props) {
  const { tag = 'div', htmlContent, className, itemProp = '' } = props;
  const Component = tag;
  const cls = classNames({
    [s.wrapper]: true,
    [`${className}`]: className,
  });

  return (
    <Component
      itemProp={itemProp}
      className={cls}
      dangerouslySetInnerHTML={{ __html: htmlContent }}
    />
  );
}
