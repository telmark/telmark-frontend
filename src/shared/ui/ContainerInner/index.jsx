import classNames from 'classnames';

import s from './ContainerInner.module.scss';

/**
 * @ContainerInner
 * @param props
 * @param {ReactNode} children
 * @param {string} className
 */

export function ContainerInner(props) {
  const { children, className } = props;
  const cls = classNames({
    [s.container]: true,
    [`${className}`]: className,
  });

  return <div className={cls}>{children}</div>;
}
