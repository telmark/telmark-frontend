import s from './ErrorMessage.module.scss';

/**
 * @ErrorMessage
 * @param props
 * @param {string} text текст сообщения
 */

export function ErrorMessage(props) {
  const { text } = props;
  return (
    <div role="alert" className={s.errorMsg}>
      {text}
    </div>
  );
}
