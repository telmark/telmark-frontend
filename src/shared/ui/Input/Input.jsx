import classNames from 'classnames';
import { forwardRef } from 'react';

import s from './Input.module.scss';

const mapTypes = {
  text: 'text',
  password: 'password',
  email: 'email',
  number: 'number',
  checkbox: 'checkbox',
};

/**
 * @Input
 * @param {TypeInput} props
 * @param {<string>} props.id - id
 * @param {<string>} props.placeholder - placeholder
 * @param {<boolean>} props.disabled - поведение disabled
 * @param {<string>} props.classNameInput - стили input
 * @param {<string>} props.name - name input
 * @param {<keyof typeof ETypeInput>} props.type - type input // text по default
 * @param {<TRest>} props.rest - кастомные атрибуты для <input> // onChange, onBlur и тд.
 */

export const Input = forwardRef((props, ref) => {
  const {
    placeholder,
    type = mapTypes['text'],
    name,
    id,
    classNameInput,
    disabled,
    onChange,
  } = props;
  const cls = classNames({
    [s.main]: true,
    [s.checkbox]: type === 'checkbox',
    [`${classNameInput}`]: classNameInput,
  });
  return (
    <input
      id={id}
      ref={ref}
      onChange={onChange}
      className={cls}
      placeholder={placeholder}
      type={type}
      name={name}
      disabled={disabled}
      {...props.rest}
    />
  );
});
/*
(prevProps, nextProps) =>
    prevProps.formState.isDirty === nextProps.formState.isDirty
*/
/* export enum ETypeInput {
  text = 'text',
  password = 'password',
  email = 'email',
  number = 'number',
  checkbox = 'checkbox',
  datetime = 'datetime',
  date = 'date',
  file = 'file',
}

export type TypeInput<TRest> = {
  id?: string;
  name?: string;
  placeholder?: string;
  disabled?: boolean;
  classNameInput?: string;
  //autocomplete?: 'off' | 'on';
  type?: keyof typeof ETypeInput;
  rest?: any;
  required?: any;
};
 */
