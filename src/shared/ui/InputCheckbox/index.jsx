import classNames from 'classnames';
import { forwardRef, useState } from 'react';

import s from './InputCheckbox.module.scss';

/**
 * @Input
 * @param {TypeInput} props
 * @param {<string>} props.id - id
 * @param {<string>} props.placeholder - placeholder
 * @param {<boolean>} props.disabled - поведение disabled
 * @param {<string>} props.classNameInput - стили input
 * @param {<string>} props.name - name input
 * @param {<keyof typeof ETypeInput>} props.type - type input // text по default
 * @param {<TRest>} props.rest - кастомные атрибуты для <input> // onChange, onBlur и тд.
 */

export const InputCheckbox = forwardRef((props, ref) => {
  const { name, id, className, defaultChecked = false, disabled, setValue } = props;
  const [state, setState] = useState(defaultChecked);
  const cls = classNames({
    [s.main]: true,
    [s.checkbox]: true,
    [`${className}`]: className,
  });

  return (
    <input
      id={id}
      ref={ref}
      className={cls}
      type={'checkbox'}
      defaultChecked={defaultChecked}
      name={name}
      disabled={disabled}
      value={state}
      onChange={() => {
        setState((prev) => {
          setValue(name, !prev);
          return !prev;
        });
      }}
    />
  );
});
