import { forwardRef, useEffect, useState } from 'react';

import s from './InputFile.module.scss';

/**
 * @InputFile
 * @param {TypeInputFile} props
 * @param {string} id - id
 * @param {function} onChange - onChange
 * @param {string} props.name - name input
 * @param {boolean} isSubmitSuccessful - rhf formState : {isSubmitSuccessful}
 */

export const InputFile = forwardRef((props, ref) => {
  const { id, name, onChange, isSubmitSuccessful, setValue, defaultValue } = props;
  const [fileName, setFileName] = useState('');

  const handlerName = (e) => {
    const name = e.target.files[0].name;
    setFileName(name);
    onChange(e);
    setValue('file', e.target.files[0]);
  };

  const removeFile = (e) => {
    e.stopPropagation();
    setFileName('');
    setValue('file', undefined);
  };

  useEffect(() => {
    if (isSubmitSuccessful) {
      setFileName('');
    }
  }, [isSubmitSuccessful]);

  useEffect(() => {
    if (Boolean(defaultValue)) {
      setFileName(defaultValue.name);
      setValue('file', defaultValue);
    }
  }, [defaultValue, setValue]);

  return (
    <div className={s.fileWrapper}>
      <label className={s.input_file}>
        <input
          id={id}
          ref={ref}
          className={s.input}
          type="file"
          name={name}
          onChange={handlerName}
        />
        <span className={s.btn}>{fileName ? fileName : 'Прикрепить файл'}</span>
      </label>
      {fileName ? (
        <button className={s.btnRemove} title="Убрать файл" onClick={removeFile}></button>
      ) : null}
    </div>
  );
});
