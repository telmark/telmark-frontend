import classNames from 'classnames';

import s from './Label.module.scss';

/* export type TypeLabel = {
  idLabel?: string;
  children?: string | React.ReactNode;
  classNameLabel?: string;
  required?: boolean;
}; */

/**
 * @Label
 * @param {TypeLabel} props
 * @param {<string>} props.idInput - связь с инпутом для label
 * @param {<boolean>} props.required - нужна ли звездочка * после текста label
 * @param {<string>} props.classNameLabel - стили для label
 * @param {<React.ReactNode>} props.children - содержимое label
 */

export const Label = (props) => {
  const { children, idLabel, className, required } = props;
  const cls = classNames({
    [s.label]: true,
    [s.requiredLabel]: required,
    [`${className}`]: className,
  });
  return (
    <label className={cls} htmlFor={idLabel}>
      {children}
    </label>
  );
};
