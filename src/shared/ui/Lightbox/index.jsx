import LightboxLIB from 'yet-another-react-lightbox';
import Captions from 'yet-another-react-lightbox/plugins/captions';
import Fullscreen from 'yet-another-react-lightbox/plugins/fullscreen';
import Slideshow from 'yet-another-react-lightbox/plugins/slideshow';
import Thumbnails from 'yet-another-react-lightbox/plugins/thumbnails';
import Video from 'yet-another-react-lightbox/plugins/video';
import Zoom from 'yet-another-react-lightbox/plugins/zoom';

/**
 * @Lightbox
 * @param {boolean} isOpen - флаг поведения
 * @param {function} onClose - функция закрытия
 * @param {arr} slideList - items
 * @prop {number} key
 * @prop {string} src
 * @prop {string} title
 * @prop {string} description
 */

export default function Lightbox(props) {
  const { isOpen, onClose, slideList } = props;
  return (
    <LightboxLIB
      open={isOpen}
      close={onClose}
      slides={slideList}
      plugins={[Captions, Fullscreen, Slideshow, Thumbnails, Video, Zoom]}
    />
  );
}
