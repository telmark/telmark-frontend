import Dialog from 'rc-dialog';
import 'rc-dialog/assets/index.css';

/**
 * @Modal
 * @param {TypeModal} props
 * @param {boolean} props.isOpen активная модалка или нет
 * @param {function} props.handlerClose функция действией открытия
 * @param {ReactNode} props.content содержимое окна
 */

const Modal = (props) => {
  const { isOpen, handlerClose, content, className = '' } = props;
  return (
    <Dialog
      title={'title'}
      visible={isOpen}
      className={className}
      animation="zoom"
      maskAnimation="fade"
      onClose={handlerClose}
      destroyOnClose={true}
    >
      {content}
    </Dialog>
  );
};

export default Modal;
