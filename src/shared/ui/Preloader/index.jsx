import React, { useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';

import s from './Preloader.module.scss';

const Preloader = () => {
  const ref = useRef(null);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    ref.current = document.querySelector('#portal');
    setMounted(true);
  }, []);

  if (mounted && ref.current) {
    return createPortal(
      <div className={s.overlay}>
        <svg className={s.icon} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
          <path
            opacity=".15"
            d="M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4"
          />
          <path
            fill="#2559d9"
            d="M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z"
            transform="rotate(101.307 16 16)"
          >
            <animateTransform
              attributeName="transform"
              type="rotate"
              from="0 16 16"
              to="360 16 16"
              dur="0.8s"
              repeatCount="indefinite"
            />
          </path>
        </svg>
      </div>,
      ref.current,
    );
  } else {
    return null;
  }
};

export default Preloader;
