import ReactSkeleton, { SkeletonTheme } from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

import s from './Skeleton.module.scss';

/**
 * @Skeleton as react-loading-skeleton
 * @param {boolean} isImage - задержка пропадания
 * @param {number} quantityElement - количество скелетонов
 * @param {string} className - className;
 * @param {string} id - id
 * @param {number} count - количество строк в скелетоне
 * @param {string} TagName - tag
 */

const Skeleton = (props) => {
  const {
    isImage,
    quantityElement = 1,
    count = 7,
    TagName = 'div',
    children,
    className = '',
  } = props;
  const Component = isImage ? (
    <ReactSkeleton className={s.skeletonImg} {...props} />
  ) : (
    <ReactSkeleton className={s.skeleton} count={count} {...props} />
  );

  return (
    <SkeletonTheme baseColor="#F1EFF1" highlightColor="white">
      <>
        {Array(quantityElement)
          .fill(1)
          .map((el, index) => {
            return (
              <TagName key={index} className={className}>
                {Component}
              </TagName>
            );
          })}
        {children}
      </>
    </SkeletonTheme>
  );
};

export default Skeleton;
