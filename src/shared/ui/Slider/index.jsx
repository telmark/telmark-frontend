import { useRef } from 'react';
import { Navigation, Pagination } from 'swiper';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import { Swiper, SwiperSlide } from 'swiper/react';

/**
 * @Slider
 * @param {TypeSlider} props
 * @param {Array} props.sliderList массив данных (компоненты)
 * @param {string} props.className стили для слайда
 * @param {string} props.classNameWrapper стили для блока-обертки
 * @param {number} props.spaceBetween расстояние между слайдами
 * @param {boolean} props.navigation флаг показа стрелок навигации
 * @param {object} props.breakpoints объект медиа отображения слайдеров
 */

export default function Slider(props) {
  const {
    sliderList,
    className = '',
    classNameWrapper = '',
    classNameSlide = '',
    spaceBetween = 0,
    loop = true,
    navigation = true,
    breakpoints = {},
  } = props;
  const prevRef = useRef(null);
  const nextRef = useRef(null);

  return (
    <div className={classNameWrapper}>
      {navigation ? (
        <>
          <div ref={nextRef} className="swiper-button swiper-button--next"></div>
          <div ref={prevRef} className="swiper-button swiper-button--prev"></div>
        </>
      ) : null}
      <Swiper
        className={className}
        onInit={(swiper) => {
          swiper.params.navigation.prevEl = prevRef.current;
          swiper.params.navigation.nextEl = nextRef.current;
          swiper.navigation.init();
          swiper.navigation.update();
        }}
        modules={[Navigation, Pagination]}
        navigation={{
          prevEl: prevRef.current,
          nextEl: nextRef.current,
        }}
        pagination={{
          bulletClass: 'swiper-pagination-bullet',
          clickable: true,
        }}
        spaceBetween={spaceBetween}
        loop={loop}
        speed={800}
        breakpoints={breakpoints}
      >
        {sliderList.map((el, index) => {
          return (
            <SwiperSlide key={index} className={classNameSlide}>
              {el}
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
}
