import { memo } from 'react';

import { Typography } from '@/shared/ui';

import s from './THead.module.scss';

const THead = memo((props) => {
  const { mapHeadItems = [], isTable = false } = props;

  if (isTable) {
    return (
      <thead>
        <tr>
          {mapHeadItems.map((el) => {
            return (
              <th key={el.id} className={el.hide ? s.hide : ''}>
                <Typography variant="title_sm" boldness="medium">
                  {el.value}
                </Typography>
              </th>
            );
          })}
        </tr>
      </thead>
    );
  }

  return (
    <div className={s.table__head}>
      {mapHeadItems.map((el) => {
        return (
          <div key={el.id} className={s.head__cell}>
            {el.value}
          </div>
        );
      })}
    </div>
  );
});

export default THead;
