import { memo } from 'react';

import s from './Tablist.module.scss';

//TODO: refactor
// el.type === activeType
export const Tablist = memo((props) => {
  const { handler, activeElement, tabsHeader, classNameList = '' } = props;
  return (
    <ul className={s.tabList}>
      {tabsHeader.map((el, index) => {
        return (
          <li
            key={index}
            className={`${s.item} ${activeElement === index ? s.active : ''} ${classNameList}`}
            onClick={() => handler(index)}
          >
            {el}
          </li>
        );
      })}
    </ul>
  );
});
