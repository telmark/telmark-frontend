import { Fragment, memo } from 'react';

import s from './TabsContent.module.scss';

export const TabsContent = memo((props) => {
  const { componentsTab = [], tabsHeader = [], classNameList = '' } = props;

  return (
    <>
      <div className={s.tabset}>
        {tabsHeader.map((h, index) => {
          return (
            <Fragment key={index}>
              <input type="radio" name="tabset" id={`tab${index}`} defaultChecked={index === 0} />
              <label htmlFor={`tab${index}`} className={`${s.tab} ${classNameList}`}>
                {h}
              </label>
            </Fragment>
          );
        })}
        <article className={s.tabPanels}>
          {componentsTab.map((Component, index) => {
            return (
              <section key={index} className={s.tabPanel}>
                <div id={`tab-content${index}`} className={s.content}>
                  {Component}
                </div>
              </section>
            );
          })}
        </article>
      </div>
    </>
  );
});
