import classNames from 'classnames';
import { forwardRef } from 'react';

import style from './Textarea.module.scss';

/**
 * @Textarea
 * @param {TypeTextarea} props
 * @param {string} props.name - name
 * @param {function} props.onChange - function onChange
 * @param {string} props.className - стили для Textarea
 * @param {string} props.placeholder - placeholder
 */

export const Textarea = forwardRef((props, ref) => {
  const { className, placeholder, onChange, name } = props;
  const cls = classNames({
    [style.main]: true,
    [`${className}`]: className,
  });
  return (
    <textarea
      ref={ref}
      name={name}
      className={cls}
      placeholder={placeholder}
      onChange={onChange}
    ></textarea>
  );
});
