import classNames from 'classnames';
import { memo } from 'react';

import s from './Typography.module.scss';

/* enum ETypographyTypeface {
  bold = 'bold',
  semibold = 'semibold',
  medium = 'medium',
  regular = 'regular',
} */

/**
 * @desc Все возможные варианты тэга
 * @readonly
 * @enum {string}
 * title_xl === h1
 * title_lg === h2
 * title_md === h3
 * title_sm === h4
 * text_xl  text_lg text_md  text_sm === p
 * table_lg === span
 * other === div
 */
/* enum ETags {
  title_xl = 'title_xl',
  title_lg = 'title_lg',
  title_md = 'title_md',
  title_sm = 'title_sm',
  text_xl = 'text_xl',
  text_lg = 'text_lg',
  text_md = 'text_md',
  text_sm = 'text_sm',
  table_lg = 'table_lg',
  undefined = 'undefined',
} */

/* type TypeTags = keyof typeof ETags;
type TypeTypographyTypeface = keyof typeof ETypographyTypeface; */

/**
 * @desc Выбор тэга
 * @param {TypeTags} v - значение для выбора тэга типографии
 */

/* type TypeTypography = {
  children: React.ReactNode | string;
  variant?: TypeTags;
  className?: string;
  boldness?: TypeTypographyTypeface;
  color?: 'gray' | 'red' | 'default';
}; */
const mapTags = {
  title_xxl: 'h1',
  title_xl: 'h1',
  title_lg: 'h2',
  title_md: 'h3',
  title_sm: 'h4',
  text_xl: 'p',
  text_default: 'p',
  text_span: 'span',
};
/**
 * @Typography
 * @desc компонент типографии для текстов и заголовков, а так же других типографических вещей
 * @param {TypeTypography} props
 * @param {React.ReactNode || string} props.children наполнение компонента
 * @param {TypeTags} props.variant выбор тэга || default div
 * @param {string} props.className кастомная стилизация
 * @param {TypeTypographyTypeface} props.boldness начертание шрифта=
 */
export const Typography = memo((props) => {
  const {
    children,
    variant = 'text_default',
    boldness = 'regular',
    className,
    color = 'default',
    mb,
    itemProp = '',
  } = props;

  const Component = mapTags[variant];

  const cls = classNames({
    //[style.main]: true,
    [s[variant]]: variant,
    [s[boldness]]: boldness,
    [s[mb]]: mb,
    [`${className}`]: className,
    [s[`color_${color}`]]: color,
  });

  return (
    <Component className={cls} itemProp={itemProp}>
      {children}
    </Component>
  );
});
Typography.displayName = 'Typography';
