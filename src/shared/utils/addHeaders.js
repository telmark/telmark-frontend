import { instance } from '@/shared/api/http/instance';

/**
 * @addHeaders for SSR
 * @param {getServerSideProps} context
 * @return {void}
 */
export function addHeaders(context) {
  const hostName = context.req?.headers?.host;
  instance.defaults.headers['x-domain'] = hostName;
}
