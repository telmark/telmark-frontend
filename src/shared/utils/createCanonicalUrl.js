/**
 * @createCanonicalUrl
 * @param {getServerSideProps} context
 * @return {string}
 */

export function createCanonicalUrl(context) {
  const canonicalUrl = 'https://' + context.req.headers.host + context.resolvedUrl.split('?')[0];
  return canonicalUrl;
}
