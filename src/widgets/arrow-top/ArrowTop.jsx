import { useHideWhenScroll } from '@/shared/hooks';

import s from './ArrowTop.module.scss';

export const ArrowTop = () => {
  const isHide = useHideWhenScroll(300);
  const scrollToTop = () => {
    window.scrollTo({ behavior: 'smooth', top: 0 });
  };

  if (!isHide) {
    return null;
  }

  return (
    <aside>
      <div className={s.wrapper}>
        <button className={s.btn} title="Вверх" onClick={scrollToTop}>
          <svg
            width="19"
            height="29"
            viewBox="0 0 19 29"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M0.142949 9.67285C-0.0503626 9.47565 -0.0472157 9.15909 0.149978 8.96578L9.14998 0.14295C9.3444 -0.0476498 9.65559 -0.0476498 9.85002 0.14295L18.85 8.96578C19.0472 9.15909 19.0504 9.47565 18.857 9.67285C18.6637 9.87004 18.3472 9.87319 18.15 9.67988L10 1.69034L10 28.5C10 28.7761 9.77614 29 9.5 29C9.22386 29 9 28.7761 9 28.5L9 1.69034L0.850021 9.67988C0.652827 9.87319 0.33626 9.87004 0.142949 9.67285Z"
              fill="#2559D9"
            />
          </svg>
        </button>
      </div>
    </aside>
  );
};
