import Link from 'next/link';
import { memo, useCallback, useState } from 'react';

import { Typography } from '@/shared/ui';
import Slider from '@/shared/ui/Slider';

import s from './CableSliderWidget.module.scss';

const CatalogListItem = (props) => {
  const { titleList, items, activeElement } = props;

  return (
    <li className={`${s.item__wrapper} ${activeElement ? s.itemActive : ''}`}>
      <Typography variant="title_md" boldness="bold">
        По {titleList}
      </Typography>

      <ul className={s.list}>
        {items.map((el, index) => {
          return (
            <li key={index}>
              <Link
                href={{
                  pathname: `/cable/[...slug]`,
                  query: { slug: el.slug },
                }}
                className={s.text}
              >
                {el.singular_name}
              </Link>
            </li>
          );
        })}
      </ul>
    </li>
  );
};

/**
 * @CableSlider
 * @param {arr} nameList
 * @param {string} props.singular_name - name
 */

const CableSlider = memo((props) => {
  const { nameList, activeElement, onSelectFilter } = props;

  const List = nameList.map((el, index) => {
    return (
      <div
        key={index}
        className={`${s.sliderBtn} ${el.singular_name === activeElement ? s.active : ''}`}
        onClick={() => onSelectFilter(el.singular_name)}
      >
        По {el.singular_name}
      </div>
    );
  });

  return (
    <Slider
      classNameWrapper={s.wrapper}
      className={s.slider}
      classNameSlide={s.sliderItem}
      spaceBetween={8}
      loop={false}
      breakpoints={{
        300: {
          // width: 576,
          slidesPerView: 'auto',
          spaceBetween: 9,
        },
        576: {
          // width: 576,
          slidesPerView: 'auto',
          spaceBetween: 9,
        },
        768: {
          // width: 768,
          slidesPerView: 'auto',
          spaceBetween: 9,
        },
        1023: {
          slidesPerView: 7,
        },
      }}
      sliderList={List}
    />
  );
});

export const CableSliderWidget = (props) => {
  const { catalogList } = props;
  const [selectedFilterName, setSelectedFilterName] = useState('типу');

  const onSelectFilter = useCallback((name) => {
    setSelectedFilterName(name);
  }, []);

  return (
    <section>
      <CableSlider
        nameList={catalogList}
        activeElement={selectedFilterName}
        onSelectFilter={onSelectFilter}
      />
      <ul>
        {catalogList.map((row, index) => {
          const selsectedType = row.singular_name === selectedFilterName;
          return (
            <CatalogListItem
              key={index}
              activeElement={selsectedType}
              items={row.children}
              titleList={row.singular_name}
            />
          );
        })}
      </ul>
    </section>
  );
};
