import { observer } from 'mobx-react-lite';
import { useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import CartTotal from '@/entities/cart/CartTotal';
import { CartFormSend } from '@/features/cart/ui/cart-form-send';
import { useGlobalStore } from '@/shared/hooks';
import { Button } from '@/shared/ui';

import s from './CartBottom.module.scss';

export const CartBottom = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const { cart } = useGlobalStore();
  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const clearCart = async () => {
    await cart.deleteCart();
  };

  const onSubmitCartOrder = async (data) => {
    try {
      // eslint-disable-next-line no-undef
      ym(56820658, 'reachGoal', 'order');
      await cart.sendCart(data);
    } catch (e) {
      throw new Error(e);
    }
  };
  //console.log('cart==', cart);
  return (
    <div className={s.cartBottom}>
      <CartTotal />
      <Button title="Оформить заказ" onClick={() => setIsOpen(true)}>
        Оформить заказ
      </Button>
      <Button title="Очистить корзину" color="gray" onClick={clearCart}>
        Очистить корзину
      </Button>
      <ModalWrapper
        isOpen={isOpen}
        handlerModalClose={handlerModalClose}
        handler={onSubmitCartOrder}
        textMessage="Ваш заказ успешно отправлен!"
        ContentModalForm={(props) => <CartFormSend {...props} />}
      />
    </div>
  );
});
