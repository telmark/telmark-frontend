import { observer } from 'mobx-react-lite';

import { CartElement } from '@/features/cart/ui/cart-element';
import { useGlobalStore } from '@/shared/hooks';
import THead from '@/shared/ui/THead';

import s from './CartTable.module.scss';

const mapHead = [
  {
    id: '1e',
    value: 'Наименование товара',
  },
  {
    id: '2e',
    value: 'Количество',
  },
  {
    id: '3e',
    value: 'Цена',
  },
  {
    id: '4e',
    value: 'Сумма (с НДС)',
  },
];

/* const a = [
  {
    cart_item_id: 4116,
    count: 1,
    name: 'ВВГнг(A)-LSLTx 5x35-1',
    price: 1259.71,
    product_id: 77822,
    url: '/cable/vvgnga-lsltx/vvgnga-lsltx-5x35-1',
  },
  {
    cart_item_id: 41126,
    count: 6,
    name: 'ВВГнг(A)-LSLTx 5x35-1',
    price: 1259.71,
    product_id: 778242,
    url: '/cable/vvgnga-lsltx/vvgnga-lsltx-5x35-1',
  },
]; */

export const CartTable = observer((props) => {
  const { handlerModalOpen } = props;
  const { cart } = useGlobalStore();

  return (
    <>
      <div className={s.cart__table}>
        <THead mapHeadItems={mapHead} />
        <div className={s.table__body}>
          {cart.itemsArray.map((el) => {
            return <CartElement key={el.cart_item_id} cartItem={el} handler={handlerModalOpen} />;
          })}
        </div>
      </div>
    </>
  );
});
