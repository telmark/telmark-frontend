import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { memo, useCallback, useState } from 'react';

import { CatalogFilter, CatalogPagination, CatalogPanel } from '@/features/catalog';
import Preloader from '@/shared/ui/Preloader';

import s from './Catalog.module.scss';

/**
 * @Catalog
 * @param {obj} catalogInfo
 * @prop {obj} meta - pagination (total, last_page, current_page)
 * @prop {obj} data - список товаров в каталоге
 */
//TODO: убрать image и сделать обертку для марок и товара
const Catalog = memo((props) => {
  const { catalogInfo, image } = props;
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const totalProducts = catalogInfo?.total;
  const totalPages = catalogInfo?.last_page;
  const currentPage = catalogInfo?.current_page;

  const onFilterCatalog = useCallback(
    async (core, section) => {
      const params = new URLSearchParams(searchParams);
      params.set('core', core);
      params.set('section', section);
      params.set('page', 1);
      setIsLoading(true);
      setTimeout(() => {
        replace(`${pathname}?${params.toString()}`);
        setIsLoading(false);
      }, 2000);
    },
    [pathname, replace, searchParams],
  );

  return (
    <section className={s.wrapperCatalog}>
      {isLoading ? <Preloader /> : null}
      <CatalogFilter handler={onFilterCatalog} />
      {!catalogInfo?.data?.length ? (
        <div>нет данных</div>
      ) : (
        <>
          <CatalogPanel mapCatalogBody={catalogInfo.data} image={image} />
        </>
      )}
      {catalogInfo.total >= 10 && catalogInfo?.data?.length ? (
        <CatalogPagination
          currentPage={currentPage}
          totalProducts={totalProducts}
          totalPages={totalPages}
        />
      ) : null}
    </section>
  );
});

export default Catalog;
