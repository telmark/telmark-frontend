export const cabelList = [
  {
    title: 'Нагреваемые',
    slug: 'kabeli-nagrevatelynye',
    pageName: 'Кабели нагреваемые',
  },
  {
    title: 'Судовые',
    slug: 'kabeli-sudovye',
    pageName: 'Кабели судовые',
  },
  {
    title: 'Холодостойкие',
    slug: 'kabeli-xolodostoykie',
    pageName: 'Кабели холодостойкие',
  },
  {
    title: 'Монтажные',
    slug: 'kabeli-montaghnye',
    pageName: 'Кабели монтажные',
  },
  {
    title: 'Силовые с резиновой изоляцией',
    slug: 'kabeli-silovye-s-rezinovoy-izolyaciey',
    pageName: 'Кабели силовые с резиновой изоляцией',
  },
  {
    title: 'Контрольные',
    slug: 'kabeli-kontrolynye',
    pageName: 'Кабели контрольные',
  },
  {
    title: 'Сварочные',
    slug: 'kabeli-svarochnye',
    pageName: 'Кабели сварочные',
  },
  {
    title: 'Универсальные',
    slug: 'kabeli-universalynye',
    pageName: 'Кабели универсальные',
  },
];

export const commonLinks = [
  {
    pathname: '/about',
    name: 'О компании',
  },
  {
    pathname: '/payment',
    name: 'Оплата и доставка',
  },
  {
    pathname: '/provider',
    name: 'Поставщикам',
  },
  {
    pathname: '/contact',
    name: 'Контакты',
  },
  {
    pathname: '/reviews',
    name: 'Отзывы',
  },
  {
    pathname: '/articles',
    name: 'Статьи',
  },
];

export const getFooterFullAddress = (resServer) => {
  let footerFullAddress = null;
  if (!resServer?.currentCity && !resServer?.primaryCity) {
    footerFullAddress =
      'г. Санкт-Петербург, вн. тер. г. Муниципальный округ Новоизмайловское,пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24';
    return footerFullAddress;
  }
  const info = resServer.currentCity ? resServer.currentCity : resServer.primaryCity;

  if (!info.is_main) {
    footerFullAddress = `г. ${info?.name}, ${info?.address}`;
  } else {
    footerFullAddress =
      'г. Санкт-Петербург, вн. тер. г. Муниципальный округ Новоизмайловское,пл. Конституции, д.2 Литера А, Помещ 19Н, Ч.П. 24';
  }
  return footerFullAddress;
};
