import { observer } from 'mobx-react-lite';
import Link from 'next/link';

import { Logo } from '@/entities/common/logo';
import { useGlobalStore } from '@/shared/hooks';
import { ContainerInner } from '@/shared/ui';

import s from './Footer.module.scss';
import { cabelList, commonLinks, getFooterFullAddress } from './data';

const Footer = observer(() => {
  const { city } = useGlobalStore();
  const footerFullAddress = getFooterFullAddress(city);
  return (
    <footer className={s.footer}>
      <div style={{ display: 'none' }}>
        <div itemScope itemType="http://schema.org/Organization">
          <span itemProp="name">Telmark</span>
          <div itemProp="address" itemScope itemType="http://schema.org/PostalAddress">
            <span itemProp="streetAddress">
              пл. Конституции, д.2 Литера А,
              <br /> Помещ 19Н, Ч.П. 24
            </span>
            <span itemProp="postalCode">196247</span>
            <span itemProp="addressLocality">Санкт-Петербург</span>,
          </div>
          <span itemProp="telephone">8 800 600 40 91</span>,
          <span itemProp="email">sale+1163823@telmark.ru</span>
        </div>
      </div>
      <div className={s.top}>
        <ContainerInner className={s.wrapper}>
          <section>
            <div className={s.logo}>
              <Logo />
            </div>
            <div className={s.txt}>
              Информация на сайте о технических характеристиках, наличии на складе, стоимости и
              изображениях товаров не является публичной офертой
            </div>
          </section>
          <section>
            <div className={s.title}>Популярные кабеля</div>
            <ul className={s.menu}>
              {cabelList.map((el, index) => {
                return (
                  <li key={index} className={s.item}>
                    <Link
                      className={s.link}
                      href={{
                        pathname: `/cable/[...slug]`,
                        query: { slug: el.slug },
                      }}
                    >
                      {el.title}
                    </Link>
                  </li>
                );
              })}
            </ul>
          </section>

          <section className={s.footer__indent}>
            <div className={s.title}>Навигация</div>
            <ul className={s.menu}>
              {commonLinks.map((el, index) => {
                return (
                  <li key={index} className={s.item}>
                    <Link
                      className={s.link}
                      href={{
                        pathname: el.pathname,
                      }}
                      as={el.pathname}
                    >
                      {el.name}
                    </Link>
                  </li>
                );
              })}
            </ul>
          </section>

          <section>
            <div className={s.title}>Контакты</div>
            <div className={s.contacts}>
              <div className={s.contacts__item}>{footerFullAddress}</div>
              <Link className={s.contacts__itemBig} href={`tel:${city.primaryCity?.phone}`}>
                {city.primaryCity?.phone}
              </Link>
              <Link className={s.contacts__itemBig} href="mailto:sale@telmark.ru">
                sale@telmark.ru
              </Link>
            </div>
          </section>
        </ContainerInner>
      </div>
      <div className={s.bottom}>
        <div className={s.footer__copyright}>{new Date().getFullYear()} Все права защищены</div>
      </div>
    </footer>
  );
});

export default Footer;
