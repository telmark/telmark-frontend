import classes from 'classnames';
import { observer } from 'mobx-react-lite';

import { Logo } from '@/entities/common/logo';
import DesktopNavigation from '@/features/menu/DesktopNavigation';
import MenuCatalogNavigation from '@/features/menu/MenuCatalogNavigation';
import MobileNavigation from '@/features/menu/MobileNavigation';
import { mapDesktopCatalog } from '@/features/menu/mapMenuData';
import SearchPanel from '@/features/search-panel/ui';
import { useHideWhenScroll } from '@/shared/hooks';
import catalog_icon from '@/shared/images/menu_catalog.svg';
import { ContainerInner } from '@/shared/ui';

import s from './Header.module.scss';
import { CartLink, Contacts } from './components';

const Header = observer(() => {
  const isHide = useHideWhenScroll(200, false);
  const clsNameWrapper = classes({
    [s.desktopNavigation]: true,
    [s.hideNavigation]: isHide,
  });

  return (
    <header className={s.header}>
      <div className={s.wrapper}>
        <div className={clsNameWrapper}>
          <DesktopNavigation isHide={isHide} />
        </div>
        <ContainerInner className={s.container}>
          <MobileNavigation />
          <Logo />
          <ul className={s.desktopCatalogMenu}>
            <MenuCatalogNavigation
              title="Каталог"
              iconSrc={catalog_icon}
              classNameWrapper={s.desktopCatalogWrapper}
              className={s.desktopCatalogMenuItem}
              subMenuItems={mapDesktopCatalog['nestedElements']}
            />
          </ul>
          <div className={s.headerSearchWrapper}>
            <SearchPanel />
          </div>
          <Contacts />
          <CartLink />
        </ContainerInner>
      </div>
    </header>
  );
});

export default Header;
