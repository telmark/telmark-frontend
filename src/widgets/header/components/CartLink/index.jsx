import { observer } from 'mobx-react-lite';
import Link from 'next/link';
import { useEffect } from 'react';

import { useGlobalStore } from '@/shared/hooks';

import s from './CartLink.module.scss';

export const CartLink = observer(() => {
  const { cart } = useGlobalStore();

  useEffect(() => {
    (async () => {
      await cart.fetchCart();
    })();
  }, [cart]);

  return (
    <Link
      className={s.headerCart}
      href={{
        pathname: 'cart',
        query: { name: 'Корзина' },
      }}
      as="/cart"
      aria-label="Корзина"
      title="Корзина"
    >
      <div className={s.headerCart__block}>
        {cart.cartItemsLength ? <div className={s.headerCart__count}></div> : <></>}
      </div>
    </Link>
  );
});
