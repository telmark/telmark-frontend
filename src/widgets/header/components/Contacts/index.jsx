import { observer } from 'mobx-react-lite';
import Image from 'next/image';
import Link from 'next/link';
import { useCallback, useState } from 'react';

import ModalWrapper from '@/entities/ModalWrapper';
import { submitContactForm } from '@/features/contact-form/model';
import FeedbackForm from '@/features/contact-form/ui/FeedbackForm';
import { useGlobalStore } from '@/shared/hooks';
import envelopeSvg from '@/shared/images/svg/envelope.svg';
import phoneSvg from '@/shared/images/svg/phone.svg';

import s from './Contacts.module.scss';

export const Contacts = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const { city } = useGlobalStore();

  const [formData, setFormData] = useState({
    name: '',
    phone: '',
    email: '',
    comment: '',
    contact_private_policy: true,
    file: undefined,
  });

  const handlerModalClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <div className={s.headerContacts}>
      <div className={s.item}>
        <div className={`${s.itemContent} ${s.itemMail}`}>
          <Link
            className={s.bigLink}
            href="mailto:sale@telmark.ru"
            onClick={() => {
              // eslint-disable-next-line no-undef
              ym(56820658, 'reachGoal', 'email_click');
            }}
            onCopy={() => {
              // eslint-disable-next-line no-undef
              ym(56820658, 'reachGoal', 'email_copy');
            }}
          >
            sale@telmark.ru
          </Link>
        </div>
        <button className={s.smallLink} title="Написать нам" onClick={() => setIsOpen(true)}>
          <Image className={s.icon} src={envelopeSvg} alt="img2" />
          <span className={s.text}>Написать нам</span>
        </button>
      </div>
      <div className={s.item}>
        <div className={`${s.itemContent}`}>
          <Link
            className={s.phoneLink}
            href={`tel:${city.primaryCity?.phone}`}
            title="Телефон"
            onClick={() => {
              // eslint-disable-next-line no-undef
              ym(56820658, 'reachGoal', 'call_click');
            }}
            onCopy={() => {
              // eslint-disable-next-line no-undef
              ym(56820658, 'reachGoal', 'call_copy');
            }}
          >
            <span className={s.phoneText}>{city.primaryCity?.phone}</span>
            <Image className={`${s.icon} ${s.phoneIcon}`} src={phoneSvg} alt="img1" />
          </Link>
        </div>
        <button
          className={`${s.smallLink} ${s.phoneBtn}`}
          title="Заказать звонок"
          onClick={() => setIsOpen(true)}
        >
          <Image className={s.icon} src={phoneSvg} alt="img1" />
          <span className={s.text}>Заказать звонок</span>
        </button>
      </div>
      <ModalWrapper
        isOpen={isOpen}
        formData={formData}
        setFormData={setFormData}
        handlerModalClose={handlerModalClose}
        handler={submitContactForm}
        ContentModalForm={(props) => <FeedbackForm {...props} />}
      />
    </div>
  );
});
