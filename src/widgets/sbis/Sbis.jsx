import classNames from 'classnames';
import Image from 'next/image';
import { memo, useEffect, useMemo, useState } from 'react';

import img from '@/shared/images/svg/sbis.svg';

import s from './Sbis.module.scss';

const expiredPeriod = 24 * 60 * 60 * 1000;
//const expiredPeriod = 60 * 1000;

export const Sbis = memo(() => {
  const [isClose, setClose] = useState(false);
  const cls = classNames({
    [s.wrapper]: true,
    [s.closeWrapper]: isClose,
  });

  const cache = useMemo(() => {
    /*  return defaultState(); */
    if (typeof window !== 'undefined') {
      return {
        status: window.localStorage.getItem('sbis'),
        time: window.localStorage.getItem('time'),
      };
    }
    return {};
  }, []);

  function isExpiredTime() {
    const time = localStorage.getItem('time');
    return time > Date.now();
  }
  function closeBanner() {
    setClose(true);
    localStorage.setItem('sbis', 'closed');
    localStorage.setItem('time', expiredPeriod + Date.now());
  }

  useEffect(() => {
    if (cache.status && cache.status !== 'showed' && isExpiredTime()) {
      setClose(true);
    } else {
      localStorage.removeItem('time');
      localStorage.setItem('sbis', 'showed');
    }
  }, [cache]);

  if (isClose) {
    return null;
  }
  return (
    <aside>
      <div className={cls} role="alert">
        <button
          className={s.btn}
          title="Закрыть"
          data-dismiss="alert"
          aria-label="Close"
          onClick={closeBanner}
        >
          <span className={s.close} aria-hidden="true">
            ×
          </span>
        </button>
        <strong>Рейтинг надежности нашей компании</strong> <br />
        <a
          href="https://sbis.ru/contragents/7806464579/781001001"
          border="0"
          target="_blank"
          rel="noreferrer"
        >
          <Image className={s.img} width={300} src={img} alt="sbis" priority={true} />
        </a>
        <br />
        <a
          className={s.moreTxt}
          href="https://sbis.ru/contragents/7806464579/781001001"
          target="_blank"
          rel="noreferrer"
        >
          Подробнее...
        </a>
      </div>
    </aside>
  );
});
