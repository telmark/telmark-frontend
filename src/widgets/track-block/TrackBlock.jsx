import { memo, useEffect, useState } from 'react';

import { fetchViewProducts } from '@/features/product/model';
import { Typography } from '@/shared/ui';
import Skeleton from '@/shared/ui/Skeleton';

import s from './TrackBlock.module.scss';
import { Card } from './components/card';

const SkeletonLoading = () => {
  return (
    <>
      <Typography variant="title_lg" boldness="bold">
        <Skeleton quantityElement={1} count={1} height={30} />
      </Typography>
      <ul className={s.list}>
        <Skeleton TagName="li" className={s.skeletonItem} quantityElement={3} count={12} />
      </ul>
    </>
  );
};

export const TrackBlock = memo((props) => {
  const { title } = props;
  const [isLoading, setLoading] = useState(true);
  const [product, setProduct] = useState([]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      try {
        const res = await fetchViewProducts();
        setProduct(res);
      } catch (error) {
        console.error('error==', error);
      } finally {
        setLoading(false);
      }
    })();
  }, [setProduct]);

  if (!product.length) {
    return null;
  }

  return (
    <article className={s.trackBlock__wrapper}>
      {isLoading ? (
        <SkeletonLoading />
      ) : (
        <>
          <Typography variant="title_md" boldness="medium">
            {title}
          </Typography>

          <ul className={s.list}>
            {product.map((el) => {
              return (
                <li key={el.id}>
                  <Card productData={el} />
                </li>
              );
            })}
          </ul>
        </>
      )}
    </article>
  );
});
