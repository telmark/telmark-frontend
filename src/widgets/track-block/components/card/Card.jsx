import Image from 'next/image';

import { AddToCartBtn } from '@/entities/cart';
import { AvailabilityBlock, Rating } from '@/entities/product';
import { useGlobalStore } from '@/shared/hooks';
import defImgCard from '@/shared/images/defImgCard.jpg';
import { Typography } from '@/shared/ui';

import s from './Card.module.scss';

export const Card = (props) => {
  const { productData } = props;
  const {
    id,
    name = {
      value: 'Наименование модели',
      link: '',
    },
    price = 33.93,
    availability = '1903',
    img = defImgCard,
    countProduct = 1,
  } = productData;
  const { cart } = useGlobalStore();

  return (
    <article className={s.card}>
      <div className={s.titleWrapper}>
        <AvailabilityBlock availabilityValue={availability} />
        <Rating />
      </div>
      <section className={s.imgWrapper}>
        <Image src={img} alt="Изображение" />
        <div className={s.imgTitle}>{name.value}</div>
      </section>
      <div className={s.actionsWrapper}>
        <section>
          <Typography variant="text_xl" className={s.price}>
            {price} руб/м
          </Typography>
        </section>
        <section>
          <AddToCartBtn handler={() => cart.addToCart(id, countProduct, productData)} />
        </section>
      </div>
    </article>
  );
};
